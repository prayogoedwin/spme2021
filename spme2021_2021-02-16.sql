# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: spme2021
# Generation Time: 2021-02-16 07:41:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table akses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `akses`;

CREATE TABLE `akses` (
  `id_akses` int(11) NOT NULL,
  `nama_akses` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_akses`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `akses` WRITE;
/*!40000 ALTER TABLE `akses` DISABLE KEYS */;

INSERT INTO `akses` (`id_akses`, `nama_akses`)
VALUES
	(77,'Kab/Kota'),
	(88,'Provinsi'),
	(99,'Adminstrator');

/*!40000 ALTER TABLE `akses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table datadasar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `datadasar`;

CREATE TABLE `datadasar` (
  `id_datadasar` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_pelayanan_dasar` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jumlah` bigint(100) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `th_pelaporan` bigint(100) NOT NULL,
  `anggaran` varchar(255) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_datadasar`),
  KEY `idjenispelayanandasar` (`id_jenis_pelayanan_dasar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `datadasar` WRITE;
/*!40000 ALTER TABLE `datadasar` DISABLE KEYS */;

INSERT INTO `datadasar` (`id_datadasar`, `id_jenis_pelayanan_dasar`, `id_user`, `jumlah`, `tahun`, `th_pelaporan`, `anggaran`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(37,37,157,430,2020,310220000,'3696660000','2021-01-11 12:50:02','2021-01-19 09:08:10',NULL),
	(38,40,157,135,2020,37560000,'156250000','2021-01-11 12:50:02','2021-01-19 09:26:27',NULL),
	(39,41,157,120,2020,25170000,'156250000','2021-01-11 12:50:02','2021-01-19 09:34:27',NULL),
	(40,42,157,45,2020,6281924,'156250000','2021-01-11 12:50:02','2021-01-19 09:34:58',NULL),
	(41,43,157,50,2020,16780000,'156250000','2021-01-11 12:50:02','2021-01-19 09:35:20',NULL),
	(42,48,96,1124,2020,2020,'98250000000','2021-01-13 15:38:47','2021-01-14 17:57:56',NULL),
	(43,50,96,0,2020,0,'0','2021-01-13 15:38:47','2021-01-14 17:57:56',NULL),
	(56,3,22,2999000111124124214,2020,2999000111124124214,'2999000111124124214','2021-01-14 17:59:35','2021-01-21 20:41:29',NULL),
	(57,4,22,2,2020,2,'2','2021-01-14 17:59:35',NULL,NULL),
	(58,5,22,3,2020,3,'3','2021-01-14 17:59:35',NULL,NULL),
	(59,38,22,4,2020,4,'4','2021-01-14 17:59:35',NULL,NULL),
	(60,49,104,1,2020,1,'1','2021-01-14 18:08:04',NULL,NULL),
	(61,51,104,1,2020,1,'1','2021-01-14 18:08:04',NULL,NULL),
	(62,8,58,30684,2020,25500000,'280750000','2021-01-18 08:18:12',NULL,NULL),
	(63,9,58,6137,2020,2147483647,'2533575000','2021-01-18 08:18:12',NULL,NULL),
	(64,10,58,28320,2020,8000000,'195000000','2021-01-18 08:18:12',NULL,NULL),
	(65,11,58,108813,2020,350000000,'1190000000','2021-01-18 08:18:12',NULL,NULL),
	(66,12,58,56338,2020,0,'120000000','2021-01-18 08:18:12',NULL,NULL),
	(67,13,58,1348904,2020,12816600,'86200000','2021-01-18 08:18:12',NULL,NULL),
	(68,14,58,251433,2020,0,'30250000','2021-01-18 08:18:12',NULL,NULL),
	(69,15,58,161830,2020,9335000,'28600000','2021-01-18 08:18:12',NULL,NULL),
	(70,16,58,33984,2020,0,'52000000','2021-01-18 08:18:12',NULL,NULL),
	(71,17,58,2190,2020,0,'91125000','2021-01-18 08:18:12',NULL,NULL),
	(72,18,58,23452,2020,156486961,'250000000','2021-01-18 08:18:12',NULL,NULL),
	(73,19,58,56164,2020,61999840,'90000000','2021-01-18 08:18:12',NULL,NULL),
	(74,45,219,32001,2020,2147483647,'3080870000','2021-01-18 11:31:20',NULL,NULL),
	(75,58,219,32001,2020,88267400,'38828215000','2021-01-18 11:31:20',NULL,NULL),
	(76,3,34,1,2020,20033510000,'210595750000','2021-01-19 10:16:46','2021-01-29 16:32:31',NULL),
	(77,4,34,5,2020,85532751141,'485690028280','2021-01-19 10:16:46','2021-01-29 16:34:44',NULL),
	(78,5,34,1,2020,7703908000,'316332685290','2021-01-19 10:16:46','2021-01-29 16:33:52',NULL),
	(79,38,34,5,2020,51033837897,'404165190150','2021-01-19 10:16:46','2021-01-29 16:34:09',NULL),
	(80,8,70,0,2020,344560000,'0','2021-01-19 13:41:38','2021-01-20 09:56:27',NULL),
	(81,9,70,0,2020,2259396000,'0','2021-01-19 13:41:38','2021-01-26 14:04:17',NULL),
	(82,10,70,0,2020,344560000,'0','2021-01-19 13:41:38','2021-01-20 10:02:14',NULL),
	(83,11,70,0,2020,344560000,'0','2021-01-19 13:41:38','2021-01-20 10:11:30',NULL),
	(84,12,70,0,2020,344560000,'0','2021-01-19 13:41:38','2021-01-20 10:14:28',NULL),
	(85,13,70,0,2020,114975000,'0','2021-01-19 13:41:38','2021-01-20 10:15:14',NULL),
	(86,14,70,0,2020,344560000,'0','2021-01-19 13:41:38','2021-01-20 10:15:40',NULL),
	(87,15,70,0,2020,167400000,'0','2021-01-19 13:41:38','2021-01-20 10:16:27',NULL),
	(88,16,70,0,2020,167400000,'0','2021-01-19 13:41:38','2021-01-20 10:17:03',NULL),
	(89,17,70,0,2020,200000000,'0','2021-01-19 13:41:38','2021-01-20 10:17:32',NULL),
	(90,18,70,0,2020,271325250,'0','2021-01-19 13:41:38','2021-01-21 13:53:58',NULL),
	(91,19,70,0,2020,282174750,'0','2021-01-19 13:41:38','2021-01-21 13:55:53',NULL),
	(92,37,162,672,2020,144080000,'675890000','2021-01-19 14:37:01','2021-02-01 07:52:06',NULL),
	(93,40,162,94,2020,85250000,'507140000','2021-01-19 14:37:01','2021-02-01 07:52:36',NULL),
	(94,41,162,38,2020,6300000,'63620000','2021-01-19 14:37:01','2021-02-01 07:53:04',NULL),
	(95,42,162,324,2020,56450000,'489760000','2021-01-19 14:37:01','2021-02-01 07:53:37',NULL),
	(96,43,162,47,2020,20920000,'163590000','2021-01-19 14:37:01','2021-02-01 07:54:07',NULL),
	(97,45,144,120,2020,14975633000,'6000000000','2021-01-19 14:50:58','2021-01-27 10:15:20',NULL),
	(98,58,144,0,2020,6886911000,'0','2021-01-19 14:50:58','2021-01-25 14:11:04',NULL),
	(99,49,126,0,2020,0,'0','2021-01-19 14:56:50',NULL,NULL),
	(100,51,126,0,2020,0,'0','2021-01-19 14:56:50',NULL,NULL),
	(101,53,182,100,2020,9835551000,'55553634000','2021-01-19 15:04:10','2021-02-01 14:16:28',NULL),
	(102,54,182,177,2020,95000000,'950000000','2021-01-19 15:04:10','2021-01-28 11:33:26',NULL),
	(103,55,182,1000,2020,225000000,'2700000000','2021-01-19 15:04:10','2021-01-28 11:33:52',NULL),
	(104,56,182,362,2020,1852505000,'11898285000','2021-01-19 15:04:10','2021-02-01 08:41:39',NULL),
	(105,57,182,32,2020,440000000,'6551347000','2021-01-19 15:04:10','2021-01-21 09:54:44',NULL),
	(106,45,255,233210,2020,2147483647,'51984552900','2021-01-20 12:30:15',NULL,NULL),
	(107,58,255,233210,2020,1000000000,'1273800000','2021-01-20 12:30:15',NULL,NULL),
	(108,49,205,0,2020,0,'0','2021-01-20 12:42:52',NULL,NULL),
	(109,51,205,0,2020,1365000000,'0','2021-01-20 12:42:52',NULL,NULL),
	(110,37,228,500,2020,80702000,'618729000','2021-01-21 09:32:01',NULL,NULL),
	(111,40,228,500,2020,66243120,'457893000','2021-01-21 09:32:01',NULL,NULL),
	(112,41,228,500,2020,0,'37830500','2021-01-21 09:32:01',NULL,NULL),
	(113,42,228,500,2020,29319500,'506633500','2021-01-21 09:32:01',NULL,NULL),
	(114,43,228,500,2020,217884400,'1532153700','2021-01-21 09:32:01',NULL,NULL),
	(115,3,29,186497,2020,1074773700,'11574330000','2021-01-25 08:28:29',NULL,NULL),
	(116,4,29,600571,2020,103306463888,'451998630000','2021-01-25 08:28:29',NULL,NULL),
	(117,5,29,1175200,2020,1632494060,'27730000000','2021-01-25 08:28:29',NULL,NULL),
	(118,38,29,299502,2020,46969111208,'247347225000','2021-01-25 08:28:29',NULL,NULL),
	(119,37,229,70,2020,100000000,'100000000','2021-01-25 10:20:20',NULL,NULL),
	(120,40,229,300,2020,22600000,'22600000','2021-01-25 10:20:20',NULL,NULL),
	(121,41,229,100,2020,10218000,'10218000','2021-01-25 10:20:20',NULL,NULL),
	(122,42,229,400,2020,18800000,'18800000','2021-01-25 10:20:20',NULL,NULL),
	(123,43,229,300,2020,39470392,'39470392','2021-01-25 10:20:20',NULL,NULL),
	(124,37,163,37,2020,36,'36','2021-01-26 14:53:04',NULL,NULL),
	(125,40,163,476,2020,476,'476','2021-01-26 14:53:04',NULL,NULL),
	(126,41,163,52,2020,52,'52','2021-01-26 14:53:04',NULL,NULL),
	(127,42,163,282,2020,282,'282','2021-01-26 14:53:04',NULL,NULL),
	(128,43,163,362,2020,362,'362','2021-01-26 14:53:04',NULL,NULL),
	(129,53,107,600,2020,6081213382,'36193771198','2021-01-27 11:22:32',NULL,NULL),
	(130,54,107,0,2020,0,'0','2021-01-27 11:22:32',NULL,NULL),
	(131,55,107,0,2020,0,'0','2021-01-27 11:22:32',NULL,NULL),
	(132,56,107,0,2020,0,'0','2021-01-27 11:22:32',NULL,NULL),
	(133,57,107,75,2020,6859257180,'36193771198','2021-01-27 11:22:32',NULL,NULL),
	(134,45,211,2849447000,2020,21341748000,'25650000000','2021-01-27 11:48:49',NULL,NULL),
	(135,58,211,250000000,2020,1635000000,'423000000','2021-01-27 11:48:49',NULL,NULL),
	(136,53,201,0,2020,0,'0','2021-01-28 08:45:13',NULL,NULL),
	(137,54,201,0,2020,0,'964241000','2021-01-28 08:45:13',NULL,NULL),
	(138,55,201,250,2020,34415000,'385695000','2021-01-28 08:45:13',NULL,NULL),
	(139,56,201,0,2020,304660000,'2111025000','2021-01-28 08:45:13',NULL,NULL),
	(140,57,201,0,2020,0,'0','2021-01-28 08:45:13',NULL,NULL),
	(141,32,94,11301,2020,17405317000,'52215951000','2021-01-28 10:57:42','2021-02-01 08:31:04',NULL),
	(142,33,94,8520,2020,14392955000,'43178865000','2021-01-28 10:57:42','2021-02-01 08:32:14',NULL),
	(143,34,94,5385,2020,14182341000,'42547023000','2021-01-28 10:57:42','2021-01-29 14:28:58',NULL),
	(144,35,94,1950,2020,5920550000,'17761650000','2021-01-28 10:57:42','2021-01-29 14:30:28',NULL),
	(145,36,94,1,2020,1375617000,'4126851000','2021-01-28 10:57:42','2021-02-01 08:34:08',NULL),
	(146,8,87,8139,2020,194140000,'4785203000','2021-01-28 10:28:49',NULL,NULL),
	(147,9,87,6149,2020,165636000,'3360425000','2021-01-28 10:28:49',NULL,NULL),
	(148,10,87,7535,2020,165636000,'3360425000','2021-01-28 10:28:49',NULL,NULL),
	(149,11,87,29810,2020,17556000,'517915000','2021-01-28 10:28:49',NULL,NULL),
	(150,12,87,86617,2020,0,'297824000','2021-01-28 10:28:49',NULL,NULL),
	(151,13,87,360908,2020,13841000,'66948000','2021-01-28 10:28:49',NULL,NULL),
	(152,14,87,89097,2020,53564000,'154357000','2021-01-28 10:28:49',NULL,NULL),
	(153,15,87,23019,2020,19246000,'468137000','2021-01-28 10:28:49',NULL,NULL),
	(154,16,87,10833,2020,19246000,'468137000','2021-01-28 10:28:49',NULL,NULL),
	(155,17,87,829,2020,26500000,'71721000','2021-01-28 10:28:49',NULL,NULL),
	(156,18,87,1044,2020,19955000,'196438000','2021-01-28 10:28:49',NULL,NULL),
	(157,19,87,11940,2020,225102000,'2030753000','2021-01-28 10:28:49',NULL,NULL),
	(158,52,97,100,2020,25000000,'100','2021-01-28 12:55:24',NULL,NULL),
	(159,49,206,0,2020,0,'0','2021-01-28 13:15:41',NULL,NULL),
	(160,51,206,0,2020,0,'0','2021-01-28 13:15:41',NULL,NULL),
	(161,6,57,111541,2020,6867295000,'0','2021-01-29 09:01:37',NULL,NULL),
	(162,7,57,152961,2020,0,'0','2021-01-29 09:01:37',NULL,NULL),
	(163,53,169,1,2020,95425000,'0','2021-01-29 18:48:45','2021-01-30 13:49:14',NULL),
	(164,54,169,1,2020,309654000,'0','2021-01-29 18:48:45',NULL,NULL),
	(165,55,169,1,2020,1030230295,'-2','2021-01-29 18:48:45',NULL,NULL),
	(166,56,169,1,2020,1539625755,'0','2021-01-29 18:48:45',NULL,NULL),
	(167,57,169,1,2020,7569654000,'0','2021-01-29 18:48:45','2021-01-30 13:39:43',NULL),
	(168,37,166,250,2020,18816450,'1331496000','2021-01-30 11:01:10',NULL,NULL),
	(169,40,166,1000,2020,77066200,'652860008','2021-01-30 11:01:10',NULL,NULL),
	(170,41,166,1150,2020,3925250,'287981000','2021-01-30 11:01:10',NULL,NULL),
	(171,42,166,6050,2020,77066200,'322190000','2021-01-30 11:01:10',NULL,NULL),
	(172,43,166,500,2020,20669150,'212619000','2021-01-30 11:01:10',NULL,NULL),
	(173,49,245,0,2020,0,'0','2021-02-01 13:45:16',NULL,NULL),
	(174,51,245,0,2020,0,'0','2021-02-01 13:45:16',NULL,NULL);

/*!40000 ALTER TABLE `datadasar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dokumen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dokumen`;

CREATE TABLE `dokumen` (
  `id_dokumen` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `latar_belakang` text,
  `dasar_hukum` text,
  `kebijakan_umum` text,
  `arah_kebijakan` text,
  `bidang_urusan` text,
  `jenis_pelayanan` text,
  `target_pencapaian` text,
  `realisasi` text,
  `alokasi_anggaran` text,
  `dukungan_personil` text,
  `permasalahan_solusi` text,
  `program_kegiatan` text,
  `penutup` text,
  `status` int(11) DEFAULT NULL,
  `creadted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_dokumen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `dokumen` WRITE;
/*!40000 ALTER TABLE `dokumen` DISABLE KEYS */;

INSERT INTO `dokumen` (`id_dokumen`, `id_user`, `tahun`, `latar_belakang`, `dasar_hukum`, `kebijakan_umum`, `arah_kebijakan`, `bidang_urusan`, `jenis_pelayanan`, `target_pencapaian`, `realisasi`, `alokasi_anggaran`, `dukungan_personil`, `permasalahan_solusi`, `program_kegiatan`, `penutup`, `status`, `creadted_at`, `updated_at`, `deleted_at`)
VALUES
	(1,22,2020,'<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','Pendidikan','<p>Pendidikan Menengah Atas,Pendidikan Khusus,Pendidikan Anak Usia Dini,Pendidikan Dasar,Pendidikan Kesetaraan,Pendidikan Menangah Pertama,Pendidikan Menengah Kejuruan,</p>\r\n<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','2020','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>','<div>\r\n<div>$id = $this-&gt;session-&gt;userdata(\'id_pengguna\');</div>\r\n</div>',NULL,NULL,NULL,NULL,NULL),
	(2,22,2020,'<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','Pendidikan','<p>Pendidikan Menengah Atas,Pendidikan Khusus,Pendidikan Anak Usia Dini,Pendidikan Dasar,Pendidikan Kesetaraan,Pendidikan Menangah Pertama,Pendidikan Menengah Kejuruan,</p>\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',NULL,NULL,NULL,'2021-02-04 09:30:52',NULL),
	(3,22,2020,'<p><strong>AAALorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','Pendidikan','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Pendidikan Menengah Atas,Pendidikan Khusus,Pendidikan Anak Usia Dini,Pendidikan Dasar,Pendidikan Kesetaraan,Pendidikan Menangah Pertama,Pendidikan Menengah Kejuruan,</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',NULL,NULL,'2021-02-04 12:31:04',NULL);

/*!40000 ALTER TABLE `dokumen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table indikator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `indikator`;

CREATE TABLE `indikator` (
  `id_indikator` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `id_jenis_pelayanan_dasar` int(11) NOT NULL,
  `id_jenis_spm` int(11) NOT NULL,
  `jenis_indikator` text,
  `sasaran` text,
  `cara_hitung` text,
  `pembilang` text,
  `penyebut` text,
  `kualifikasi` text,
  `akses` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '0 = aktif, 1 = tidak aktif',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '0 = hapus, 1 = dihapus',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_indikator`),
  KEY `idjnspeldasindikator` (`id_jenis_pelayanan_dasar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `indikator` WRITE;
/*!40000 ALTER TABLE `indikator` DISABLE KEYS */;

INSERT INTO `indikator` (`id_indikator`, `parent_id`, `id_jenis_pelayanan_dasar`, `id_jenis_spm`, `jenis_indikator`, `sasaran`, `cara_hitung`, `pembilang`, `penyebut`, `kualifikasi`, `akses`, `id_user`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(52,0,1,2,'Sekolah Menengah Atas','Anak usia 16 - 18','Jumlah sasaran yang sudah tamat atau sedang balajar / jumlah sasaran','Jumlah sasaran yang sudah tamat atau sedang balajar','jumlah Anak usia 16 - 18','',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(53,0,39,2,'Sekolah Menengah Kejuruan','Anak umur 16 - 18 Tahun','Jumlah Anak umur 16 - 18 Tahun yang sudah tamat atau sedang belajar / Jumalh Anak umur 16 - 18 Tahun','Jumlah Anak umur 16 - 18 Tahun yang sudah tamat atau sedang belajar','Jumalh Anak umur 16 - 18 Tahun',NULL,88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(54,0,2,2,'Pendidikan Khusus (SLB)','Anak Usia 4 - 18 Tahun','Jumlah Anak Usia 4 - 18 Tahun yang sudah tamat atau sedang belajar / Jumlah Anak Usia 4 - 18 Tahun','Jumlah Anak Usia 4 - 18 Tahun yang sudah tamat atau sedang belajar','Jumlah Anak Usia 4 - 18 Tahun',NULL,88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(55,0,3,2,'Pendidikan Anak Usia Dini','Anak usia dini 5 - 6 tahun','Jumlah anak usia dini  yang sudah tamat atau sedang belajar PAUD / Jumlah anak usia dini','Jumlah anak usia dini  yang sudah tamat atau sedang belajar PAUD','Jumlah anak usia dini pada kabupaten/kota bersangkutan',NULL,77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(56,0,4,2,'Pendidikan pada sekolah dasar','anak usia 7 - 12 tahun','anak usia 7 - 12 tahun yang sudah atau sednag belajar / anak usia 7 - 12 tahun di kota berasangkutan','anak usia 7 - 12 tahun yang sudah atau sednag belajar ','anak usia 7 - 12 tahun',NULL,77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(57,0,38,2,'Pendidikan pada sekolah menengah pertama','anak usia 12 - 15 tahun','anak usia 12 - 15 tahun yang sudah atau sedang balajar / anak usia 12 - 15 tahun di kota berangkutan','anak usia 12 - 15 tahun yang sudah atau sedang balajar','anak usia 12 - 15 tahun',NULL,77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(58,0,5,2,'Pendidikan pada kesetaraan','anak usia 7 - 18 ','anak usia 7 - 18 yang sedang atau sudah belajar pada pendidikan kesetaraan / anak usia 7 - 18 tahun pada kota bersangkutan','anak usia 7 - 18 yang sedang atau sudah belajar pada pendidikan kesetaraan ','anak usia 7 - 18 ','',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(59,0,6,3,'Pelayanan kesehatan bagi penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana',' penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana','penduduk terdampak krisis kesehatan akibat bencana dan atau berpotensi bencana yang mendapatkan pelayanan dalam kurun waktu satu tahun /  penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana dalam kurun waktu satu tahun X 100%                               \r\n                                ','penduduk terdampak krisis kesehatan akibat bencana dan atau berpotensi bencana yang mendapatkan pelayanan dalam kurun waktu satu tahun ','penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana dalam kurun waktu satu tahun','                                 \r\n                                ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(60,0,7,3,'Kesehatan bagi penduduk yang terdampak dan beresiko pada situasi kondisi luar biasa Provinsi','penduduk yang terdampak dan beresiko pada situasi kondisi luar biasa Provinsi','Jumlah penduduk yang terdampak dan beresiko pada situasi kondisi luar biasa Provinsi yang mendapat pelayanan kesehatan sesuai standar /  penduduk yang terdampak dan beresiko pada situasi kondisi luar biasa Provinsi * 100                               \r\n                                ','Jumlah penduduk yang terdampak dan beresiko pada situasi kondisi luar biasa Provinsi yang mendapat pelayanan kesehatan sesuai standar ','penduduk yang terdampak dan beresiko pada situasi kondisi luar biasa Provinsi','                                 \r\n                                ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(61,0,8,3,'Pelayanan kesehatan ibu hamil','ibu hamil di suatu kabupaten/kota ','Jumlah ibu hamil di suatu kabupaten/kota yang mendapat pelayanan /   jumlah ibu hamil di suatu kabupaten/kota                             \r\n                                ','Jumlah ibu hamil di suatu kabupaten/kota yang mendapat pelayanan ','jumlah ibu hamil di suatu kabupaten/kota ','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(62,0,9,3,'Ibu bersalin mendapatkan pelayanan','ibu bersalin di wilayah kerja kabupaten / kota bersangkutan','Jumlah ibu bersalin mendapat pelayanan /  jumlah ibu bersalin                    \r\n                                ','Jumlah ibu bersalin mendapat pelayanan di wilayah kerja kabupaten / kota bersangkutan','jumlah ibu bersalin di wilayah kerja kabupaten / kota bersangkutan','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(63,0,10,3,'Pelayanan untuk bayi baru lahir ','Bayi baru lahir di kabupaten atau kota bersangkutan','Jumlah bayi baru lahir mendapat pelayanan   /    Jumlah bayi baru lahir                 ','Jumlah bayi baru lahir mendapat pelayanan','Jumlah bayi baru lahir','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(64,0,11,3,'Pelayanan kesehatan balita','Balita 12-59 bulan','Jumlah balita usia 12 -  59 bulan yang mendapatkan pelayanan kesehatan /  Jumlah balita usia 12 -  59 bulan                               \r\n                                ','Jumlah balita usia 12 -  59 bulan yang mendapatkan pelayanan kesehatan','Jumlah balita usia 12 -  59 bulan','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(65,0,12,3,'Pelayanan Kesehatan untuk anak usia pendidikan dasar','anak usia pendidikan dasar','Jumlah anak usia pendidikan dasar yang mendapat pelayanan kesehatan     / Jumlah anak usia pendidikan dasar                            \r\n                                ','Jumlah anak usia pendidikan dasar yang mendapat pelayanan kesehatan','Jumlah anak usia pendidikan dasar','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(66,0,13,3,'Pelayanan orang usia 15 - 59 ','Orang usia 15 - 59','Jumlah orang usia 15 - 59 tahun di kabupaten atau kota yang mendapat pelayanan skrining kesehatan sesuai standart   /  Jumlah orang usia 15 - 59 tahun                               \r\n                                ','Jumlah orang usia 15 - 59 tahun di kabupaten atau kota yang mendapat pelayanan skrining kesehatan sesuai standart','Jumlah orang usia 15 - 59 tahun ','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(67,0,14,3,'Pelayanan kesehatan warga negara usia 60 tahun keatas','warga negara usia 60 tahun keatas','Jumlah warga negara usia 60 tahun keatas mendapat pelayanan   /  Jumlah warga negara usia 60 tahun keatas                            \r\n                                ','Jumlah warga negara usia 60 tahun keatas mendapat pelayanan','Jumlah warga negara usia 60 tahun keatas','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(68,0,15,3,'Pelayanan kesehatan terhadap penderita hipertensi','penderita hipertensi usia &gt;= 15 tahun','Jumlah penderita hipertensi mendapat pelayanan /  Jumlah penderita hipertensi                               \r\n                                ','Jumlah penderita hipertensi mendapat pelayanan','Jumlah penderita hipertensi','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(69,0,17,3,'Pelayanan kesehatan kepada ODGJ','ODGJ','Jumlah ODGJ mendapat pelayanan   / Jumlah ODGJ                         \r\n                                ','Jumlah ODGJ mendapat pelayanan','Jumlah ODGJ ','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(70,0,18,3,'Pelayanan kesehatan kepada orang terduga TBC','Orang terduga TBC',' Jumlah orang terduga TBC mendapat pelayanan /  Jumlah orang terduga TBC                         \r\n                                ','Jumlah orang terduga TBC mendapat pelayanan','Jumlah orang terduga TBC','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(71,0,19,3,'Pelayanan kesehatan terhadap orang dengan resiko HIV','orang dengan resiko HIV','Jumlah orang dengan resiko HIV yang mendapat pelayanan / Jumlah orang dengan resiko HIV','Jumlah orang dengan resiko HIV yang mendapat pelayanan','Jumlah orang dengan resiko HIV ','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(72,0,16,3,'Pelayanan kesehatan terhadap orang penderita diabetes melitus','penderita diabetes melitus','Pelayanan kesehatan terhadap orang penderita diabetes melitus / Jumlah estimasi penderita diabetes melitus','Pelayanan kesehatan terhadap orang penderita diabetes melitus','Jumlah estimasi penderita diabetes melitus\n',NULL,77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(73,0,32,7,'Persentase (%) penyandang disabilitas terlantar yang terpenuhi kebutuhan dasarnya di dalam panti','Terpenuhinya kebutuhan dasar penyandang disabilitas terlantar di dalam panti','Pembilang dibagi penyebut  dikali 100%                                \r\n                                ','jumlah penyandang disabilitas terlantar di salam panti yang terpenuhi kebutuhan dasarnya','populasi penyandang disabilitas terlantar di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti','Jumlah penyandang disabilitas terlantar di dalam panti yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi penyandang disablitas terlantar di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti dikali 100% (seratus persen)               ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(75,0,33,7,'Persentase (%) anak terlantar yang terpenuhi kebutuhan dasarnya di dalam panti','Terpenuhinya kebutuhan dasar anak terlantar di dalam panti','Pembilang dibagi penyebut dikali 100%                                 \r\n                                ','jumlah anak terlantar di dalam panti yang terpenuhi kebutuhan dasarnya','populasi anak terlantar di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti','Jumlah anak terlantar di dalam panti yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi anak  terlantar di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti dikali 100% (seratus persen)               ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(76,0,34,7,'Persentase (%) lanjut usia terlantar yang terpenuhi kebutuhan dasarnya di dalam panti','Terpenuhinya kebutuhan dasar lanjut usia terlantar di dalam panti','Pembilang dibagi penyebut dikali 100%                      ','jumlah lanjut usia terlantar di dalam panti yang terpenuhi kebutuhan dasarnya','populasi lanjut usia terlantar di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti','Jumlah lanjut usia terlantar di dalam panti yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi lanjut usia  terlantar di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti dikali 100% (seratus persen)             ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(77,0,35,7,'Persentase (%) gelandangan dan pengemis yang terpenuhi kebutuhan dasarnya di dalam panti','Terpenuhinya kebutuhan gelandangan dan pengemis di dalam panti','Pembilang dibagi penyebut dikali 100%     ','jumlah gelandangan dan pengemis di dalam panti yang terpenuhi kebutuhan dasarnya','populasi gelandangan dan pengemis di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti','Jumlah gelandangan dan pengemis di dalam panti yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi gelandangan dan pengemis di daerah provinsi yang membutuhkan rehabilitasi sosial dasar di dalam panti dikali 100% (seratus persen)  ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(78,0,36,7,'Persentase (%) korban bencana alam dan sosial yang terpenuhi kebutuhan dasarnya pada saat dan setelah tanggap darurat bencana provinsi','Terpenuhinya kebutuhan dasar korban bencana alam dan sosial provinsi','Pembilang dibagi penyebut dikali 100% ','jumlah korban bencana alam dan sosial  provinsi yang terpenuhi kebutuhan dasarnya','populasi korban bencana alam dan sosial di daerah provinsi yang membutuhkan perlindungan dan jaminan sosial  pada saat dan setelah tanggap darurat bencana provinsi','Jumlah korban bencana alam dan sosial yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi korban bencana alam dan sosial di daerah provinsi yang membutuhkan perlindungan dan jaminan sosial pada saat dan setelah  tanggap darurat bencana provinsi dikali 100% (seratus persen)     ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(79,0,40,7,'Persentase (%) penyandang disabilitas terlantar yang terpenuhi kebutuhan dasarnya di luar panti','Terpenuhinya kebutuhan dasar penyandang disabilitas terlantar di luar panti','Pembilang dibagi penyebut dikali 100%                  ','jumlah penyandang disabilitas terlantar yang terpenuhi kebutuhan dasarnya diluar panti','populasi penyandang disabilitas terlantar di daerah kabupaten /kota  yang membutuhkan rehabilitasi sosial dasar di luar panti','Jumlah penyandang disabilitas terlantar yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi penyandang disablitas terlantar di daerah kabupaten /kota yang membutuhkan rehabilitasi sosial dasar di luar panti dikali 100% (seratus persen)       ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(80,0,41,7,'Persentase (%) anak terlantar yang terpenuhi kebutuhan dasarnya di luar panti','Terpenuhinya kebutuhan dasar anak terlantar di luar panti','Pembilang dibagi penyebut dikali 100%      ','jumlah anak terlantar yang terpenuhi kebutuhan dasarnya diluar panti','populasi anak terlantar di daerah kabupaten /kota  yang membutuhkan rehabilitasi sosial dasar di luar panti','Jumlah anak terlantar yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi anak terlantar di daerah kabupaten /kota yang membutuhkan rehabilitasi sosial dasar di luar panti dikali 100% (seratus persen)                                      ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(81,0,42,7,'Persentase (%) lanjut usia terlantar yang terpenuhi kebutuhan dasarnya di luar panti','Terpenuhinya kebutuhan dasar lanjut usia terlantar di luar panti','Pembilang dibagi penyebut dikali 100%','jumlah lanjut usia terlantar yang terpenuhi kebutuhan dasarnya diluar panti','populasi lanjut usia terlantar di daerah kabupaten /kota  yang membutuhkan rehabilitasi sosial dasar di luar panti','Jumlah lanjut usia terlantar yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi lanjut usia terlantar di daerah kabupaten /kota yang membutuhkan rehabilitasi sosial dasar di luar panti dikali 100% (seratus persen)                                  ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(82,0,43,7,'Persentase (%) gelandangan dan pengemis yang terpenuhi kebutuhan dasarnya di luar panti','Terpenuhinya kebutuhan gelandangan dan pengemis di luar panti','Pembilang dibagi penyebut dikali 100%','jumlah gelandangan dan pengemis yang terpenuhi kebutuhan dasarnya diluar panti','populasi gelandangan dan pengemis di daerah kabupaten /kota  yang membutuhkan rehabilitasi sosial dasar di luar panti','Jumlah gelandangan dan pengemis yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi gelandangan dan pengemis di daerah kabupaten /kota yang membutuhkan rehabilitasi sosial dasar di luar panti dikali 100% (seratus persen)                                     ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(83,0,37,7,'Persentase (%)korban bencana alam dan sosial yang terpenuhi kebutuhan dasarnya pada saat dan setelah tanggap darurat bencana daerah kabupaten /kota  ','Terpenuhinya kebutuhan dasar korban bencana alam dan sosial kabupaten /kota  ','Pembilang dibagi penyebut dikali 100%              ','jumlah korban bencana alam dan sosial  daerah kabupaten /kota yang terpenuhi kebutuhan dasarnya','populasi korban bencana alam dan sosial di daerah kabupaten /kota yang membutuhkan perlindungan dan jaminan sosial  pada saat dan setelah tanggap darurat bencana daerah kabupaten /kota  ','Jumlah korban bencana alam dan sosial yang terpenuhi kebutuhan dasarnya selama 1 (satu) tahun anggaran dibagi populasi korban bencana alam dan sosial di daerah kabupaten /kota yang membutuhkan rehabilitasi sosial dasar di luar panti dikali 100% (seratus persen)                                    ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(85,0,24,4,'Persentase kapasitas yang dapat terlayani melalui penyaluran air minum curah lintas kabupaten/kota','Warga memeperoleh kebutuhan air minum curah lintas kabupaten/kota','Pembilang dibagi penyebut dikali 100%                             \r\n                                ','Jumlah kumulatif kapasitas yang dapat terlayani melalui penyaluran air minum curah lintas kabupaten/kota ','Jumlah kumulatif demand pemenuhan kapasitas yang memerlukan pelayanan air minum curah lintas kabupaten/kota di provinsi yang bersangkutan','Jumlah warga negara yang memeperoleh kebutuhan air minum curah lintas kabupaten/kota                               \r\n                                ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(86,0,26,4,'Persentase jumlah penduduk yang mendapatkan akses terhadap air minum lelalui SPAM dengan jaringan perpipaan dan bukan jaringan terlindungi','Warga memperoleh kebutuhan pokok air minum sehari-hari','Pembilang dibagi penyebut dikali 100%           ','Masyarakat terlayani pada tahun-n penerapan SPM adalah jumlah kumulatif masyarakat yang mendapatkan akses terhadap air minum melalui SPAM dengan jaringan perpipaan dan bukan jaringan perpipaan terlindungi di dalam sebuah kabupaten/kota pada tahun-n penerapan SPM.','Proyeksi total masyarakat pada tahun ke-n penerapan SPM adalah jumlah total proyeksi masyarakat di seluruh kabupaten/kota tersebut pada tahun ke-n penerapan SPM.','Jumlah warga negara yag memeperoleh kebutuhan pokok air minum sehari-hari',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(87,0,27,4,'Rumah yang memiliki akses pengelolaan air limbah domestk berupa cubluk','Warga memperoleh layanan pengolahan air limbah domestik','Pembilang dibagi penyebut dikali 100%                      ','Jumlah rumah yang memiliki akses pengolahan berupa cubluk','Jumlah rumah di wilayah pengembangan SPALDS dgn kepadatan penduduk pada wilayah terbangun &lt; 25 jiwa/Ha','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik   ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(88,0,27,4,'Rumah yang lumpur tinja-nya telah diolah di IPLT','Warga memperoleh layanan pengolahan air limbah domestik','Pembilang dibagi penyebut dikali 100%           ','Jumlah rumah yang lumpur tinja-nya telah diolah di IPLT	','Jumlah rumah di wilayah pengembangan SPALDS dgn kepadatan penduduk pada wilayah terbangun &gt; 25 jiwa/Ha','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik                    ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(89,0,27,4,'Rumah yag memiliki sambungan rumah dan air limbah nya diolah di IPALD','Warga memperoleh layanan pengolahan air limbah domestik','Pembilang dibagi penyebut dikali 100%              ','Jumlah rumah yag memiliki sambungan rumah dan air limbah nya diolah di IPALD','Jumlah rumah di wiayah pengembangan SPALD-T','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik                             ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(90,0,25,4,'Rumah yang memiliki akses pengelolaan air limbah domestk berupa cubluk, lumpur tinja diolah di IPLT, dan memiliki sambungan rumah dan air limbah nya diolah di IPALD','Warga memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota','Pembilang dibagi penyebut dikali 100%                        ','Jumlah rumah yang memiliki akses pengolahan berupa cubluk, lumpur tinja diolah di IPLT, dan memiliki sambungan rumah dan air limbah nya diolah di IPALD','Jumlah rumah yag ada di kabupaten / kota','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota                  ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(91,0,52,6,'Presentase jumlah penduduk yang memperoleh Pelayanan ketentraman dan ketertiban umum provinsi','Terpenuhinya Pelayanan ketentraman dan ketertiban umum provinsi','Pembilang dibagi penyebut dikali 100%','jumlah penduduk  yang menerima Pelayanan ketentraman dan ketertiban umum provinsi','Seluruh penduduk yang terkena dampak gangguan trantibum akibat penegakan hukum terhadap pelanggaran perda provinsi','Warga negara yang terkena dampak gangguan trantibum akibat penegakan hukum terhadap pelanggaran perda provinsi dan kabupaten/kota serta perkada  ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(92,0,53,6,'Presentase jumlah penduduk yang memperoleh layanan akibat dari penegakan hukum Perda dan Perkada','Setiap Warga Negara yang terkena dampak ganguan Trantibum akibat penegakan hukum akibat pelanggaran Perda Kabupaten/Kota dan Perkada','Pembilang dibagi penyebut dikali 100%','Jumlah dan identitas Warga Negara yang terkena dampak gangguan trantibum akibat penegakan hukum akibat pelanggaran Perda Kabupaten/kota dan Perda serta mengalami kerugian materi dan/atau terkena cedera fisik','Jumlah Warga Negara yang terkena dampak ganguan Trantibum akibat penegakan hukum akibat pelanggaran Perda Kabupaten/Kota dan Perkada','Warga negara yang terkena dampak gangguan trantibum akibat penegakan hukum terhadap pelanggaran perda kabupaten/kota serta perkada               ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(100,0,55,6,'Persentase (%) jumlah warga negara yang memperoleh layanan pencegahan dan kesiapsiagaan terhadap bencana','Setiap Warga Negara yang berada di kawasan rawan bencana dan yang menjadi  korban bencana','Pembilang dibagi penyebut dikali 100%         ','Jumlah warga negara yang menjadi korban bencana Daerah Kabupaten Kota','jumlah Warga Negara yang berada di kawasan rawan bencana tingkat Kab./Kota','Warga negara yang berada di kawasan rawan bencana dan yang menjadi korban bencana untuk jenis pelayanan dasar pelayanan pencegahan dan kesiasiagaan terhadap bencana         ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(104,0,56,6,'Persentase (%) jumlah Warga Negara yang memperoleh layanan penyelamatan dan evakuasi korban bencana','Setiap Warga Negara yang berada di kawasan rawan bencana dan yang menjadi  korban bencana','Pembilang dibagi penyebut dikali 100%	                   ','Jumlah Warga Negara yang memperoleh layanan penyelamatan dan evakuasi korban bencana tingkat Kab./Kota','jumlah Warga Negara yang berada di kawasan rawan bencana tingkat Kab./Kota','Warga negara yang berada di kawasan rawan bencana dan yang menjadi korban bencana untuk jenis pelayanan dasar pelayanan penyelamatan dan evakuasi korban bencana.                    ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(105,0,48,5,'Presentase jumlah Warga Negara yang terkena relokasi akibat program pemerintah daerah provinsi yang memperoleh fasilitas penyediaan rumah layak huni','Terpenuhinya jumlah unit rumah yang layak huni bagi masyarakat yang terkena relokasi program pemerintah daerah provinsi','Pembilang dibagi penyebut dikali 100%                         \r\n                                ','Rumah Tangga penerima Fasilitasi penggantian hak atas penguasaan tanah dan /atau bangunan + Rumah Tangga penerima  Subsidi uang sewa + Rumah Tangga penerima Penyediaan rumah layak huni','Jumlah total rumah tangga terkena relokasi program pemerintah daerah  yang memenuhi kriteria penerima pelayanan','Jumlah rumah tangga terkena korban bencana daerah provinsi yang memenuhi kriteria penerima pelayanan                          \r\n                                ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(106,0,50,5,'Presentase jumlah Warga Negara korban bencana yang memperoleh rumah layak huni','Terpenuhinya Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana provinsi','Pembilang dibagi penyebut dikali 100%                         ','Jumlah unit rumah korban bencana yang ditangani pada tahun n','Jumlah total rencana unit rumah korban bencana yang akan ditangani pada tahun n','Jumlah rumah tangga terkena relokasi program pemerintah daerah provinsi yang memenuhi kriteria penerima pelayanan                ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(107,0,49,5,'Presentase jumlah Warga Negara korban bencana yang memperoleh rumah layak huni','Terpenuhinya Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana kabupaten/kota','Pembilang dibagi penyebut dikali 100%                  ','Jumlah unit rumah korban bencana yang ditangani pada tahun n','Jumlah total rencana unit rumah korban bencana yang akan ditangani pada tahun n','Jumlah rumah tangga terkena korban bencana kabupaten/kota yang memenuhi kriteria penerima pelayanan            ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(108,0,51,5,'Presentase jumlah Warga Negara yang terkena relokasi akibat program pemerintah daerah Kabupaten/Kota yang memperoleh fasilitas penyediaan rumah layak huni','Terpenuhinya jumlah unit rumah yang layak huni bagi masyarakat yang terkena relokasi program pemerintah daerah kabupaten/kota','Pembilang dibagi penyebut dikali 100%                 ','Rumah Tangga penerima Fasilitasi penggantian hak atas penguasaan tanah dan /atau bangunan + Rumah Tangga penerima  Subsidi uang sewa + Rumah Tangga penerima Penyediaan rumah layak huni','Jumlah total rumah tangga terkena relokasi program pemerintah daerah  yang memenuhi kriteria penerima pelayanan','Jumlah rumah tangga terkena relokasi program pemerintah daerah kabupaten/kota yang memenuhi kriteria penerima pelayanan       ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(109,0,59,4,'Presentase jumlah masyarakat/rumah tangga yang memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota','Jumlah rumah tangga lintas Kabupaten/Kota','Pembilang dibagi penyebut dikali 100%               ','Jumlah rumah yang memiliki akses pengolahan berupa cubluk, lumpur tinja diolah di IPLT, dan memiliki sambungan rumah dan air limbah nya diolah di IPALD','Jumlah rumah yang ada di kabupaten / kota','Jumlah warga negara yag memeperoleh kebutuhan pokok air minum sehari-hari            ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(110,0,44,4,'Persentase jumlah rumah tangga yang mendapatkan akses terhadap air minum melalui SPAM jaringan perpipaan dan bukan jaringan perpipaan terlindungi terhadap rumah tangga di seluruh curah lintas Kabupaten/Kot','Jumlah rumah tangga,terutama diprioritaskan pada masyarakat miskin atau tidak mampu dan berdomisili pada daerah rawan air dan akan dilayani melalui sistem penyediaan air minum lintas kabupaten kota','Pembilang dibagi penyebut dikali 100%             ','Jumlah kumulatif masyarakat/rumah tangga yang medapatkan akses terhadap air minum melalui SPAM jaringan perpipaan dan bukan jaringan perpipaan terlindungi dalam sebuah Kab./Kota','Jumlah total proyeksi rumah tangga di seluruh Kab./Kota tersebut','Jumlah warga negara yang memeperoleh kebutuhan air minum curah lintas kabupaten/kota  ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(111,0,46,4,'Rumah yang memiliki akses pengelolaan air limbah domestk berupa cubluk','Warga memperoleh layanan pengolahan air limbah domestik','Pembilang dibagi penyebut dikali 100%                      ','Jumlah rumah yang memiliki akses pengolahan berupa cubluk','Jumlah rumah di wilayah pengembangan SPALDS dgn kepadatan penduduk pada wilayah terbangun &lt; 25 jiwa/Ha','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik            ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(112,0,46,4,'Rumah yang lumpur tinja-nya telah diolah di IPLT','Warga memperoleh layanan pengolahan air limbah domestik','Pembilang dibagi penyebut dikali 100%                   ','Jumlah rumah yang lumpur tinja-nya telah diolah di IPLT','Jumlah rumah di wilayah pengembangan SPALDS dgn kepadatan penduduk pada wilayah terbangun &gt; 25 jiwa/Ha','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik          ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(113,0,46,4,'Rumah yag memiliki sambungan rumah dan air limbah nya diolah di IPALD','Warga memperoleh layanan pengolahan air limbah domestik','Pembilang dibagi penyebut dikali 100%                      ','Jumlah rumah yag memiliki sambungan rumah dan air limbah nya diolah di IPALD','Jumlah rumah di wiayah pengembangan SPALD-T','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik                   ',88,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(114,0,47,4,'Rumah yang memiliki akses pengelolaan air limbah domestk berupa cubluk, lumpur tinja diolah di IPLT, dan memiliki sambungan rumah dan air limbah nya diolah di IPALD','Warga memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota','Pembilang dibagi penyebut dikali 100%	                          ','Jumlah rumah yang memiliki akses pengolahan berupa cubluk, lumpur tinja diolah di IPLT, dan memiliki sambungan rumah dan air limbah nya diolah di IPALD','Jumlah rumah yag ada di kabupaten / kota','Jumlah warga negara yang memperoleh layanan pengolahan air limbah domestik regional lintas kabupaten/kota               ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(115,0,0,0,'','','                                 \r\n                                ','','','                                 \r\n                                ',0,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(116,0,0,0,'','','                                 \r\n                                ','','','                                 \r\n                                ',0,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(119,0,54,6,'Presentase jumlah penduduk dikawasan rawan bencana yang memperoleh informasi rawan bencana sesuai jenis ancaman bencana','Setiap Warga Negara yang memperoleh informasi rawan bencana sesuai jenis ancaman bencana','Pembilang dibagi penyebut dikali 100%                         \r\n                                ','Jumlah Warga Negara yang  memperoleh informasi rawan bencana sesuai jenis ancaman bencana tingkat Kab./Kota','jumlah Warga Negara yang berada di kawasan rawan bencana tingkat Kab./Kota','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(120,0,57,6,'Presentase Warga Negara yang memperoleh layanan penyelamatan dan evakuasi korban kebakaran','Setiap Warga Negara yang mejadi korban kebakaran atau terdampak kebakaran ','Pembilang dibagi penyebut dikali 100%\r\n                                ','Jumlah Warga Negara yang mendapat pelayanan pemadaman,penyelamatan dan evakuasi kebakaran tingkat Kab./Kota','Jumlah Warga Negara yang mejadi korban kebakaran atau terdampak kebakaran tingkat Kab./Kota','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(121,0,45,4,'Persentase jumlah rumah tangga yang mendapatkan akses terhadap air minum melalui SPAM jaringan perpipaan dan bukan jaringan perpipaan terlindungi terhadap rumah tangga di seluruh Kabupaten/Kota','Jumlah rumah tangga,terutama diprioritaskan pada masyarakat miskin atau tidak mampu dan berdomisili pada daerah rawan air dan akan dilayani melalui sistem penyediaan air minum','   Pembilang dibagi penyebut dikali 100%                              \r\n                                ','Jumlah kumulatif masyarakat/rumah tangga yang medapatkan akses terhadap air minum melalui SPAM jaringan perpipaan dan bukan jaringan perpipaan terlindungi dalam sebuah Kab./Kota','Jumlah total proyeksi rumah tangga di seluruh Kab./Kota tersebut','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),
	(122,0,58,4,'Presentase jumlah masyarakat/rumah tangga yang memperoleh layanan pengolahan air limbah domestik','Jumlah rumah tangga seluruh Kabupaten/Kota','Pembilang dibagi penyebut dikali 100%           ','Jumlah rumah yang memiliki akses pengolahan berupa cubluk, lumpur tinja diolah di IPLT, dan memiliki sambungan rumah dan air limbah nya diolah di IPALD','Jumlah rumah yang ada di kabupaten / kota','                                 \r\n                                ',77,5,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `indikator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table informasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informasi`;

CREATE TABLE `informasi` (
  `id_informasi` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deskripsi_info` text,
  `id_akses` int(11) DEFAULT NULL,
  `id_jenis_spm` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_informasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `informasi` WRITE;
/*!40000 ALTER TABLE `informasi` DISABLE KEYS */;

INSERT INTO `informasi` (`id_informasi`, `deskripsi_info`, `id_akses`, `id_jenis_spm`)
VALUES
	(3,'Pemerintah Daerah kabupaten/kota sesuai dengan kewarganegaraan wajib menyampaikan laporan pelaksanaan pemenuhan SPM Pendidikan kepada menteri yang menyelenggarakan urusan pemerintahan dalam negeri melalui gubernur sebagai wakil dari pemerintahan pusat dengan tembusa kepada menteri',77,2),
	(5,'Pemerintah Daerah kabupaten/kota sesuai dengan kewarganegaraan wajib menyampaikan laporan pelaksanaan pemenuhan SPM Pendidikan kepada menteri yang menyelenggarakan urusan pemerintahan dalam negeri melalui gubernur sebagai wakil dari pemerintahan pusat dengan tembusan kepada menteri',77,2),
	(6,'Rehabilitasi sosial dasar penyandang disabilitas terlantar di dalam  panti sosial',88,7),
	(8,'Rehabilitasi sosial dasar anak terlantar di dalam panti sosial Sebagaimana dimaksud dalam pasal 7 huruf a sampai dengan huruf c dengan kriteria : a. tidak ada lagi perseorangan, keluarga, dan/atau masyarakat yang mengurus; b. rentan mengalami tindak kekerasan dari ligkungannya; dan/atau c. masih memiliki keluarg tetapi berpotensi mengalami tindak kekerasan, perlakuan salah, eksploitasi, dan penelantaran.',88,7),
	(9,'Rehabilitasi sosial dasar lanjut usia  terlantar di dalam panti sosial Sebagaimana dimaksud dalam pasal 7 huruf a sampai dengan huruf c dengan kriteria : a. tidak ada lagi perseorangan, keluarga, dan/atau masyarakat yang mengurus; b. rentan mengalami tindak kekerasan dari ligkungannya; dan/atau c. masih memiliki keluarg tetapi berpotensi mengalami tindak kekerasan, perlakuan salah, eksploitasi, dan penelantaran.',88,7),
	(10,'(1)Rehabilitasi sosial dasar tuna sosial khususnya gelandangan dan pengemis di dalam panti sosial (2)Rehabilitasi sosial dasar tuna sosial khususnya gelandangan dan pengemis sebagaimana dimaksud pada ayat (1) dilakukan terhadap kepala keluarga, istri/suami, dan anaknya',88,7),
	(11,'Perlindungan dan jaminan sosial pada saat dan setelah tanggap darurat bencana bagi korban bencana daerah provinsi sebagaimana bencana dengan kriteria: a. jumlah pengungsi/penyintas sebanyak 51 orang sampai dengan 100 orang; b. dampak bencana meliputi lebih dari1 (satu) daerah kabupaten/kota; dan/atau c. adanya surat penetapan bencana dari gubernur.',88,7),
	(12,'(1) Standar  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh penyandang disabilitas terlantar di dalam panti sosial merupakan kebutuhan dasar. (2) Kebutuhan dasar sebagaimana dimaksud pada ayat (1) terdiri atas : a. permakanan; b. sandang; c. asrama yang mudah diakses; d. alat bantu; e. perbekalan kesehatan; f.bimbingan fisik, mental priritual, dan sosial; g. bimbingan keterampilan hidu sehari-hari; h. pembuatan nomor induk kependudukan; i. akses ke layanan pendidikan dan kesehatan dasar; j.  pelayanan penelusuran keluarga; dan/atau k. pelayanan reunifikasi keluarga.  (3)  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh penyandang disabilitas terlantar sebagaimana dimaksud pada ayat (1) diberikan sesuai dengan kebutuhan penerima pelayanan dan ragam disabilitas berdasarkan hasil asesmen dari pekerja sosial profesional.',88,7),
	(13,'(1) Standar  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh anak  terlantar di dalam panti sosial merupakan kebutuhan dasar. (2) Kebutuhan dasar sebagaimana dimaksud pada ayat (1) terdiri atas : a.	pengasuhan b.	permakanan; c.	sandang; d.	asrama yang mudah diakses; e.	perbekalan kesehatan; f.	bimbingan fisik, mental priritual, dan sosial; g.	bimbingan keterampilan hidu sehari-hari; h.	pembuatan nomor induk kependudukan; i.	akses ke layanan pendidikan dan kesehatan dasar; j.	pelayanan penelusuran keluarga;  k.	pelayanan reunifikasi keluarga; dan/atau l.	Akses layanan pengasuhan kepada keluarga pengganti.  (3)  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh anak  terlantar sebagaimana dimaksud pada ayat (1) diberikan sesuai dengan kebutuhan penerima pelayanan ',88,7),
	(14,'(1) Standar  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh anak  terlantar di dalam panti sosial merupakan kebutuhan dasar. (2) Kebutuhan dasar sebagaimana dimaksud pada ayat (1) terdiri atas : a.	pengasuhan b.	permakanan; c.	sandang; d.	asrama yang mudah diakses; e.	perbekalan kesehatan; f.	bimbingan fisik, mental priritual, dan sosial; g.	bimbingan keterampilan hidu sehari-hari; h.	pembuatan nomor induk kependudukan; i.	akses ke layanan pendidikan dan kesehatan dasar; j.	pelayanan penelusuran keluarga;  k.	pelayanan reunifikasi keluarga; dan/atau l.	Akses layanan pengasuhan kepada keluarga pengganti.  (3)  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh anak  terlantar sebagaimana dimaksud pada ayat (1) diberikan sesuai dengan kebutuhan penerima pelayanan berdasarkan hasil asesmen dari pekerja sosial profesional.',88,7),
	(15,'(1) Standar  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh lanjut usia  terlantar di dalam panti sosial merupakan kebutuhan dasar. (2) Kebutuhan dasar sebagaimana dimaksud pada ayat (1) terdiri atas : a.	permakanan; b.	sandang; c.	asrama yang mudah diakses; d.	alat bantu; e.	perbekalan kesehatan; f.	bimbingan fisik, mental priritual, dan sosial; g.	bimbingan keterampilan hidup sehari-hari; h.	pembuatan nomor induk kependudukan; i.	akses ke layanan kesehatan dasar; j.	pelayanan penelusuran keluarga;  k.	pelayanan reunifikasi keluarga; dan/atau l.	pemulasaraan.   (3)  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh lanjut usia  terlantar sebagaimana dimaksud pada ayat (1) diberikan sesuai dengan kebutuhan penerima pelayanan berdasarkan hasil asesmen dari pekerja sosial profesional.',88,7),
	(16,'(1) Standar  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh gelandangan dan pengemis di dalam panti sosial merupakan kebutuhan dasar. (2) Kebutuhan dasar sebagaimana dimaksud pada ayat (1) terdiri atas : a.	permakanan; b.	sandang; c.	asrama yang mudah diakses; d.	perbekalan kesehatan; e.	bimbingan fisik, mental priritual, dan sosial; f.	bimbingan keterampilan dasar; g.	fasilitasi pembuatan nomor induk kependudukan, akta kelahiran, surat nikah, dan/atau kartu identitas anak; h.	akses ke layanan kesehatan dasar; dan/atau i.	pemulangan ke daerah asal.  (3)  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh gelandangan dan pengemis sebagaimana dimaksud pada ayat (1) diberikan sesuai dengan kebutuhan penerima pelayanan berdasarkan hasil asesmen dari pekerja sosial profesional.',88,7),
	(17,'(1) Standar  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh korban bencana daerah provinsi pada saat tanggap darurat bencana merupakan kebutuhan dasar. (2) Kebutuhan dasar sebagaimana dimaksud pada ayat (1) terdiri atas : a.	permakanan; b.	sandang; c.	tempat penampungan pengungsi; d.	penanganan khusus bagi kelompok rentan; dan e.	dukungan psikososial.  (3)  jumlah dan kualitas barang dan/atau jasa yang harus diterima oleh korban bencana sebagaimana dimaksud pada ayat (1) diberikan sesuai dengan kebutuhan penerima pelayanan berdasarkan hasil asesmen dari pekerja sosial profesional, tenaga kesejahteraan sosial dan/atau relawan sosial.',88,7),
	(18,'(1) Sumber daya manusia kesejahteraan sosial terdiri atas: a. tenaga kesejahteraan sosial; b. pekrjaan sosial profesional; c. penyuluh sosial; dan d. relawan sosial (2) ketentuan mengenai standart dan kualifikasi sumber daya manusia kesejahteraan sosial sebagaimana dimaksud pada ayat (1) dilaksanakan  sesuai dengan ketentuan peraturan perundang-undangan mengenai sumber daya manusia kesejahteraan sosial.',88,7),
	(19,'(1) Setiap panti sosial harus memiliki paling sedikit 1 (satu) orag pekerja sosial profesional. (2) Pemerintah daerah mendorong dan memfasilitasi sertifikasi pekerja sosial profesional. (3) sertifikasi sebagaimana dimaksud pada ayat (2) dilakukan oleh lembaga sertifikasi profesi pekerjaan sosial.',88,7),
	(20,'A.	Rehabilitasi sosial dasar penyandang disabilitas terlantar di luar panti sosial Sebagaimana dimaksud dalam pasal 26 huruf a sampai dengan huruf c dengan kriteria : a.	Tidak terpenuhi kebutuhan dasarnya, tidak terpelihara, tidak terawat, dan tidak terurus; dan b.	Masih ada perseorangan, keluarga, dan/atau masyarakat yang mengurus.',77,7),
	(21,'B.	Rehabilitasi sosial dasar anak terlantar di luar panti sosial Sebagaimana dimaksud dalam pasal 26 huruf a sampai dengan huruf c dengan kriteria : a.	Tidak terpenuhi kebutuhan dasarnya, tidak terpelihara, tidak terawat, dan tidak terurus; dan b.	Masih ada perseorangan, keluarga, dan/atau masyarakat yang mengurus.',77,7),
	(22,'C.	Rehabilitasi sosial dasar lanjut usia  terlantar di luar panti sosial Sebagaimana dimaksud dalam pasal 26 huruf a sampai dengan huruf c dengan kriteria : a.	Tidak terpenuhi kebutuhan dasarnya, tidak terpelihara, tidak terawat, dan tidak terurus; dan b.	Masih ada perseorangan, keluarga, dan/atau masyarakat yang mengurus.',77,7),
	(23,'D.	(1)Rehabilitasi sosial dasar tuna sosial khususnya gelandangan dan pengemis di luar panti sosial sebagaimana dimaksud dalam pasal 26 huruf d dengan kriteria : a.	Perseoangan atau Kepala keluarga berusia 19 (sembilan belas) tahun sampai dengan 60 (enak puluh ) tahun; b.	Tidak tepenuhinya kebutuhan dasarnya, tidak terpelihara, tidak terawat,dan tidak terurus; c.	Tidak memiiki tempat tinggal tetap; dan d.	Masih ada persesorangan, keluarga, dan/atau masyarakat yang peduli. (2) Rehabilitasi sosial dasar tuna sosial khususnya gelandangan dan pengemis sebagaimana dimaksud pada ayat (1) dilakukan terhadap kepala keluarga, istri/suami, dan anaknya',77,7),
	(24,'E.	Perlindungan dan jaminan sosial pada saat dan setelah tanggap darurat bencana bagi korban bencana daerah kabupaten/kota sebagaimana dimaksud dalam pasal 26 huruf e, persatu kali kejadian bencana dengan kriteria: a.	jumlah pengungsi/penyintas sebanyak 1 (satu)orang sampai dengan 50 (lima puluh) orang; b.	dampak bencana meliputi 1 (satu) daerah kabupaten/kota; dan/atau c.	adanya surat penetapan bencana dari bupati/wali kota.',77,7),
	(25,'(1) Sumber daya manusia kesejahteraan sosial terdiri atas: a.	Tenaga kesejahteraan sosial; b.	Pekerja sosialprofesional; c.	Penyuluh sosial;dan d.	Relawan sosial. (2) ketentuan mengenai standar dan kualifikasi sumber daya manusia kesejahteraan sosial sebagaimana dimaksud pada ayat (1) dilaksanakan sesuai dengan ketentuan peraturan perundang – undangan mengenai sumber daya manusia kesejahteraan sosial.',77,7),
	(26,'(1) Setiap pusat kesejahteraan sosial memiiki paling sedikit 1 (satu) orang relawan sosial. (2) Relawan sosial sebagaimana dimaksud pada ayat (1) terdiri atas: a.	Pekerja sosial masyarakat; b.	Karang taruna; c.	Tenaga pelopor perdamaian; d.	Taruna diaga bencana; e.	Tenaga kesejahteraan sosial kecamatan; f.	Wanita pemimpin kesejahteraan sosial; g.	Kader rehabilitasi berbasis masyarakat; h.	Kader rehabilitasi berbasis keluarga; dan/atau i.	Penyuluh sosial masyarakat. (3) relawan sosial sebagaimana dimaksud pada ayat (2) harus tersertifikasi. (4) sertifikasi sebagaimana dimaksud pada ayat (3) dilakukan oleh lembaga sertifikasi tenaga kesejahteraan sosial dan relawan sosial.',77,7),
	(27,'(1) standar minimum sarana dan prasarana pelayanan rehabilitasi sosial dasar di pusat kesejahteraan sosial dilaksanakan sesuai dengan ketentuan peraturan perundang-undangan. (2) standar minimum sarana dan prasarana pusat kesejahteraan sosial sebagaimana dimaksud pada ayar (1) meliputi : a.	Tempat yang dijadikan pusat kegiatan bersama; b.	Tenaga pelayanan yang terdiri dari tenaga pengelola dan pelaksana; dan c.	Peralatan yag terdiri dari peralatan penunjang perkantoran dan peralatan penunjang pelayanan teknis.',77,7),
	(28,'(3) laporan pelaksanaan pemenuhan SPM pendidikan termasuk dalam materi muatan laporan penyelenggaraan pemerintah daerah dan disampaikan sesuai dengan ketentuan peraturanperundang-undangan  yang mengatur mengenai laporan penyelenggaraan pemerintahan daerah.',77,2),
	(29,'(3) laporan pelaksanaan pemenuhan SPM pendidikan termasuk dalam materi muatan laporan penyelenggaraan pemerintah daerah dan disampaikan sesuai dengan ketentuan peraturanperundang-undangan  yang mengatur mengenai laporan penyelenggaraan pemerintahan daerah.',88,2),
	(30,'(4) Materi muatan laporan pelaksanaan pemenuhan SPM pendidikan sekurang-kurangnya terdiri atas : a.	Hasil penerapan SPM pendidikan b.	Kendala penerapan SPM pendidikan; dan c.	Ketersediaan anggaran dalam penerapan SPM pendidikan.',77,2),
	(31,'(4) Materi muatan laporan pelaksanaan pemenuhan SPM pendidikan sekurang-kurangnya terdiri atas : a.	Hasil penerapan SPM pendidikan b.	Kendala penerapan SPM pendidikan; dan c.	Ketersediaan anggaran dalam penerapan SPM pendidikan.',88,2),
	(32,'Selain materi muatan sebagaimana dimaksud pada ayat (4) , laporan pelaksanaan pemenuhan SPM pendidikan daerah provinsi juga harus mencantumkan rekapitulasi penerapan SPM pendidikan daerah kabupaten/kota.',88,2),
	(33,'Laporan pelaksanaan pemenuhan SPM pendidikan disusun sesuai dengan ketentuan peraturan perundang-undangan.',77,2),
	(34,'Laporan pelaksanaan pemenuhan SPM pendidikan disusun sesuai dengan ketentuan peraturan perundang-undangan.',88,2),
	(35,'A. Tahapan Penerapan SPM Bidang Kesehatan.  Beberapa langkah yang perlu dilakukan dalam pelaksanaan SPM bidang Kesehatan oleh Pemerintah Daerah adalah;  1.	Pengumpulan data, yang mencakup jumlah dan identitas lengkap warga Negara yang berhak memperoleh barang dan/ atau jasa kebutuhan dasar kesehatan secara minimal dan jumlah barang dan/atau jasa yang tersedia, termasuk jumlah sarana dan prasarana kesehatan yang tersedia;   2.	Penghitungan kebutuhan pemenuhan Pelayanan Dasar SPM Bidang Kesehatan, dilakukan dengan cara menghitung selisih antara jumlah barang dan /atau jasa yang dibutuhkan untuk pemenuhan pelayanan dasar kesehatan dengan jumlah barang dan/ atau jasa kesehatan yang tersedia, termasuk menghitung selisih antara sarana dan prasarana yang dibutuhkan untuk pemenuhan Pelayanan Dasar kesehatan dengan jumlah sarana dan prasaran kesehatan yang tersedia. Untuk penghitungan kebutuhan biaya pemenuhan Pelayanan Dasar kesehatan menggunakan standar biaya sesuai dengan ketentuan peraturan perundang-undangan yang berlaku; ',88,3),
	(36,'A. Tahapan Penerapan SPM Bidang Kesehatan.  3.	Penyusunan rencana pemenuhan Pelayanan Dasar SPM Bidang Kesehatan, dilakukan oleh pemerintah daerah agar Pelayanan Dasar Kesehatan tersedia secara cukup dan berkesinambungan yang ditetapkan dalam dokumen perencanaan dan penganggaran pembangunan Daerah sebagai prioritas belanja Daerah sesuai dengan ketentuan peraturan perundang-undangan; dan  4.	Pelaksanaan pemenuhan Pelayanan Dasar SPM Bidang Kesehatan, dilakukan sesuai dengan rencana pemenuhan Pelayanan Dasar. Pelaksanaan pemenuhan Pelayanan Dasar SPM Bidang Kesehatan dilakukan oleh Pemerintah Daerah berupa menyediakan barang dan/atau jasa yang dibutuhkan dan/atau melakukan kegusama Daerah sesuai dengan ketentuan peraturan perundang undangan. Dalam pelaksanaan pemenuhan Pelayanan Dasar SPM bidang kesehatan Pemerintah Daerah dapat membebaskan biaya untuk memenuhi kebutuhan dasar bagi Warga Negara yang berhak memperoleh Pelayanan Dasar secara minimal, dengan memprioritaskan bagi masyarakat miskin atau tidak mampu sesuai dengan ketentuan peraturan perundang-undangan, dan/atau memberikan bantuan pemenuhan barang dan/jasa kebutuhan dasar minimal dengan memprioritaskan bagi masyarakat miskin atau tidak mampu sesuai dengan ketentuan peraturan perundang-undangan. ',88,3),
	(37,'B. Monitoring dan Evaluasi  Monitoring dan evaluasi pelaksanaan pelayanan minimal bidang kesehatan dilakukan secara berkala setiap tiga bulan secara berjenjang menggunakan sistem pencatatan dan pelaporan yang berlaku pada setiap jenis layanan dasar. Bexjenjang dengan menggunakan tataran wilayah kerja sebagai berikut :   1.	Puskesmas bertanggung jawab terhadap pelaksanaan monitoring dan evaluasi pelayanan minimal bidang kesehatan dalam wilayah kerjanya dari seluruh fasilitas pelayanan kesehatan yang ada, baik milik pemerintah maupun milik swasta. Puskesmas melaporkan hasil monitoring dan evaluasi kepada Dinas Kesehatan Kabupaten / Kabupaten Kota.  2.	Dinas Kesehatan Kabupaten/ Kota bertanggung jawab terhadap pelaksanaan monitoring dan evaluasi pelayanan minimal bidang kesehatan dalam wilayah Kabupaten/Kota. Dinas Kesehatan Kabupaten/ Kota melaporkan hasil pelaksanaan monitoring dan evaluasi pelayanan minimal bidang kesehatan kepada Dinas Kesehatan Provinsi.  3.	Dinas Kesehatan Provinsi bertanggung jawab terhadap pelaksanaan Monitoring dan evaluasi pelaksanaan pelayanan minimal bidang kesehatan daerah Provinsi dan Kabupaten/Kota. Dinas Kesehatan Provinsi melaporkan hasil pelaksanaan monitoring dan evaluasi pelayanan ‘minimal bidang kesehatan. daerah Provinsi dan Kabupaten / Kota kepada Menteri Kesehatan.',88,3),
	(38,'C. Pelaporan Penerapan SPM Bidang Kesehatan  Pelaporan penerapan SPM termasuk dalam materi muatan laporan penyelenggaraan pemerintahan daerah dan disampaikan sesuai dengan  ketentuan peraturan pemndang-undangan yang mengatur mengenai laporan penyelenggaraan pemerintahan daerah.  Materi muatan laporan penerapan SPM Bidang Kesehatan memuat sebagai berikut :  1.	Hasil penerapan SPM; 2.	Kendala penerapan SPM; dan  3.	Ketersediaan anggaran dalam penerapan SPM.   Gubernur menyampaikan laporan SPM Bidang Kesehatan kepada Menteri Kesehatan atas laporan SPM Provinsi dan SPM Kabupaten/Kota. Bupati/Walikota menyampaikan laporan SPM Bidang Kesehatan kepada Menteri Kesehatan melalui Gubernur sebagai wakil pemerintah pusat.',88,3),
	(39,'Pembinaan dan pengawasan Salah satu tugas Pemerintah Pusat sebagaimana telah diamanahkan dalam Undang-undang Nomor 23 tahun 2014 adalah melakukan pembinaan dan pengawasan pada Pemerintah Daerah. Pembinaan dan pengawasan pelaksanaan SPM Bidang Kesehatan dilaksanakan secara berjenjang sebagai berikut:  a.	Menteri yang menyelenggarakan urusan pemerintahan dalam negeri melaksanakan pembinaan dan pengawasan penerapan SPM Daerah provinsi secara umum;  b.	Menteri kesehatan melaksanakan pembinaan dan pengawasan penerapan SPM bidang kesehatan Daerah Provinsi secara teknis;  c.	Gubernur melaksanakan pembinaan dan pengawasan terhadap penerapan SPM bidang kesehatan Provinsi oleh Organisasi Perangkat Daerah provinsi;  d.	Gubernur sebagai wakil Pemerintah Pusat melaksanakan pembinaan dan pengawasan pelaksanaan SPM Bidang Kesehatan Kabupaten/ Kota secara umum dan teknis;  e.	Bupati melaksanakan pembinaan dan pengawasan penerapan SPM Bidang Kesehatan Daerah Kabupaten oleh Organisasi Perangkat Daerah Kabupaten dan Walikota melaksanakan pembinaan dan pengawasan penerapan SPM Bidang Kesehatan Daerah Kota oleh Organisasi Perangkat Daerah Kota;  f.	Pembinaan dan pengawasan pelaksanaan SPM Bidang Kesehatan dilaksanakan sesuai dengan peraturan perundang-undangan yang mengatur mengenai pembinaan dan pengawasan penyelenggaraan Pemerintahan Daerah;  g.	Kepala Daerah dan/ atau Wakil Kepala Daerah yang tidak melaksanakan SPM dijatuhi sanksi administrative yang diatur dengan Peraturan Menteri yang menyelenggarakan urusan pemerintahan dalam negeri.',88,3),
	(41,'Pelaksanaan pembangunan baru, peningkatan dan peluasan SPAM lintas kabupaten/kota dilakukan oleh pelaksana penyelenggara BUMD, UPT/UPTD dan Badan Usaha untuk memenuhi kebutuhan sendiri',88,4),
	(42,'Pelaksanaan pembangunan baru, peningkatan dan peluasan SPAM bukan jaringan perpipaan skala individu dilakukan oleh perorangan dan skala komunal dilakukan oleh pelaksana penyelenggara BUMD, UPT/UPTD , Kelompok Masyarakat dan Badan Usaha untuk memenuhi kebutuhan sendiri',77,4),
	(44,'Pelaksanaan pembangunan baru, peningkatan dan peluasan SPAM jaringan perpipaan dilakukan oleh pelaksana penyelenggara BUMD, UPT/UPTD , Kelompok Masyarakat dan Badan Usaha untuk memenuhi kebutuhan sendiri',77,4),
	(45,'a.	Standar operasional prosedur Satpol PP; b.	Standar sarana praasarana Satpol PP; c.	Standar peningkatan kapasitas anggota Satpol PP dan anggota perlindungan masyarakat; dan d.	Standar pelayanan yang terkena dampak gangguan Trantibum akibat penegakan hukum terhadap pelanggaran Perda dan Perkada',88,6),
	(46,'a.	Standar operasional prosedur Satpol PP; b.	Standar sarana praasarana Satpol PP; c.	Standar peningkatan kapasitas anggota Satpol PP dan anggota perlindungan masyarakat; dan d.	Standar pelayanan yang terkena dampak gangguan Trantibum akibat penegakan hukum terhadap pelanggaran Perda dan Perkada',77,6),
	(47,'a.	Tingkat waktu tanggap (response time) 15 menit sejak diterimanya informasi/laporan sampai tiba di lokasi dan siap memberikan layanan penyelamatan dan evakuasi; b.	Prosedur operasional penanganan kebakaran, penyelamatan, dan evakuasi; c.	Sarana prasarana pemadam kebakaran, penyelamatan, dan evakuasi; d.	Kapasitas aparaur pemadaman kebakaran dan penyelamtan/ sumber daya manusia; e.	Pelayanan pemadaman, penyelamatan, dan evakuasi bagi warga negara yang menjadi korban kebakaran; dan f.	Pelayanan penyelamatan dan evakuasi bagi warga negara yang terdampak kebakaran.',77,6),
	(48,'a.	Standar jumlah  dan kualitas barang dan/atau, Personel/Sumber Daya Manusia dan petunjuk teknis atau tata cara pemenuhan standar untuk jenis pelayanan informasi rawan bencana; b.	Standar dan jumlah kualitas barang /atau, Personel/Sumber Daya Manusia Dan Petunjuk Teknis atau Tata cara pemenuhan standar  untuk jenis pelayanan pencegahan da kesiapsiagaan terhadap bencana; c.	Standar dan jumlah kualitas barang /atau, Personel/Sumber Daya Manusia Dan Petunjuk Teknis atau Tata cara pemenuhan standar  untuk jenis pelayanan penyelamatan dan evakuasi korban bencana.',77,6),
	(49,'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana kab/kota',77,5),
	(50,'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program pemerintah daerah daerah kabupaten / kota',77,5),
	(51,'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana provinsi',88,5),
	(52,'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program pemerintah daerah provinsi',88,5);

/*!40000 ALTER TABLE `informasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jenis_pelayanan_dasar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_pelayanan_dasar`;

CREATE TABLE `jenis_pelayanan_dasar` (
  `id_jenis_pelayanan_dasar` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_spm` int(11) NOT NULL,
  `nama_pelayanan` text,
  `penerima` text,
  `diampu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_pelayanan_dasar`),
  KEY `idjenisspm` (`id_jenis_spm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `jenis_pelayanan_dasar` WRITE;
/*!40000 ALTER TABLE `jenis_pelayanan_dasar` DISABLE KEYS */;

INSERT INTO `jenis_pelayanan_dasar` (`id_jenis_pelayanan_dasar`, `id_jenis_spm`, `nama_pelayanan`, `penerima`, `diampu`)
VALUES
	(1,2,'Pendidikan Menengah Atas','Usia 16 tahun sampai dengan 18 tahun',88),
	(2,2,'Pendidikan Khusus','Usia 4 tahun sampai dengan 18 tahun',88),
	(3,2,'Pendidikan Anak Usia Dini','Usia 5 tahun sampai dengan 6 tahun',77),
	(4,2,'Pendidikan Dasar','Usia 7 tahun sampai dengan 15 tahun',77),
	(5,2,'Pendidikan Kesetaraan','Usia 7 tahun sampai dengan 18 tahun',77),
	(6,3,'Kesehatan bagi penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi','Penduduk terdampak krisis kesehatan akibat bencana dan/atau berpotensi bencana provinsi',88),
	(7,3,'Kesehatan bagi penduduk pada kondisi kejadian luar biasa Provinsi','Penduduk pada kondisi kejadian luar biasa Provinsi',88),
	(8,3,'Kesehatan ibu hamil','Ibu hamil',77),
	(9,3,'Kesehatan ibu bersalin','Ibu bersalin',77),
	(10,3,'Kesehatan bayi baru lahir','Bayi baru lahir',77),
	(11,3,'Kesehatan balita','Balita',77),
	(12,3,'Kesehatan pada usia pendidikan dasar','Usia pendidikan dasar',77),
	(13,3,'Kesehatan usia produktif','Usia produktif',77),
	(14,3,'Kesehtan usia lanjut','Usia lanjut',77),
	(15,3,'Kesehatan penderita hipertensi','Penderita hipertensi',77),
	(16,3,'Kesehtan Penderita diabetes melitus','Penderita diabetes melitus',77),
	(17,3,'Kesehatan Orang dengan gangguan jiwa berat','Orang dengan gangguan jiwa berat',77),
	(18,3,'Kesehatan Orang terduga tuberkulosus','Orang terduga tuberkulosus',77),
	(19,3,'Kesehatan orang dengan resiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)','Orang dengan resiko terinfeksi virus yang melemahkan daya tahan tubuh manusia (Human Immunodeficiency Virus)',77),
	(32,7,'Rehabilitasi sosial dasar penyandang disabilitas terlantar di dalam  panti sosial','Penyandang disabilitas terlantar',88),
	(33,7,'Rehabilitasi sosial dasar anak terlantar di dalam panti sosial','Anak terlantar',88),
	(34,7,'Rahabilitasi sosial dasar lanjut usia terlantar di dalam panti sosial','Lanjut usia terlantar',88),
	(35,7,'Rehabilitasi sosial dasar tuna sosial khususnya gelandangan dan pengemis di dalam panti sosial','Gelandangan dan pengemis',88),
	(36,7,'Perlindungan dan jaminan sosial pada saat dan setelah tanggap darurat bencana bagi korban bencana daerah provinsi','Korban bencana provinsi',88),
	(37,7,'Perlindungan dan jaminan sosial pada saat dan setelah tanggap darurat bencana bagi korban bencana kabupaten/kota','Korban bencana kabupaten/kota',77),
	(38,2,'Pendidikan Menangah Pertama','Usia 7 tahun sampai dengan 15 tahun',77),
	(39,2,'Pendidikan Menengah Kejuruan','Usia 16 tahun sampai dengan 18 tahun',88),
	(40,7,'Rehabilitasi sosial dasar penyandang disabilitas terlantar di luar panti sosial','penyandang disabilitas terlantar',77),
	(41,7,'Rehabilitasi sosial dasar anak terlantar di luar panti sosial','anak terlantar',77),
	(42,7,'Rehabilitasi sosial dasar lanjut usia terlantar di luar panti sosial','lanjut usia terlantar',77),
	(43,7,'Rehabilitasi sosial dasar tuna sosial khususnya gelandangan dan pengemis di luar panti sosial','gelandangan dan pengemis',77),
	(44,4,'Pemenuhan kebutuhan air minum curah lintas kabupaten/kota','Penyelenggara sistem penyediaan air minum oleh badan usaha miik daerah dan unit pelaksana teknis daerah pada pemerintah daerah kabupaten/kota',88),
	(45,4,'Pemenuhan kebutuhan pokok air minum  sehari-hari','Jumlah Rumah tangga, terutama diprioritaskan pada masyarakat miskin atau tidak mampu dan berdomisili pada daerah rawan air dan akan dilayani melalui sistem penyediaan air minum',77),
	(48,5,'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana provinsi','Jumlah warga negara korban bencana yang memperoleh rumah layak huni',88),
	(49,5,'Penyediaan dan rehabilitasi rumah yang layak huni bagi korban bencana kabupaten/kota','Jumlah warga negara korban bencana yang memperoleh rumah layak huni',77),
	(50,5,'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program pemerintah daerah provinsi','Jumlah warga negara yang terkena relokasi akibat program pemerintah daerah provinsi yang memperoleh fasilitasi penyediaan rumah yang layak huni',88),
	(51,5,'Fasilitasi penyediaan rumah yang layak huni bagi masyarakat yang terkena relokasi program pemerintah daerah daerah kabupaten / kota','Jumlah warga negara yang terkena relokasi akibat program pemerintah daerah kabupaten / kota yang memperoleh fasilitasi penyediaan rumah yang layak huni',77),
	(52,6,'Pelayanan ketentraman dan ketertiban umum provinsi','Jumlah warga negara yang memeperoleh layanan akibat dari penegakan hukum perda dan perkada di provinsi',88),
	(53,6,'Pelayanan ketentraman dan ketertiban umum	','Jumlah warga negara yang memperoleh layanan akibat dari penegakan hukum perda dan perkada',77),
	(54,6,'Pelayanan informasi rawan bencana','Jumlah warga negara yang memperoleh layanan informasi  rawan bencana',77),
	(55,6,'Pelayanan pencegahan dan kesiapsiagaan terhadap bencana','Jumlah warga negara yang memperoleh layanan pencegahan dan kesiapsiagaan terhadap bencana',77),
	(56,6,'Pelayanan penyelamatan dan evakuasi korban bencana','Jumlah warga negara yang memperoleh layanan penyelamatan dan evakuasi korban bencana',77),
	(57,6,'Pelayanan penyelamatan dan evakuasi korban kebakaran','Jumlah warga negara yang memperoleh layanan penyelamatan dan evakuasi korban kebakaran',77),
	(58,4,'Penyediaan pelayanan pengolahan air limbah domestik','Jumlah rumah tangga seluruh Kabupaten/Kota',77),
	(59,4,'Penyediaan pelayanan pengolahan air limbah domestik regional lintas kabupaten/kota','Jumlah rumah tangga lintas Kabupaten/Kota',88),
	(60,0,'','',0),
	(61,0,'','',0),
	(62,0,'','',0),
	(63,0,'','',0),
	(64,0,'','',0);

/*!40000 ALTER TABLE `jenis_pelayanan_dasar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jenis_spm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_spm`;

CREATE TABLE `jenis_spm` (
  `id_jenis_spm` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_spm` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_spm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `jenis_spm` WRITE;
/*!40000 ALTER TABLE `jenis_spm` DISABLE KEYS */;

INSERT INTO `jenis_spm` (`id_jenis_spm`, `nama_jenis_spm`)
VALUES
	(1,'All'),
	(2,'Pendidikan'),
	(3,'Kesehatan'),
	(4,'Pekerjaan Umum'),
	(5,'Perumahan Rakyat'),
	(6,'Ketentraman, Ketertiban umum, dan Perlindungan masyarakat'),
	(7,'Sosial');

/*!40000 ALTER TABLE `jenis_spm` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kendala
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kendala`;

CREATE TABLE `kendala` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `kendala` WRITE;
/*!40000 ALTER TABLE `kendala` DISABLE KEYS */;

INSERT INTO `kendala` (`id`, `nama`)
VALUES
	(1,'Pemahaman Tim SPM Daerah Tentang SPM (Penentuan Sasaran, Perhitungan Pembiayaan, Menyusun Perencanaan)'),
	(2,'Komitmen Pimpinan Dalam Penerapan Dan Pencapaian Target SPM'),
	(3,'Belum Termuat Dalam Dokumen Perencanaan / Kurangnya Anggaran Untuk Indikator SPM'),
	(4,'Kurangnya Kuantitas dan Kualitas SDM Pengampu SPM'),
	(5,'Perubahan Struktur yang Dinamis (Promosi dan Mutasi)'),
	(6,'Terdapat Indikator yang Tidak Ada Datanya / Tidak Bisa Dikerjakan'),
	(99,'Lainnya');

/*!40000 ALTER TABLE `kendala` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kendala_
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kendala_`;

CREATE TABLE `kendala_` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table kota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kota`;

CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(50) DEFAULT NULL,
  `latlng` varchar(35) DEFAULT NULL,
  `id_provinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `kota` WRITE;
/*!40000 ALTER TABLE `kota` DISABLE KEYS */;

INSERT INTO `kota` (`id_kota`, `nama_kota`, `latlng`, `id_provinsi`)
VALUES
	(1,'Kabupaten Cilacap','-7.727096, 109.011726',33),
	(2,'Kabupaten Banyumas','-7.521535, 109.293572',33),
	(3,'Kabupaten Purbalingga','-7.389254, 109.363327',33),
	(4,'Kabupaten Banjarnegara','-7.450313, 109.531943',33),
	(5,'Kabupaten Kebumen','-7.667055, 109.651303',33),
	(6,'Kabupaten Purworejo','-7.710950, 110.011098',33),
	(7,'Kabupaten Wonosobo','-7.363335, 109.914064',33),
	(8,'Kabupaten Magelang','-7.477988, 110.216665',33),
	(9,'Kabupaten Boyolali','-7.515003, 110.628559',33),
	(10,'Kabupaten Klaten','-7.719142, 110.663713',33),
	(11,'Kabupaten Sukoharjo','-7.683031, 110.826059',33),
	(12,'Kabupaten Wonogiri','-7.801660, 110.940264',33),
	(13,'Kabupaten Karanganyar','-7.606645, 111.015271',33),
	(14,'Kabupaten Sragen','-7.426745, 111.020844',33),
	(15,'Kabupaten Grobogan','-7.025068, 110.919200',33),
	(16,'Kabupaten Blora','-6.968054, 111.420829',33),
	(17,'Kabupaten Rembang','-6.708301, 111.337239',33),
	(18,'Kabupaten Pati','-6.753485, 111.039908',33),
	(19,'Kabupaten Kudus','-6.808161, 110.841785',33),
	(20,'Kabupaten Jepara','-6.582311, 110.678105',33),
	(21,'Kabupaten Demak','-6.893513, 110.638800',33),
	(22,'Kabupaten Semarang','-7.257120, 110.456596',33),
	(23,'Kabupaten Temanggung','-7.315157, 110.177001',33),
	(24,'Kabupaten Kendal','-6.923303, 110.204875',33),
	(25,'Kabupaten Batang','-6.907616, 109.732297',33),
	(26,'Kabupaten Pekalongan','-6.890658, 109.676391',33),
	(27,'Kabupaten Pemalang','-6.883960, 109.370089',33),
	(28,'Kabupaten Tegal','-6.877046, 109.124277',33),
	(29,'Kabupaten Brebes','-6.870281, 109.039475',33),
	(71,'Kota Magelang','-7.484286, 110.219069',33),
	(72,'Kota Surakarta','-7.560229, 110.824133',33),
	(73,'Kota Salatiga','-7.332892, 110.503951',33),
	(74,'Kota Semarang','-7.000917, 110.435325',33),
	(75,'Kota Pekalongan','-6.896365, 109.679034',33),
	(76,'Kota Tegal','-6.874489, 109.123419',33),
	(77,'Provinsi Jawa Tengah','0',33);

/*!40000 ALTER TABLE `kota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table status_dokumen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status_dokumen`;

CREATE TABLE `status_dokumen` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `status_dokumen` WRITE;
/*!40000 ALTER TABLE `status_dokumen` DISABLE KEYS */;

INSERT INTO `status_dokumen` (`id`, `nama`)
VALUES
	(1,'-'),
	(2,'Save as Draft'),
	(3,'Cetak');

/*!40000 ALTER TABLE `status_dokumen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tablok08
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tablok08`;

CREATE TABLE `tablok08` (
  `kodkab` char(2) DEFAULT NULL COMMENT 'Kode Kab/Kota (00)',
  `kd` char(2) NOT NULL COMMENT 'Kode instansi',
  `nm` varchar(100) DEFAULT NULL COMMENT 'Nama instansi',
  `alamat` varchar(100) DEFAULT NULL COMMENT 'Alamat instansi induk',
  `kota` varchar(20) DEFAULT NULL COMMENT 'Kota instansi induk',
  `telp` varchar(50) DEFAULT NULL COMMENT 'Nomor telepon instansi induk',
  `fax` varchar(20) DEFAULT NULL COMMENT 'Nomor faximile instansi induk',
  `pergub` varchar(30) DEFAULT NULL,
  `aktif` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`kd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tablok08` WRITE;
/*!40000 ALTER TABLE `tablok08` DISABLE KEYS */;

INSERT INTO `tablok08` (`kodkab`, `kd`, `nm`, `alamat`, `kota`, `telp`, `fax`, `pergub`, `aktif`)
VALUES
	('00','01','GUBERNUR JAWA TENGAH','Jl. Pahlawan No. 9','Semarang','(024) 8311174 - 8311183','',NULL,0),
	('00','02','SEKRETARIAT DAERAH','Jl. Pahlawan No. 9','Semarang','(024) 8311174 - 8311183','',NULL,0),
	('00','03','SEKRETARIAT DPRD','Jl. Pahlawan No. 9','Semarang','(024) 8311174','',NULL,0),
	('00','10','BAKORWIL I','Jl. P. Sudirman No. 52 ','Pati','(0295) 381806, 381310','(024) 383156',NULL,0),
	('00','11','BAKORWIL II','Jl. P. Diponegoro No. 1 ','Magelang','(0293) 362220, 362000','(0293) 362308',NULL,0),
	('00','12','BAKORWIL III','Jl. Jend. Gatot Soebroto No. 67 ','Purwokerto','(0281) 363022 ','(0281) 635002',NULL,0),
	('00','13','SEKRETARIAT BPBD','Jl. Imam Bonjol I F ','Semarang','(024) 3519927, 3519932','',NULL,1),
	('00','14','SEKRETARIAT KPID','Jl. Trilombajuang No. 6','Semarang','(024) 8448608, 8448611','',NULL,0),
	('00','16','SEKRETARIAT BAKORLUH','Jl. Gatot Subroto No. 1 Komplek Tarubudaya','Ungaran','(024) 6924605','',NULL,0),
	('00','17','KANTOR PERWAKILAN','Jl. Darmawangsa VIII/26 Kebayoran Baru','Jakarta Selatan','(021) 7254212','',NULL,0),
	('00','21','BAPPEDA','Jl. Pemuda No. 127-133 ','Semarang','(024) 3515591, 3515592','(024) 3546802',NULL,0),
	('00','22','INSPEKTORAT','Jl. Pemuda No. 127-133','Semarang','(024) 3517283','',NULL,0),
	('00','23','BADAN KEPEGAWAIAN DAERAH','Jl. Stadion Selatan No. 1','Semarang','(024) 8415548, 8453676, 8318846','(024) 8318890',NULL,0),
	('00','24','BADAN LINGKUNGAN HIDUP','Jl. Setiabudi Komplek Diklat Propinsi Jawa Tengah','Semarang','(024) 7478813','(024) 8478813',NULL,0),
	('00','25','BADAN KESBANGPOL','Jl. Ahmad Yani No. 160','Semarang','(024) 8414205 ','(024) 8313122',NULL,1),
	('00','26','BADAN PENANAMAN MODAL DAERAH','Jl. Mgr. Sugiyopranoto No. 1','Semarang','(024) 3547081, 3547438, 3541487 ','(024) 3549560',NULL,0),
	('00','27','BADAN ARSIP & PERPUSTAKAAN','Jl. Setiabudi Srondol Komplek Diklat Propinsi Jateng','Semarang','(024) 7473746, 7474170, 7479466','(024) 7473800',NULL,0),
	('00','28','BADAN KETAHANAN PANGAN','Jl. Gatot Subroto Komplek Tarubudaya','Ungaran','(024) 6923412 ','(024) 6921997',NULL,0),
	('00','29','BADAN PENDIDIKAN & PELATIHAN','Jl. Setiabudi No. 201 A','Semarang','(024) 7472105, 7473066 Psw. 211-260','',NULL,0),
	('00','30','BADAN PEMBERDAYAAN MASY & DESA','Jl. Menteri Supeno No. 17 ','Semarang','(024) 8311621','(024) 8318492',NULL,0),
	('00','31','BADAN PENELITIAN & PENGEMBANGAN','Jl. Imam Bonjol No. 190 ','Semarang','(024) 3540025, 3548063 ','(024) 3560505',NULL,0),
	('00','32','BADAN PEMBERDAYAAN PEREMPUAN, PERLINDUNGAN ANAK DAN KELUARGA BERENCANA','Jl. Pamularsih 28','Semarang','(024) 7602952','(024) 7622536',NULL,0),
	('00','40','DINAS PENDIDIKAN','Jl. Pemuda 134/136','Semarang','(024) 3515301, 3546001','(024) 3520071',NULL,0),
	('00','41','DINAS KESEHATAN','Jl. Piere Tendean No. 24','Semarang','(024) 3511337, 3511349','(024) 3517463',NULL,0),
	('00','42','DINAS SOSIAL','Jl. Pahlawan No. 12','Semarang','(024) 8311729, 8311843','(024) 8450704',NULL,0),
	('00','43','DINAS NAKERTRANS & KEPENDUDUKAN','Jl. Pahlawan No. 16','Semarang','(024) 8311713','(024) 8311713',NULL,0),
	('00','44','DINAS KEBUDAYAAN & PARIWISATA','Jl. Pemuda No. 136','Semarang','(024) 3546001','(024) 3557119',NULL,0),
	('00','45','DINAS KOPERASI & UMKM','Jl. Sisingamangaraja No. 3A','Semarang','(024) 8310556, 8318773','(024) 8414165',NULL,0),
	('00','46','DINAS PEMUDA & OLAH RAGA','Jl. Ki Mangun Sarkoro No. 12','Semarang','(024) 8419956 - 8419958','(024) 8419959',NULL,0),
	('00','47','DINAS PERINDUSTRIAN & PERDAGANGAN','Jl. Pahlawan No. 4','Semarang','(024) 8419825','(024) 83111707',NULL,0),
	('00','48','DINAS PPAD','Jl. Pemuda No. 1','Semarang','(024) 3515514','',NULL,0),
	('00','49','DINAS PENGELOLAAN SUMBER DAYA AIR','Jl. Madukoro Blok AA-BB','Semarang','(024) 7608201, 7608342, 7608621','(024) 7612334',NULL,0),
	('00','50','DINAS BINA MARGA','Jl. Madukoro Blok AA-BB','Semarang','(024) 7611339, 7608368, 7611300 ','(024) 7608647',NULL,0),
	('00','51','DINAS CIPTA KARYA & TATA RUANG','Jl. Madukoro Blok. AA-BB','Semarang','(024) 7608202, 7600247','(024) 7608435, 76085',NULL,0),
	('00','52','DINAS ENERGI & SUMBER DAYA MINERAL','Jl. Madukoro Blok AA-BB No. 44','Semarang','(024) 7608203, 7610121','',NULL,0),
	('00','53','DINAS PERHUBUNGAN, KOMINFO','Jl. Siliwangi 355 - 357 Kotak Pos 1181','Semarang','(024) 7604640, 7605660, 7605700','(024) 7607697',NULL,0),
	('00','54','DINAS PERTANIAN TP & HORTIKULTURA','Jl. Jend. Gatot Soebroto (Tarubudaya)','Ungaran','(024) 6921010','(024) 6921060',NULL,0),
	('00','55','DINAS PETERNAKAN & KESWAN','Jl. Jend. Gatot Soebroto (Tarubudaya)','Ungaran','(024) 69121023','(024) 6921397',NULL,0),
	('00','56','DINAS KEHUTANAN','Jl. Menteri Supeno I/2','Semarang','(024) 8319140','(024) 8319328',NULL,0),
	('00','57','DINAS PERKEBUNAN','Jl. Jend. Gatot Subroto, Tarubudaya ','Ungaran','(024) 692121, 6921348, 6921430','',NULL,0),
	('00','58','DINAS KELAUTAN & PERIKANAN','Jl. Imam Bonjol No. 134','Semarang','(024) 3546469, 3546607','(024) 3551289',NULL,0),
	('00','60','SATUAN POLISI PAMONG PRAJA','Jl. Menteri Supeno No. 2','Semarang','(024) 8447331','',NULL,0),
	('00','80','RSUD DR. MOEWARDI','Jl. Kol. Sutarto 132, Jebres','Surakarta','(0271) 634634','',NULL,1),
	('00','81','RSUD PROF. DR. MARGONO SOEKARJO','Jl. DR Gumbreg 1','Purwokerto','(0281) 634537, 632708, 632550','',NULL,1),
	('00','82','RSUD TUGUREJO','Jl. Raya Tugurejo','Semarang','(024) 7605297, 7605378','',NULL,1),
	('00','83','RSUD KELET','Jl. Raya Kelet Jepara KM. 33 Keling','Jepara','(0291) 579002','(0291) 578161',NULL,1),
	('00','84','RSJD DR. AMINO GONDOHUTOMO','Jl. Brigjen Soediarto (Majapahit) No 347','Semarang','(024) 6722564','(024) 6722566',NULL,1),
	('00','85','RSJD SURAKARTA','Jl. Ki Hajar Dewantoro No 80 Jebres','Surakarta','(0271) 641442','',NULL,1),
	('00','86','RSJD DR. RM. SOEDJARWADI','Jl. Ki Pandanaran Km 2','Klaten','(0272) 321435','(0272) 321418',NULL,1),
	('00','99','LOKASI PENSIUN',NULL,NULL,NULL,NULL,NULL,1),
	('00','A1','GUBERNUR JAWA TENGAH','Jl. Pahlawan No. 9','Semarang',NULL,NULL,NULL,1),
	('00','A2','SEKRETARIAT DAERAH','Jl. Pahlawan No. 9','Semarang',NULL,NULL,'54',1),
	('00','A3','SEKRETARIAT DPRD','Jl. Pahlawan No. 9','Semarang',NULL,NULL,'55',1),
	('00','B0','INSPEKTORAT','Jl. Pemuda No. 127-133','Semarang',NULL,NULL,'56',1),
	('00','B1','BADAN PERENCANAAN PEMBANGUNAN, PENELITIAN DAN PENGEMBANGAN DAERAH','Jl. Pemuda No. 127-133 ','Semarang',NULL,NULL,'80',1),
	('00','B2','BADAN KEPEGAWAIAN DAERAH','Jl. Stadion Selatan No. 1','Semarang',NULL,NULL,'83',1),
	('00','B3','BADAN PENGEMBANGAN SUMBER DAYA MANUSIA DAERAH','Jl. Setiabudi No. 201 A','Semarang',NULL,NULL,'84',1),
	('00','B4','BADAN PENGELOLA PENDAPATAN DAERAH','Jl. Pemuda No. 1','Semarang',NULL,NULL,'81',1),
	('00','B5','BADAN PENGELOLA KEUANGAN DAN ASET DAERAH','Jl. Pahlawan No. 9 Gedung C','Semarang',NULL,NULL,'82',1),
	('00','C0','SATUAN POLISI PAMONG PRAJA','Jl. Menteri Supeno No. 2','Semarang',NULL,NULL,'62',1),
	('00','C1','BADAN PENGHUBUNG','Jl. Darmawangsa VIII/26 Kebayoran Baru','Jakarta',NULL,NULL,'93',1),
	('00','D0','DINAS PENDIDIKAN DAN KEBUDAYAAN','Jl. Pemuda 134/136','Semarang',NULL,NULL,'57',1),
	('00','D1','DINAS KESEHATAN','Jl. Piere Tendean No. 24','Semarang',NULL,NULL,'58',1),
	('00','D2','DINAS PEKERJAAN UMUM BINA MARGA DAN CIPTA KARYA','Jl. Madukoro Blok AA-BB','Semarang',NULL,NULL,'59',1),
	('00','D3','DINAS PEKERJAAN UMUM SUMBER DAYA AIR DAN PENATAAN RUANG','Jl. Madukoro Blok AA-BB','Semarang',NULL,NULL,'60',1),
	('00','D4','DINAS PERUMAHAN RAKYAT DAN KAWASAN PERMUKIMAN','Jl. Madukoro Blok AA-BB','Semarang',NULL,NULL,'61',1),
	('00','D5','DINAS SOSIAL','Jl. Pahlawan No. 12','Semarang',NULL,NULL,'63',1),
	('00','D6','DINAS TENAGA KERJA DAN TRANSMIGRASI','Jl. Pahlawan No. 16','Semarang',NULL,NULL,'64',1),
	('00','D7','DINAS ENERGI DAN SUMBER DAYA MINERAL','Jl. Madukoro Blok AA-BB No. 44','Semarang',NULL,NULL,'78',1),
	('00','D8','DINAS KEPEMUDAAN, OLAHRAGA DAN PARIWISATA','Jl. Ki Mangun Sarkoro No. 12','Semarang',NULL,NULL,'73',1),
	('00','D9','DINAS PERHUBUNGAN','Jl. Siliwangi 355 - 357 Kotak Pos 1181','Semarang',NULL,NULL,'69',1),
	('00','E0','DINAS KOMUNIKASI DAN INFORMATIKA','Jl. Menteri Supeno I/2','Semarang',NULL,NULL,'70',1),
	('00','E1','DINAS PERINDUSTRIAN DAN PERDAGANGAN','Jl. Pahlawan No. 4','Semarang',NULL,NULL,'79',1),
	('00','E2','DINAS KOPERASI, USAHA KECIL, DAN MENENGAH','Jl. Sisingamangaraja No. 3A','Semarang',NULL,NULL,'71',1),
	('00','E3','DINAS PERTANIAN DAN PERKEBUNAN','Jl. Jend. Gatot Soebroto (Tarubudaya)','Ungaran',NULL,NULL,'76',1),
	('00','E4','DINAS KETAHANAN PANGAN','Jl. Jend. Gatot Soebroto (Tarubudaya)','Ungaran',NULL,NULL,'66',1),
	('00','E5','DINAS PETERNAKAN DAN KESWAN','Jl. Jend. Gatot Soebroto (Tarubudaya)','Ungaran',NULL,NULL,'77',1),
	('00','E6','DINAS KELAUTAN DAN PERIKANAN','Jl. Imam Bonjol No. 134','Semarang',NULL,NULL,'75',1),
	('00','E7','DINAS LINGKUNGAN HIDUP DAN KEHUTANAN','Jl. Setiabudi Komplek Diklat Propinsi Jawa Tengah','Semarang',NULL,NULL,'67',1),
	('00','E8','DINAS PEMBERDAYAAN MASYARAKAT, DESA, KEPENDUDUKAN DAN PENCATATAN SIPIL','Jl. Menteri Supeno No. 17 ','Semarang',NULL,NULL,'68',1),
	('00','E9','DINAS PEMBERDAYAAN PEREMPUAN, PERLINDUNGAN ANAK, PENGENDALIAN PENDUDUK DAN KELUARGA BERENCANA','Jl. Pamularsih 28','Semarang',NULL,NULL,'65',1),
	('00','F0','DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU','Jl. Mgr. Sugiyopranoto No. 1','Semarang',NULL,NULL,'72',1),
	('00','F1','DINAS KEARSIPAN DAN PERPUSTAKAAN','Jl. Setiabudi Srondol Komplek Diklat Propinsi Jateng','Semarang',NULL,NULL,'74',1);

/*!40000 ALTER TABLE `tablok08` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table targetcapaian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `targetcapaian`;

CREATE TABLE `targetcapaian` (
  `id_targetcapaian` int(11) NOT NULL AUTO_INCREMENT,
  `id_indikator` int(11) NOT NULL,
  `tahun` char(4) DEFAULT '2019',
  `target` int(11) DEFAULT '0' COMMENT 'dalam %',
  `anggaran_t` varchar(255) NOT NULL DEFAULT '0',
  `realisasi` varchar(255) NOT NULL DEFAULT '0',
  `kendala` int(255) NOT NULL,
  `solusi` varchar(255) DEFAULT NULL,
  `jumlah_pembilang` int(11) DEFAULT '0',
  `jumlah_penyebut` int(11) DEFAULT '0',
  `id_user` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_targetcapaian`),
  KEY `idindikator` (`id_indikator`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `targetcapaian` WRITE;
/*!40000 ALTER TABLE `targetcapaian` DISABLE KEYS */;

INSERT INTO `targetcapaian` (`id_targetcapaian`, `id_indikator`, `tahun`, `target`, `anggaran_t`, `realisasi`, `kendala`, `solusi`, `jumlah_pembilang`, `jumlah_penyebut`, `id_user`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(48,55,'2020',100,'3','3',1,NULL,3,3,22,'2021-01-14 17:59:51','2021-02-05 02:08:58',NULL),
	(49,56,'2020',100,'3','34',0,NULL,3,3,22,'2021-01-14 17:59:51','2021-01-14 18:01:54',NULL),
	(50,58,'2020',100,'3','3',0,NULL,3,3,22,'2021-01-14 17:59:51',NULL,NULL),
	(51,57,'2020',100,'3','3',0,NULL,3,3,22,'2021-01-14 17:59:51',NULL,NULL),
	(52,107,'2020',100,'1','1',3,NULL,1,1,104,'2021-01-14 18:08:15','2021-02-05 02:09:01',NULL),
	(53,108,'2020',100,'1','1',0,NULL,1,1,104,'2021-01-14 18:08:15',NULL,NULL),
	(54,61,'2020',100,'25500000','24690000',0,NULL,29408,30684,58,'2021-01-18 08:37:51',NULL,NULL),
	(55,62,'2020',100,'5593998000','2540950654',0,NULL,28471,28488,58,'2021-01-18 08:37:51',NULL,NULL),
	(56,63,'2020',100,'8000000','8000000',0,NULL,28303,28303,58,'2021-01-18 08:37:51',NULL,NULL),
	(57,64,'2020',100,'350000000','302169030',0,NULL,136024,136024,58,'2021-01-18 08:37:51',NULL,NULL),
	(58,65,'2020',100,'0','0',0,NULL,56018,56322,58,'2021-01-18 08:37:51',NULL,NULL),
	(59,66,'2020',100,'12816600','12816600',0,NULL,349587,1348904,58,'2021-01-18 08:37:51',NULL,NULL),
	(60,67,'2020',100,'0','0',0,NULL,158093,251443,58,'2021-01-18 08:37:51',NULL,NULL),
	(61,68,'2020',100,'9335000','9335000',0,NULL,107058,161830,58,'2021-01-18 08:37:51',NULL,NULL),
	(62,72,'2020',100,'0','0',0,NULL,23617,33984,58,'2021-01-18 08:37:51',NULL,NULL),
	(63,69,'2020',100,'0','0',0,NULL,2190,1988,58,'2021-01-18 08:37:51','2021-01-21 10:39:22',NULL),
	(64,70,'2020',100,'156486961','142935975',0,NULL,6377,23540,58,'2021-01-18 08:37:51',NULL,NULL),
	(65,71,'2020',100,'61999840','61999840',0,NULL,51092,56541,58,'2021-01-18 08:37:51',NULL,NULL),
	(66,55,'2020',100,'20033510000','20024978600',0,NULL,27731,28193,34,'2021-01-19 10:47:19','2021-01-29 16:12:20',NULL),
	(67,56,'2020',100,'85532751141','80893221809',0,NULL,73838,73838,34,'2021-01-19 10:47:19','2021-01-29 16:09:19',NULL),
	(68,58,'2020',100,'7703908000','7687957000',0,NULL,823,831,34,'2021-01-19 10:47:19','2021-01-29 16:15:39',NULL),
	(69,57,'2020',100,'51599738397','51033837897',0,NULL,30413,30414,34,'2021-01-19 10:47:19','2021-01-29 16:20:55',NULL),
	(70,61,'2020',100,'344560000','336545000',0,NULL,12698,12698,70,'2021-01-19 13:43:08','2021-01-20 10:27:50',NULL),
	(71,62,'2020',100,'2259396000','2237531787',0,NULL,12146,12146,70,'2021-01-19 13:43:08','2021-01-20 10:32:24',NULL),
	(72,63,'2020',100,'344560000','336545000',0,NULL,11701,11701,70,'2021-01-19 13:43:08','2021-01-20 11:01:41',NULL),
	(73,64,'2020',100,'344560000','336545000',0,NULL,44036,44036,70,'2021-01-19 13:43:08','2021-01-20 11:03:24',NULL),
	(74,65,'2020',100,'344560000','336545000',0,NULL,20925,21475,70,'2021-01-19 13:43:08','2021-01-21 10:15:16',NULL),
	(75,66,'2020',100,'114975000','111485000',0,NULL,311328,358717,70,'2021-01-19 13:43:08','2021-01-20 11:07:44',NULL),
	(76,67,'2020',100,'344545000','336545000',0,NULL,86612,86612,70,'2021-01-19 13:43:08','2021-01-21 10:10:57',NULL),
	(77,68,'2020',100,'167400000','150200000',0,NULL,101784,101784,70,'2021-01-19 13:43:08','2021-01-21 10:06:58',NULL),
	(78,72,'2020',100,'167400000','150200000',0,NULL,10790,10790,70,'2021-01-19 13:43:08','2021-01-21 10:04:38',NULL),
	(79,69,'2020',100,'200000000','185705000',0,NULL,1192,1192,70,'2021-01-19 13:43:08','2021-01-21 10:02:20',NULL),
	(80,70,'2020',100,'271325250','244709247',0,NULL,2211,2211,70,'2021-01-19 13:43:08','2021-01-21 10:00:46',NULL),
	(81,71,'2020',100,'282174750','256659747',0,NULL,11438,11438,70,'2021-01-19 13:43:08','2021-01-21 09:46:38',NULL),
	(82,83,'2020',100,'144080000','140135000',0,NULL,144,144,162,'2021-01-19 14:37:39','2021-01-28 09:37:54',NULL),
	(83,79,'2020',100,'85250000','65397250',0,NULL,15,15,162,'2021-01-19 14:37:39','2021-01-28 09:38:25',NULL),
	(84,80,'2020',100,'6300000','6300000',0,NULL,5,5,162,'2021-01-19 14:37:39','2021-01-28 09:38:52',NULL),
	(85,81,'2020',100,'56450000','39900000',0,NULL,50,50,162,'2021-01-19 14:37:39','2021-01-28 09:39:17',NULL),
	(86,82,'2020',100,'20920000','16720000',0,NULL,8,8,162,'2021-01-19 14:37:39','2021-01-28 09:39:40',NULL),
	(87,121,'2020',100,'14975633000','0',0,NULL,263958,292420,144,'2021-01-19 14:51:16','2021-01-20 11:25:03',NULL),
	(88,122,'2020',100,'6886911000','6542565450',0,NULL,205046,216761,144,'2021-01-19 14:51:16','2021-01-27 10:18:56',NULL),
	(89,107,'2020',100,'0','0',0,NULL,0,0,126,'2021-01-19 14:57:16',NULL,NULL),
	(90,108,'2020',100,'0','0',0,NULL,211554,216751,126,'2021-01-19 14:57:16','2021-01-20 11:48:28',NULL),
	(91,121,'2020',87,'5343228212','5367130000',99,NULL,212379,233210,255,'2021-01-20 12:34:53','2021-01-20 12:39:51',NULL),
	(92,122,'2020',81,'1000000000','1000000000',3,NULL,159826,233210,255,'2021-01-20 12:34:53','2021-01-20 12:40:11',NULL),
	(93,107,'2020',100,'0','0',3,NULL,0,0,205,'2021-01-20 12:43:38',NULL,NULL),
	(94,108,'2020',100,'1365000000','1295000000',3,NULL,37,181,205,'2021-01-20 12:43:38',NULL,NULL),
	(95,92,'2020',100,'9835551000','9748955150',0,NULL,0,0,182,'2021-01-21 12:46:38','2021-01-25 14:06:22',NULL),
	(96,119,'2020',100,'95000000','94552000',99,NULL,504978,504978,182,'2021-01-21 12:46:38','2021-02-01 08:03:40',NULL),
	(97,100,'2020',100,'225000000','224020000',0,NULL,504978,504978,182,'2021-01-21 12:46:38','2021-02-01 08:04:44',NULL),
	(98,104,'2020',100,'1852505000','1710196740',0,NULL,362,362,182,'2021-01-21 12:46:38','2021-02-01 08:05:36',NULL),
	(99,120,'2020',100,'440000000','439248100',0,NULL,84,84,182,'2021-01-21 12:46:38','2021-02-01 08:48:21',NULL),
	(100,79,'2020',100,'66243120','65267420',0,NULL,581,5685,228,'2021-01-22 12:21:56',NULL,NULL),
	(101,80,'2020',100,'0','0',0,NULL,69,609,228,'2021-01-22 12:21:56',NULL,NULL),
	(102,81,'2020',100,'29319500','27635650',0,NULL,11,20561,228,'2021-01-22 12:21:56',NULL,NULL),
	(103,82,'2020',100,'217884400','213167533',0,NULL,18,76,228,'2021-01-22 12:21:56',NULL,NULL),
	(104,83,'2020',100,'80702000','80702000',0,NULL,58,58,228,'2021-01-22 12:21:56',NULL,NULL),
	(105,55,'2020',100,'1074773700','715111565',0,NULL,24443,38642,29,'2021-01-25 08:45:36',NULL,NULL),
	(106,56,'2020',100,'103306463888','102366983946',0,NULL,118839,122449,29,'2021-01-25 08:45:36',NULL,NULL),
	(107,57,'2020',100,'46969111208','46535856436',0,NULL,47676,57972,29,'2021-01-25 08:45:36',NULL,NULL),
	(108,58,'2020',100,'1537301648','1587301648',0,NULL,1410,229922,29,'2021-01-25 08:45:36',NULL,NULL),
	(109,79,'2020',100,'60845000','60845000',0,NULL,476,476,163,'2021-01-26 14:55:48',NULL,NULL),
	(110,80,'2020',100,'23783000','23783000',0,NULL,52,52,163,'2021-01-26 14:55:48',NULL,NULL),
	(111,81,'2020',100,'17050000','17049997',0,NULL,282,282,163,'2021-01-26 14:55:48',NULL,NULL),
	(112,82,'2020',100,'507545000','500117499',0,NULL,362,362,163,'2021-01-26 14:55:48',NULL,NULL),
	(113,83,'2020',100,'3345000060','31775000',0,NULL,36,36,163,'2021-01-26 14:55:48',NULL,NULL),
	(114,79,'2020',100,'37560000','25127696',0,NULL,18,20,157,'2021-01-27 08:45:58',NULL,NULL),
	(115,80,'2020',100,'25170000','18845772',0,NULL,11,15,157,'2021-01-27 08:45:58',NULL,NULL),
	(116,81,'2020',100,'8390000','6281924',0,NULL,3,5,157,'2021-01-27 08:45:58',NULL,NULL),
	(117,82,'2020',100,'16780000','12563848',0,NULL,9,10,157,'2021-01-27 08:45:58',NULL,NULL),
	(118,83,'2020',100,'310220000','309586362',0,NULL,80,80,157,'2021-01-27 08:45:58',NULL,NULL),
	(119,92,'2020',100,'110040000','93522000',99,NULL,0,0,107,'2021-01-27 11:24:43',NULL,NULL),
	(120,100,'2020',100,'0','0',0,NULL,0,0,107,'2021-01-27 11:24:43',NULL,NULL),
	(121,104,'2020',100,'0','0',0,NULL,0,0,107,'2021-01-27 11:24:43',NULL,NULL),
	(122,119,'2020',100,'0','0',0,NULL,0,0,107,'2021-01-27 11:24:43',NULL,NULL),
	(123,120,'2020',100,'717244502','712754835',99,NULL,53,63,107,'2021-01-27 11:24:43','2021-01-27 13:54:00',NULL),
	(124,91,'2020',100,'25000000','0',0,NULL,0,0,97,'2021-01-28 12:59:14',NULL,NULL),
	(125,61,'2020',100,'194140000','150678600',99,NULL,1499,1560,87,'2021-01-28 12:58:08',NULL,NULL),
	(126,62,'2020',100,'165636000','112894200',0,NULL,1440,1440,87,'2021-01-28 12:58:08',NULL,NULL),
	(127,63,'2020',100,'165636000','122894200',99,NULL,1422,1441,87,'2021-01-28 12:58:08',NULL,NULL),
	(128,64,'2020',100,'17556000','15393400',99,NULL,3639,5092,87,'2021-01-28 12:58:08',NULL,NULL),
	(129,65,'2020',100,'0','0',99,NULL,17114,17454,87,'2021-01-28 12:58:08',NULL,NULL),
	(130,66,'2020',100,'13841000','13591000',0,NULL,82894,82894,87,'2021-01-28 12:58:08',NULL,NULL),
	(131,67,'2020',100,'53564000','50150805',99,NULL,13243,18409,87,'2021-01-28 12:58:08',NULL,NULL),
	(132,68,'2020',100,'19246000','17545000',0,NULL,8089,8089,87,'2021-01-28 12:58:08',NULL,NULL),
	(133,69,'2020',100,'26500000','25720000',0,NULL,494,493,87,'2021-01-28 12:58:08',NULL,NULL),
	(134,70,'2020',100,'19955000','18166000',0,NULL,469,469,87,'2021-01-28 12:58:08',NULL,NULL),
	(135,71,'2020',100,'225102000','176131000',0,NULL,3890,3890,87,'2021-01-28 12:58:08',NULL,NULL),
	(136,72,'2020',100,'19246000','17545000',0,NULL,2488,2488,87,'2021-01-28 12:58:08',NULL,NULL),
	(137,59,'2020',100,'6867295','5439072113',1,NULL,111541,111541,57,'2021-01-29 09:15:33',NULL,NULL),
	(138,60,'2020',100,'0','0',1,NULL,152961,152961,57,'2021-01-29 09:15:33',NULL,NULL),
	(139,92,'2020',100,'0','0',0,NULL,0,0,169,'2021-01-29 18:57:12',NULL,NULL),
	(140,100,'2020',100,'1030230295','972804545',0,NULL,0,1295019,169,'2021-01-29 18:57:12',NULL,NULL),
	(141,104,'2020',100,'1539625761','1431392765',0,NULL,527,1295017,169,'2021-01-29 18:57:12',NULL,NULL),
	(142,119,'2020',100,'309654000','29842606',0,NULL,822310,1295017,169,'2021-01-29 18:57:12',NULL,NULL),
	(143,120,'2020',100,'0','0',0,NULL,91,91,169,'2021-01-29 18:57:12','2021-01-30 13:43:55',NULL),
	(144,79,'2020',100,'77066200','63969300',99,NULL,90,356,166,'2021-01-30 11:18:32','2021-01-30 11:26:58',NULL),
	(145,80,'2020',100,'3925250','3925250',0,NULL,30,30,166,'2021-01-30 11:18:32',NULL,NULL),
	(146,81,'2020',100,'77066200','63969300',0,NULL,1451,1451,166,'2021-01-30 11:18:32',NULL,NULL),
	(147,82,'2020',100,'20669150','20669150',0,NULL,200,200,166,'2021-01-30 11:18:32',NULL,NULL),
	(148,83,'2020',100,'18816450','10205950',0,NULL,117,120,166,'2021-01-30 11:18:32',NULL,NULL),
	(149,73,'2020',100,'17405317000','100',0,NULL,3767,11301,94,'2021-02-01 08:34:19',NULL,NULL),
	(150,75,'2020',100,'14392955000','100',0,NULL,2840,8520,94,'2021-02-01 08:34:19',NULL,NULL),
	(151,76,'2020',100,'14182341000','100',0,NULL,1795,5385,94,'2021-02-01 08:34:19',NULL,NULL),
	(152,77,'2020',100,'5920550000','100',0,NULL,650,1950,94,'2021-02-01 08:34:19',NULL,NULL),
	(153,78,'2020',100,'1375617000','100',0,NULL,100,100,94,'2021-02-01 08:34:19',NULL,NULL),
	(154,107,'2020',100,'0','0',0,NULL,0,0,245,'2021-02-01 13:46:46',NULL,NULL),
	(155,108,'2020',100,'0','0',0,NULL,0,0,245,'2021-02-01 13:46:46',NULL,NULL);

/*!40000 ALTER TABLE `targetcapaian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_akses` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `id_skpd` varchar(10) NOT NULL COMMENT 'kd (tablok08)',
  `id_pengampu_spm` int(11) NOT NULL,
  `nip` varchar(225) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `aktif` int(11) DEFAULT '0',
  `hapus` int(11) DEFAULT '0',
  PRIMARY KEY (`id_user`),
  KEY `idakses` (`id_akses`),
  KEY `idkota` (`id_kota`),
  KEY `kdskpd` (`id_skpd`),
  KEY `idspmpengampu` (`id_pengampu_spm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id_user`, `id_akses`, `id_kota`, `id_skpd`, `id_pengampu_spm`, `nip`, `nama`, `password`, `aktif`, `hapus`)
VALUES
	(5,99,0,'A2',1,'14045','Programmer','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(12,99,77,'A2',1,'97531','BIRO OTDA','2ce962d00d36199ff38483681618b085',0,0),
	(21,88,77,'D0',2,'PendidikanJateng','Pendidikan Provinsi Jawa Tengah','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(22,77,1,'D0',2,'PendidikanCilacap','Pendidikan Kabupaten Cilacap','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(23,77,2,'D0',2,'PendidikanBanyumas','Pendidikan Kabupaten Banyumas','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(24,77,3,'D0',2,'PendidikanPurbalingga','Pendidikan Kabupaten Purbalingga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(25,77,4,'D0',2,'PendidikanBanjarnegara','Pendidikan Kabupaten Banjarnegara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(26,77,5,'D0',2,'PendidikanKebumen','Pendidikan Kabupaten Kebumen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(27,77,6,'D0',2,'PendidikanPurworejo','Pendidikan Kabupaten Purworejo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(28,77,7,'D0',2,'PendidikanWonosobo','Pendidikan Kabupaten Wonosobo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(29,77,8,'D0',2,'PendidikanMagelang','Pendidikan Kabupaten Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(30,77,9,'D0',2,'PendidikanBoyolali','Pendidikan Kabupaten Boyolali','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(31,77,10,'D0',2,'PendidikanKlaten','Pendidikan Kabupaten Klaten','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(32,77,11,'D0',2,'PendidikanSukoharjo','Pendidikan Kabupaten Sukoharjo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(33,77,12,'D0',2,'PendidikanWonogiri','Pendidikan Kabupaten Wonogiri','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(34,77,13,'D0',2,'PendidikanKaranganyar','Pendidikan Kabupaten Karanganyar','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(35,77,14,'D0',2,'PendidikanSragen','Pendidikan Kabupaten Sragen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(36,77,15,'D0',2,'PendidikanGrobogan','Pendidikan Kabupaten Grobogan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(37,77,16,'D0',2,'PendidikanBlora','Pendidikan Kabupaten Blora','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(38,77,17,'D0',2,'PendidikanRembang','Pendidikan Kabupaten Rembang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(39,77,18,'D0',2,'PendidikanPati','Pendidikan Kabupaten Pati','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(40,77,19,'D0',2,'PendidikanKudus','Pendidikan Kabupaten Kudus','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(41,77,20,'D0',2,'PendidikanJepara','Pendidikan Kabupaten Jepara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(42,77,21,'D0',2,'PendidikanDemak','Pendidikan Kabupaten Demak','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(43,77,22,'D0',2,'PendidikanSemarang','Pendidikan Kabupaten Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(44,77,23,'D0',2,'PendidikanTemanggung','Pendidikan Kabupaten Temanggung','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(45,77,24,'D0',2,'PendidikanKendal','Pendidikan Kabupaten Kendal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(46,77,25,'D0',2,'PendidikanBatang','Pendidikan Kabupaten Batang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(47,77,26,'D0',2,'PendidikanPekalongan','Pendidikan Kabupaten Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(48,77,27,'D0',2,'PendidikanPemalang','Pendidikan Kabupaten Pemalang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(49,77,28,'D0',2,'PendidikanTegal','Pendidikan Kabupaten Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(50,77,29,'D0',2,'PendidikanBrebes','Pendidikan Kabupaten Brebes','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(51,77,71,'D0',2,'PendidikanKotaMagelang','Pendidikan Kota Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(52,77,72,'D0',2,'PendidikanKotaSurakarta','Pendidikan Kota Surakarta','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(53,77,73,'D0',2,'PendidikanKotaSalatiga','Pendidikan Kota Salatiga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(54,77,74,'D0',2,'PendidikanKotaSemarang','Pendidikan Kota Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(55,77,75,'D0',2,'PendidikanKotaPekalongan','Pendidikan Kota Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(56,77,76,'D0',2,'PendidikanKotaTegal','Pendidikan Kota Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(57,88,77,'D1',3,'KesehatanJateng','Kesehatan Provinsi Jawa Tengah','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(58,77,1,'D1',3,'KesehatanCilacap','Kesehatan Kabupaten Cilacap','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(59,77,2,'D1',3,'KesehatanBanyumas','Kesehatan Kabupaten Banyumas','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(60,77,3,'D1',3,'KesehatanPurbalingga','Kesehatan Kabupaten Purbalingga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(61,77,4,'D1',3,'KesehatanBanjarnegara','Kesehatan Kabupaten Banjarnegara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(62,77,5,'D1',3,'KesehatanKebumen','Kesehatan Kabupaten Kebumen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(63,77,6,'D1',3,'KesehatanPurworejo','Kesehatan Kabupaten Purworejo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(64,77,7,'D1',3,'KesehatanWonosobo','Kesehatan Kabupaten Wonosobo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(65,77,8,'D1',3,'KesehatanMagelang','Kesehatan Kabupaten Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(66,77,9,'D1',3,'KesehatanBoyolali','Kesehatan Kabupaten Boyolali','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(67,77,10,'D1',3,'KesehatanKlaten','Kesehatan Kabupaten Klaten','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(68,77,11,'D1',3,'KesehatanSukoharjo','Kesehatan Kabupaten Sukoharjo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(69,77,12,'D1',3,'KesehatanWonogiri','Kesehatan Kabupaten Wonogiri','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(70,77,13,'D1',3,'KesehatanKaranganyar','Kesehatan Kabupaten Karanganyar','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(71,77,14,'D1',3,'KesehatanSragen','Kesehatan Kabupaten Sragen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(72,77,15,'D1',3,'KesehatanGrobogan','Kesehatan Kabupaten Grobogan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(73,77,16,'D1',3,'KesehatanBlora','Kesehatan Kabupaten Blora','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(74,77,17,'D1',3,'KesehatanRembang','Kesehatan Kabupaten Rembang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(75,77,18,'D1',3,'KesehatanPati','Kesehatan Kabupaten Pati','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(76,77,19,'D1',3,'KesehatanKudus','Kesehatan Kabupaten Kudus','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(77,77,20,'D1',3,'KesehatanJepara','Kesehatan Kabupaten Jepara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(78,77,21,'D1',3,'KesehatanDemak','Kesehatan Kabupaten Demak','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(79,77,22,'D1',3,'KesehatanSemarang','Kesehatan Kabupaten Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(80,77,23,'D1',3,'KesehatanTemanggung','Kesehatan Kabupaten Temanggung','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(81,77,24,'D1',3,'KesehatanKendal','Kesehatan Kabupaten Kendal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(82,77,25,'D1',3,'KesehatanBatang','Kesehatan Kabupaten Batang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(83,77,26,'D1',3,'KesehatanPekalongan','Kesehatan Kabupaten Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(84,77,27,'D1',3,'KesehatanPemalang','Kesehatan Kabupaten Pemalang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(85,77,28,'D1',3,'KesehatanTegal','Kesehatan Kabupaten Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(86,77,29,'D1',3,'KesehatanBrebes','Kesehatan Kabupaten Brebes','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(87,77,71,'D1',3,'KesehatanKotaMagelang','Kesehatan Kota Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(88,77,72,'D1',3,'KesehatanKotaSurakarta','Kesehatan Kota Surakarta','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(89,77,73,'D1',3,'KesehatanKotaSalatiga','Kesehatan Kota Salatiga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(90,77,74,'D1',3,'KesehatanKotaSemarang','Kesehatan Kota Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(91,77,75,'D1',3,'KesehatanKotaPekalongan','Kesehatan Kota Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(92,77,76,'D1',3,'KesehatanKotaTegal','Kesehatan Kota Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(94,88,77,'D5',7,'SosialJateng','Sosial Jateng','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(95,88,77,'D3',4,'PUJateng','Pekerjaan Umum Jateng','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(96,88,77,'D4',5,'PRJateng','Perumahan Rakyat Jateng','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(97,88,77,'C0',6,'TrantibumlinmasJateng','Trantibumlinmas Jateng','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(98,77,1,'D5',7,'SosialCilacap','Sosial Cilacap','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(99,77,16,'D5',7,'SosialBlora','Sosial Blora','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(100,77,29,'D5',7,'SosialBrebes','Sosial Brebes','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(102,77,1,'D3',4,'PUCilacap','PU Cilacap','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(103,77,16,'D3',4,'PUBlora','PU Blora','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(104,77,1,'D4',5,'PRCilacap','PR Cilacap','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(105,77,16,'D4',5,'PRBlora','PR Blora','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(106,77,29,'D4',5,'PRBrebes','PR Brebes','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(107,77,1,'C0',6,'TrantibumlinmasCilacap','Trantibumlinmas Cilacap','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(108,77,16,'C0',6,'TrantibumlinmasBlora','Trantibumlinmas Blora','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(109,77,29,'C0',6,'TrantibumlinmasBrebes','Trantibumlinmas Brebes','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(114,77,2,'D4',5,'PRBanyumas','PR Banyumas','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(115,77,3,'D4',5,'PRPurbalingga','PR Purbalingga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(116,77,4,'D4',5,'PRBanjarnegara','PR Banjarnegara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(117,77,5,'D4',5,'PRKebumen','PR Kebumen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(119,77,7,'D4',5,'PRWonosobo','PR Wonosobo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(120,77,8,'D4',5,'PRKabMagelang','PR Kabupaten Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(121,77,71,'D4',5,'PRKotaMagelang','PR Kota Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(122,77,9,'D4',5,'PRBoyolali','PR Boyolali','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(123,77,10,'D4',5,'PRKlaten','PR Klaten','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(124,77,11,'D4',5,'PRSukoharjo','PR Sukoharjo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(125,77,12,'D4',5,'PRWonogiri','PR Wonogiri','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(126,77,13,'D4',5,'PRKaranganyar','PR Karanganyar','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(127,77,14,'D4',5,'PRSragen','PR Sragen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(128,77,15,'D4',5,'PRGrobogan','PR Grobogan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(129,77,17,'D4',5,'PRRembang','PR Rembang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(130,77,18,'D4',5,'PRPati','PR Pati','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(131,77,19,'D4',5,'PRKudus','PR Kudus','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(132,77,2,'D3',4,'PUBanyumas','PU Banyumas','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(133,77,3,'D3',4,'PUPurbalingga','PU Purbalingga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(134,77,4,'D3',4,'PUBanjarnegara','PU Banjarnegara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(135,77,5,'D3',4,'PUKebumen','PU Kebumen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(136,77,6,'D3',4,'PUPurworejo','PU Purworejo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(137,77,7,'D3',4,'PUWonosobo','PU Wonosobo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(138,77,8,'D3',4,'PUKabMagelang','PU Kab Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(139,77,9,'D3',4,'PUBoyolali','PU Boyolali','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(141,77,10,'D3',4,'PUKlaten','PU Klaten','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(142,77,11,'D3',4,'PUSukoharjo','PU Sukoharjo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(143,77,12,'D3',4,'PUWonogiri','PU Wonogiri','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(144,77,13,'D3',4,'PUKaranganyar','PU Karanganyar','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(145,77,14,'D3',4,'PUSragen','PU Sragen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(146,77,15,'D3',4,'PUGrobogan','PU Grobogan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(147,77,17,'D3',4,'PURembang','PU Rembang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(148,77,71,'D5',7,'SosialKotaMagelang','Sosial Kota Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(151,77,2,'D5',7,'SosialBanyumas','Sosial Banyumas','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(152,77,3,'D5',7,'SosialPurbalingga','Sosial Purbalingga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(153,77,4,'D5',7,'SosialBanjarnegara','Sosial Banjarnegara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(154,77,5,'D5',7,'SosialKebumen','Sosial Kebumen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(155,77,6,'D5',7,'SosialPurworejo','Sosial Purworejo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(156,77,7,'D5',7,'SosialWonosobo','Sosial Wonosobo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(157,77,8,'D5',7,'SosialKabMagelang','Sosial Kab Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(158,77,9,'D5',7,'SosialBoyolali','Sosial Boyolali','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(159,77,10,'D5',7,'SosialKlaten','Sosial Klaten','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(160,77,11,'D5',7,'SosialSukoharjo','Sosial Sukoharjo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(161,77,12,'D5',7,'SosialWonogiri','Sosial Wonogiri','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(162,77,13,'D5',7,'SosialKaranganyar','Sosial Karanganyar','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(163,77,14,'D5',7,'SosialSragen','Sosial Sragen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(164,77,15,'D5',7,'SosialGrobogan','Sosial Grobogan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(165,77,17,'D5',7,'SosialRembang','Sosial Rembang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(166,77,18,'D5',7,'SosialPati','Sosial Pati','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(167,77,19,'D5',7,'SosialKudus','Sosial Kudus','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(168,77,71,'C0',6,'TrantibumlinmasKotaMagelang','Trantibumlinmas Kota Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(169,77,8,'C0',6,'TrantibumlinmasKabMagelang','Trantibumlinmas Kab Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(170,77,2,'C0',6,'TrantibumlinmasBanyumas','Trantibumlinmas Banyumas','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(172,77,3,'C0',6,'TrantibumlinmasPurbalingga','Trantibumlinmas Purbalingga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(173,77,4,'C0',6,'TrantibumlinmasBanjarnegara','Trantibumlinmas Banjarnegara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(174,77,5,'C0',6,'TrantibumlinmasKebumen','Trantibumlinmas Kebumen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(175,77,6,'C0',6,'TrantibumlinmasPurworejo','Trantibumlinmas Purworejo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(176,77,7,'C0',6,'TrantibumlinmasWonosobo','Trantibumlinmas Wonosobo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(178,77,9,'C0',6,'TrantibumlinmasBoyolali','Trantibumlinmas Boyolali','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(179,77,10,'C0',6,'TrantibumlinmasKlaten','Trantibumlinmas Klaten','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(180,77,11,'C0',6,'TrantibumlinmasSukoharjo','Trantibumlinmas Sukoharjo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(181,77,12,'C0',6,'TrantibumlinmasWonogiri','Trantibumlinmas Wonogiri','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(182,77,13,'C0',6,'TrantibumlinmasKaranganyar','Trantibumlinmas Karanganyar','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(183,77,14,'C0',6,'TrantibumlinmasSragen','Trantibumlinmas Sragen','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(184,77,15,'C0',6,'TrantibumlinmasGrobogan','Trantibumlinmas Grobogan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(185,77,17,'C0',6,'TrantibumlinmasRembang','Trantibumlinmas Rembang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(186,77,18,'C0',6,'TrantibumlinmasPati','Trantibumlinmas Pati','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(187,77,19,'C0',6,'TrantibumlinmasKudus','Trantibumlinmas Kudus','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(188,77,20,'C0',6,'TrantibumlinmasJepara','Trantibumlinmas Jepara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(189,77,21,'C0',6,'TrantibumlinmasDemak','Trantibumlinmas Demak','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(190,77,22,'C0',6,'TrantibumlinmasKabSemarang','Trantibumlinmas Kab Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(191,77,23,'C0',6,'TrantibumlinmasTemanggung','Trantibumlinmas Temanggung','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(192,77,24,'C0',6,'TrantibumlinmasKendal','Trantibumlinmas Kendal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(193,77,25,'C0',6,'TrantibumlinmasBatang','Trantibumlinmas Batang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(194,77,26,'C0',6,'TrantibumlinmasKabPekalongan','Trantibumlinmas Kab Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(195,77,27,'C0',6,'TrantibumlinmasPemalang','Trantibumlinmas Pemalang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(196,77,28,'C0',6,'TrantibumlinmasKabTegal','Trantibumlinmas Kab Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(197,77,72,'C0',6,'TrantibumlinmasKotaSurakarta','Trantibumlinmas Kota Surakarta','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(198,77,73,'C0',6,'TrantibumlinmasKotaSalatiga','Trantibumlinmas Kota Salatiga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(199,77,74,'C0',6,'TrantibumlinmasKotaSemarang','Trantibumlinmas KotaSemarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(200,77,75,'C0',6,'TrantibumlinmasKotaPekalongan','Trantibumlinmas Kota Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(201,77,76,'C0',6,'TrantibumlinmasKotaTegal','Trantibumlinmas Kota Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(202,77,20,'D4',5,'PRJepara','PR Jepara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(203,77,21,'D4',5,'PRDemak','PR Demak','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(204,77,22,'D4',5,'PRKabSemarang','PR Kab Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(205,77,23,'D4',5,'PRTemanggung','PR Temanggung','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(206,77,24,'D4',5,'PRKendal','PR Kendal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(207,77,25,'D4',5,'PRBatang','PR Batang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(211,77,18,'D3',4,'PUPati','PU Pati','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(212,77,19,'D3',4,'PUKudus','PU Kudus','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(213,77,20,'D3',4,'PUJepara','PU Jepara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(214,77,21,'D3',4,'PUDemak','PU Demak','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(215,77,22,'D3',4,'PUKabSemarang','PU Kab Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(217,77,29,'D3',4,'PUBrebes','PU Brebes','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(218,77,28,'D3',4,'PUKabTegal','PU KabTegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(219,77,71,'D3',4,'PUKotaMagelang','PU Kota Magelang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(220,77,72,'D3',4,'PUKotaSurakarta','PU Kota Surakarta','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(221,77,73,'D3',4,'PUKotaSalatiga','PU Kota Salatiga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(222,77,74,'D3',4,'PUKotaSemarang','PU Kota Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(223,77,75,'D3',4,'PUKotaPekalongan','PU Kota Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(224,77,76,'D3',4,'PUKotaTegal','PU Kota Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(225,77,20,'D5',7,'SosialJepara','Sosial Jepara','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(226,77,21,'D5',7,'SosialDemak','Sosial Demak','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(227,77,22,'D5',7,'SosialKabSemarang','Sosial Kab Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(228,77,23,'D5',7,'SosialTemanggung','Sosial Temanggung','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(229,77,24,'D5',7,'SosialKendal','Sosial Kendal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(230,77,25,'D5',7,'SosialBatang','Sosial Batang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(232,77,27,'D5',7,'SosialPemalang','Sosial Pemalang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(233,77,28,'D5',7,'SosialKabTegal','Sosial Kab Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(235,77,72,'D5',7,'SosialKotaSurakarta','Sosial Kota Surakarta','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(236,77,73,'D5',7,'SosialKotaSalatiga','Sosial Kota Salatiga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(237,77,74,'D5',7,'SosialKotaSemarang','Sosial Kota Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(238,77,75,'D5',7,'SosialKotaPekalongan','Sosial Kota Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(239,77,76,'D5',7,'SosialKotaTegal','Sosial Kota Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(240,77,26,'D4',5,'PRKabPekalongan','PR Kab Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(241,77,27,'D4',5,'PRPemalang','PR Pemalang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(243,77,72,'D4',5,'PRKotaSurakarta','PR Kota Surakarta','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(244,77,28,'D4',5,'PRKabTegal','PR Kab Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(245,77,73,'D4',5,'PRKotaSalatiga','PR Kota Salatiga','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(246,77,74,'D4',5,'PRKotaSemarang','PR Kota Semarang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(247,77,75,'D4',5,'PRKotaPekalongan','PR Kota Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(248,77,76,'D4',5,'PRKotaTegal','PR Kota Tegal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(249,77,25,'D3',4,'PUBatang','PU Batang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(250,77,24,'D3',4,'PUKendal','PU Kendal','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(251,77,26,'D3',4,'PUKabPekalongan','PU Kab Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(252,77,27,'D3',4,'PUPemalang','PU Pemalang','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(254,77,6,'D4',5,'PRPurworejo','PR Purworejo','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(255,77,23,'D3',4,'PUTemanggung','PU Temanggung','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(256,99,77,'A2',1,'adminespm','Admin E-SPM Jateng','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(257,77,26,'D5',7,'SosialKabPekalongan','Sosial Kab Pekalongan','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(258,99,77,'A2',1,'prgrmr2020','Programmer','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(259,99,77,'D4',5,'Perakim2019','Perakim','827ccb0eea8a706c4c34a16891f84e7b',0,0),
	(260,99,74,'A2',1,'Akuadmin','Aku Admin','b723f69a66c50bb2d6c77bccaf3866cd',0,0),
	(261,99,77,'A2',1,'cumacadangan','Admin Cadangan','244ca9a91742e339c92c965dc8230887',0,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
