<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterdata extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('homepage1/head');
        $this->load->view('homepage1/header');
        $this->load->view('homepage1/home');
        $this->load->view('homepage1/footer');
        
	}

    //ini untuk mengambil nama user dengan id_user
    public function get_nama_user($kode){
        $d="select nama from users where id_user='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama;
    }
    //ini untuk mengambil nip user dengan id_user
    public function get_nip_user($kode){
        $d="select nip from users where id_user='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nip;
    }

    //ini untuk mengambil hak akses user dengan id_user
     public function get_akses_user($kode){
        $a="select id_akses from users where id_user='$kode'";
        $half = $this->db->query($a)->row();
        $d="select nama_akses from akses where id_akses='$half->id_akses'";
        $result = $this->db->query($d)->row();
        return $result->nama_akses;
    }

    //ini untuk mengambil kota user dengan id_user
    public function get_kota_user($kode){
        $a="select id_kota from users where id_user='$kode'";
        $half = $this->db->query($a)->row();
        $d="select nama_kota from kota where id_kota='$half->id_kota'";
        $result = $this->db->query($d)->row();
        return $result->nama_kota;
    }

    //ini untuk mengambil skpd user dengan id_user
     public function get_skpd_user($kode){
        $a="select id_skpd from users where id_user='$kode'";
        $half = $this->db->query($a)->row();
        $d="select nm from tablok08 where kd='$half->id_skpd'";
        $result = $this->db->query($d)->row();
        return $result->nm;
    }

    //ini untuk mengambil data spm/ spm yang di ampu oleh user tsb dengan id_user
     public function get_diampu_user($kode){
        $a="select id_pengampu_spm from users where id_user='$kode'";
        $half = $this->db->query($a)->row();
        $d="select nama_jenis_spm from jenis_spm where id_jenis_spm='$half->id_pengampu_spm'";
        $result = $this->db->query($d)->row();
        return $result->nama_jenis_spm;
    }



    //intu get nama saja dari session 
    public function get_nama_akses($kode){
        $d="select nama_akses from akses where id_akses='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama_akses;
    }

    public function get_nama_kota($kode){
        $d="select nama_kota from kota where id_kota='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama_kota;
    }

    public function get_nama_skpd($kode){
        $d="select nm from tablok08 where kd='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nm;
    }

    public function get_nama_pengampu($kode){
        $d="select nama_jenis_spm from jenis_spm where id_jenis_spm='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama_jenis_spm;
    }

    public function get_nama_pelayanan_dasar($kode){
        $d="select nama_pelayanan from jenis_pelayanan_dasar where id_jenis_pelayanan_dasar='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama_pelayanan;
    }

    public function akses_pengampu($kode){
        $d="select nama_akses from akses where id_akses='$kode'";
        $cek = $this->db->query($d);
        if($cek->num_rows() > 0){
        $result = $this->db->query($d)->row();
        return $result->nama_akses;
        }else{
            return 'Belum di Setting';
        }
    }

    public function get_nama_indikator($kode){
        $d="select jenis_indikator from akses where id_indikator='$kode'";
        $result = $this->db->query($d)->row();
        return $result->jenis_indikator;
    }

    public function get_nama_status($kode){
        if($kode == 0){
            $d = "<button bagian=\"button\" class=\"btn btn-success\"> Aktif </button> ";
        }else{
            $d = "<button bagian=\"button\" class=\"btn btn-danger\"> Non Aktif </button> ";
        }
        return $d;
    }

     public function get_select_akses(){
            $query = $this->db->query("SELECT id_akses, nama_akses FROM akses
            ;");
            return $query->result();
    }

    public function get_select_kota(){
            $query = $this->db->query("SELECT id_kota, nama_kota FROM kota
            WHERE id_provinsi = 33;");
            return $query->result();
    }

    public function get_select_skpd(){
        $query = $this->db->query("SELECT kd, nm FROM tablok08
            WHERE aktif = 1;");
            return $query->result();
    }

    public function get_select_pengampu(){
            $query = $this->db->query("SELECT id_jenis_spm, nama_jenis_spm FROM jenis_spm
            ;");
            return $query->result();
    }

    public function get_select_pengampu_w_a(){
        $query = $this->db->query("SELECT id_jenis_spm, nama_jenis_spm FROM jenis_spm WHERE id_jenis_spm <> 1
        ;");
        return $query->result();
}

    public function get_select_jenis_pelayanan(){
            $query = $this->db->query("SELECT id_jenis_pelayanan_dasar, nama_pelayanan , id_jenis_spm FROM jenis_pelayanan_dasar
            ;");
            return $query->result();
    }   



    // get get untuk nama pembilang & penyebut di indikator
    public function get_indikator_pembilang($kode){
        $d="select pembilang from indikator where id_indikator='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama_akses;
    }

    public function get_indikator_penyebut($kode){
        $d="select penyebut from indikator where id_indikator='$kode'";
        $result = $this->db->query($d)->row();
        return $result->nama_akses;
    }





   


	

}
