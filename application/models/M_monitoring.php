<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_targetcapaian extends CI_Model
{

    public $table = 'targetcapaian';
    public $id = 'id_targetcapaian';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $get = $this->db->query("SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut,c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
        FROM targetcapaian a
        INNER JOIN indikator b ON b.id_indikator = a.id_indikator
        INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
        INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
        INNER JOIN users e ON e.id_user = a.id_user 
        WHERE a.deleted_at IS NULL");
        return $get->result();
    }

   
    // get all
    function get_all_by($id, $year)
    {
        $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
        FROM targetcapaian a
        INNER JOIN indikator b ON b.id_indikator = a.id_indikator
        INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
        INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
        INNER JOIN users e ON e.id_user = a.id_user
        WHERE a.id_user = ?
        AND a.tahun = ?
        ";
        $query = $this->db->query($get, array($id, $year));
        return $query->result();
    }

    // get all
    function get_by_id($id)
    {
        $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
        FROM targetcapaian a
        INNER JOIN indikator b ON b.id_indikator = a.id_indikator
        INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
        INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
        INNER JOIN users e ON e.id_user = a.id_user
        WHERE a.id_targetcapaian = ?";
        $query = $this->db->query($get, array($id));
        return $query->row();
    }

    // get all
    function get_by_id_n_user($id, $user)
    {
        $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
        FROM targetcapaian a
        INNER JOIN indikator b ON b.id_indikator = a.id_indikator
        INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
        INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
        INNER JOIN users e ON e.id_user = a.id_user
        WHERE a.id_targetcapaian = ?
        AND a.id_user = ?";
        $query = $this->db->query($get, array($id, $user));
        return $query->row();
    }

    // get all
    function get_by_all_for_admin($kota, $spm, $tahun)
    {
        if($kota == 'all' && $spm == 'all'){
            $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
            FROM targetcapaian a
            INNER JOIN indikator b ON b.id_indikator = a.id_indikator
            INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
            INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
            INNER JOIN users e ON e.id_user = a.id_user
            WHERE a.tahun = ?";
            $query = $this->db->query($get, array($tahun));
            return $query->result();
        }elseif($kota == 'all'){
            $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
            FROM targetcapaian a
            INNER JOIN indikator b ON b.id_indikator = a.id_indikator
            INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
            INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
            INNER JOIN users e ON e.id_user = a.id_user
            WHERE c.id_jenis_spm = ?
            AND a.tahun = ?";
            $query = $this->db->query($get, array($spm, $tahun));
            return $query->result();
        }elseif($spm == 'all'){
            $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
            FROM targetcapaian a
            INNER JOIN indikator b ON b.id_indikator = a.id_indikator
            INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
            INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
            INNER JOIN users e ON e.id_user = a.id_user
            WHERE e.id_kota = ?
            AND a.tahun = ?
            ";
            $query = $this->db->query($get, array($kota, $tahun));
            return $query->result();
        }else{
            $get = "SELECT a.*,b.jenis_indikator as indikator, b.pembilang, b.penyebut, c.nama_pelayanan as pelayanan, d.jumlah as jml_datadasar, d.anggaran, d.tahun as thn_datadasar, e.nama as user
            FROM targetcapaian a
            INNER JOIN indikator b ON b.id_indikator = a.id_indikator
            INNER JOIN jenis_pelayanan_dasar c ON c.id_jenis_pelayanan_dasar = b.id_jenis_pelayanan_dasar
            INNER JOIN datadasar d ON d.id_jenis_pelayanan_dasar = c.id_jenis_pelayanan_dasar
            INNER JOIN users e ON e.id_user = a.id_user
            WHERE e.id_kota = ?
            AND c.id_jenis_spm = ?
            AND a.tahun = ?
            ";
            $query = $this->db->query($get, array($kota, $spm, $tahun));
            return $query->result();
        }
        
        
    }
    
    //insert data
    function insert($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->delete($this->table);
    }

}

/* End of file informasi_model.php */
/* Location: ./application/models/informasi_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-11-18 13:01:12 */
/* http://harviacode.com */