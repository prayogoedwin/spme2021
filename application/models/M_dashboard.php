<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dashboard extends CI_Model
{

    public $table = 'datadasar';
    public $id = 'id_datadasar';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    
    function target_realisasi_capaian_old($tahun)
    {
        $get = $this->db->query("SELECT c.nama_kota AS kota, 
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.anggaran_t,0)) AS anggaran_tahun_pelaporan,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, realisasi,0)) AS realisasi_anggaran_tahun_pelaporan,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS realisasi_capaian,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.realisasi,0)) /SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.anggaran_t,0)) * 100 AS realisasi_anggaran        
        FROM targetcapaian a
        RIGHT JOIN users b ON a.id_user = b.id_user
        INNER JOIN kota c ON b.id_kota = c.id_kota
        GROUP BY b.id_kota;");
        return $get->result();
    }

    function target_realisasi_capaian($tahun)
    {
        $get = $this->db->query("SELECT c.nama_kota AS kota, 
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.anggaran_t,0)) AS anggaran_tahun_pelaporan,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, realisasi,0)) AS realisasi_anggaran_tahun_pelaporan,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS realisasi_capaian,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.realisasi,0)) /SUM(IF(a.tahun = '2020' AND a.deleted_at IS NULL, a.anggaran_t,0)) * 100 AS realisasi_anggara, 
        SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 2 AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 2 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS pendidikan,
        SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 3 AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 3 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS kesehatan,
        SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 4 AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 4 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS pu ,  
        SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 5 AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 5 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS pr , 
        SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 6 AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 6 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS trantibumlinmas , 
        SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 7 AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = '$tahun' AND x.id_jenis_spm = 7 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS sosial
           
        FROM targetcapaian a
        RIGHT JOIN users b ON a.id_user = b.id_user
        INNER JOIN kota c ON b.id_kota = c.id_kota
        LEFT JOIN indikator y ON a.id_indikator = y.id_indikator
        LEFT JOIN jenis_spm x ON y.id_jenis_spm = x.id_jenis_spm
        GROUP BY b.id_kota;");
        return $get->result();
    }

    
   
    function kendala_old($tahun)
    {
        $get = $this->db->query("SELECT c.nama_jenis_spm as spm_s,
        COUNT(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.kendala, 0)) as jml_kendala
        FROM targetcapaian a
        RIGHT JOIN users b ON a.id_user = b.id_user
        INNER JOIN jenis_spm c ON b.id_pengampu_spm = c.id_jenis_spm
        WHERE b.id_pengampu_spm != 1 AND a.tahun = '$tahun'
        GROUP BY b.id_pengampu_spm;");
        return $get->result();
    }


    function kendala($tahun)
    {
        $get = $this->db->query("SELECT c.id, c.nama as masalah,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, 1, 0)) as jml_kendala
        FROM targetcapaian a
        RIGHT JOIN kendala c ON a.kendala = c.id
        GROUP BY c.id;");
        return $get->result();
    }


    public function spm($tahun, $kota){
        $query = $this->db->query("SELECT d.nama_jenis_spm AS spm, 
                COALESCE(SUM(IF(a.tahun = '$tahun' AND b.id_kota = '$kota' AND a.deleted_at IS NULL, a.jumlah_pembilang, 0)) / SUM(IF(a.tahun = '$tahun' AND b.id_kota = '$kota' AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100, 0) AS realisasi_capaian
                FROM targetcapaian a
                INNER JOIN users b ON a.id_user = b.id_user
                INNER JOIN kota c ON b.id_kota = c.id_kota
                INNER JOIN jenis_spm d ON b.id_pengampu_spm = d.id_jenis_spm
                GROUP BY d.id_jenis_spm;");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function realisasi_ang($tahun, $kota){
        $query = $this->db->query("SELECT d.nama_jenis_spm AS spm, 
                COALESCE(SUM(IF(a.tahun = '$tahun' AND b.id_kota = '$kota' AND a.deleted_at IS NULL, a.realisasi, 0)))  AS realisasi_anggaran
                FROM targetcapaian a
                INNER JOIN users b ON a.id_user = b.id_user
                INNER JOIN kota c ON b.id_kota = c.id_kota
                INNER JOIN jenis_spm d ON b.id_pengampu_spm = d.id_jenis_spm
                GROUP BY d.id_jenis_spm;");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function kendala_pie($tahun, $kota){
        $query = $this->db->query("SELECT d.id, d.nama as masalah,
        SUM(IF(a.tahun = '$tahun' AND b.id_kota = '$kota' AND a.deleted_at IS NULL, 1, 0)) as jml_kendala
        FROM targetcapaian a
        INNER JOIN users b ON a.id_user = b.id_user
        INNER JOIN kota c ON b.id_kota = c.id_kota
        RIGHT JOIN kendala d ON a.kendala = d.id
        GROUP BY d.id;");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function capai_an_spm_per($bidang, $akses, $kota, $tahun){
        $query = $this->db->query("SELECT COALESCE(SUM(IF(b.id_jenis_spm = '$bidang' AND b.akses = '$akses'  AND d.id_kota = '$kota' AND a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_pembilang, 0)) 
        / SUM(IF(b.id_jenis_spm = '$bidang' AND b.akses = '$akses'  AND d.id_kota = '$kota' AND a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100, 0) AS realisasi_capaian, b.pembilang
        FROM targetcapaian a
        INNER JOIN indikator b ON a.id_indikator = b.id_indikator
        INNER JOIN users d ON a.id_user = d.id_user
        WHERE b.id_jenis_spm = '$bidang' AND b.akses = '$akses'  AND d.id_kota = '$kota' AND a.tahun = '$tahun' AND a.deleted_at IS NULL
        GROUP BY b.id_indikator");
        return $query->result();
    }

    public function capai_an_spm_per_($akses, $kota, $tahun){
        $query = $this->db->query("SELECT COALESCE(SUM(IF(b.akses = '$akses' AND d.id_kota = '$kota' AND a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_pembilang, 0)) / SUM(IF(b.akses = '$akses' AND d.id_kota = '$kota' AND a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100, 0) AS realisasi_capaian , c.nama_jenis_spm FROM targetcapaian a INNER JOIN indikator b ON a.id_indikator = b.id_indikator INNER JOIN users d ON a.id_user = d.id_user INNER JOIN jenis_spm c ON b.id_jenis_spm = c.id_jenis_spm -- WHERE b.id_jenis_spm = '2' AND d.id_kota = '1' AND a.tahun = '2020' AND a.deleted_at IS NULL GROUP BY c.id_jenis_spm");
        return $query->result();
    }

   


}?>