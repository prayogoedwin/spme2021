<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
		$this->load->model('M_targetcapaian');
	}
	
	
    public function index()
	{
        
        $tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y');
		}else{
			$year = $tahun ;
        }
        
        $data['alldata'] = $this->db->query("SELECT id_user, nama FROM users WHERE id_akses <> 99 AND hapus = 0;")->result();
        $data['year'] = $year;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/monitoring/index', $data);
		// $this->load->view('backend/template/footer');
        
	}

	
	
}
