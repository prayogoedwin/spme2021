<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen_tapem extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_dokumen_tapem');
        $this->load->model('M_targetcapaian');
		is_login();
	}
	
	public function index()
	{
		$id = $this->session->userdata('id_pengguna');
        $akses = get_user($id)->id_akses;
        $id_kota = get_user($id)->id_kota;
    
        $data['alldata'] = $this->M_dokumen_tapem->get_all_by($id);
        
		$tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
		$data['tahun'] = $year;
		
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/dokumen_tapem/index', $data);
		// $this->load->view('backend/template/footer');
        
    }

    public function tambah()
	{	
        $id = $this->session->userdata('id_pengguna');
        $id_kota = get_user($id)->id_kota;
        // $spm = nama_spm(get_user($id)->id_pengampu_spm)->nama_jenis_spm;
        // $id_spm = nama_spm(get_user($id)->id_pengampu_spm)->id_jenis_spm;
        // $spm = nama_spm($uri_spm)->nama_jenis_spm;
        // $id_spm = $uri_spm;
        $data['bidang_urusan'] = $this->db->query("SELECT * FROM jenis_spm WHERE id_jenis_spm != 1")->result();
        $data['jenis_pelayanan_dasar'] = $this->db->query("SELECT * FROM jenis_pelayanan_dasar")->result();
        $data['tahuns'] = $this->db->query("SELECT DISTINCT(targetcapaian.tahun) FROM targetcapaian 
        INNER JOIN users ON users.id_user = targetcapaian.id_user WHERE users.id_kota = '$id_kota'")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/dokumen_tapem/tambah',$data);
    }
    
    public function aksi_tambah(){ 
        $id = $this->session->userdata('id_pengguna');
        $tahun 	    = $this->input->post('tahun');
        $latar 	    = $this->input->post('latar');
        $dasar 	    = $this->input->post('dasar');
        $umum 	    = $this->input->post('umum');
        $arah 	    = $this->input->post('arah');
        $spm 	    = $this->input->post('spm');
        $jenis 	    = $this->input->post('jenis');
        $target 	    = $this->input->post('target');
        $realisasi 	    = $this->input->post('realisasi');
        $alokasi 	    = $this->input->post('alokasi');
        $dukungan 	    = $this->input->post('dukungan');
        $permasalahan 	    = $this->input->post('permasalahan');
        $prog 	    = $this->input->post('prog');
        $penutup 	    = $this->input->post('penutup');
       
           $data=array(
                "id_user"            => $id,
                "tahun"              => $tahun,
				"latar_belakang"     => $latar,
                "dasar_hukum"        => $dasar,
                "kebijakan_umum"     => $umum,
                "arah_kebijakan"     => $arah,
                "bidang_urusan"      => $spm,
                "jenis_pelayanan"    => $jenis,
                "target_pencapaian"  => $target,
                "realisasi"          => $realisasi,
                "alokasi_anggaran"   => $alokasi,
                "dukungan_personil"  => $dukungan,
                "permasalahan_solusi"=> $permasalahan,
                "program_kegiatan"   => $prog,
                "penutup"            => $penutup
			); 
			
        $add = $this->M_dokumen_tapem->insert($data);
        // echo json_encode($add);
		if ($add){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Selamat, Berhasil tambah Dokumen');
			redirect(site_url('dokumen_tapem'));
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Berhasil tambah Dokumen');
			redirect(site_url('dokumen_tapem'));
		}
    }
    
    public function cetak($id_dokumen)
	{	
        $id = $this->session->userdata('id_pengguna');
        $id_kota = get_user($id)->id_kota;
        $dok = $this->db->query("SELECT * FROM dokumen WHERE id_dokumen = '$id_dokumen'")->row();
        $data['dokumen'] = $dok;
        // $spm = cek_bidang_urusan($dok->bidang_urusan)->id_jenis_spm;
        $data['targetcapaian'] = $this->M_targetcapaian->get_all_by_user_kota($id_kota, $dok->tahun);
        // $this->load->view('backend/template/head');
        // $this->load->view('backend/template/header');
        // $this->load->view('backend/template/sidebar');
		$this->load->view('backend/dokumen/cetak',$data);
    }

    public function edit($id_dokumen)
	{	
        $id = $this->session->userdata('id_pengguna');
        // $id = $this->session->userdata('id_pengguna');
        // $spm = nama_spm(get_user($id)->id_pengampu_spm)->nama_jenis_spm;
        // $id_spm = nama_spm(get_user($id)->id_pengampu_spm)->id_jenis_spm;
        $dok = $this->db->query("SELECT * FROM dokumen WHERE id_dokumen = '$id_dokumen'")->row();
        // $uri_spm = cek_bidang_urusan($dok->bidang_urusan)->id_jenis_spm;

        // $spm = $dok->bidang_urusan;
        // $id_spm = $uri_spm;

        $data['dokumen'] = $dok;
        // $data['bidang_urusan'] = $spm;
        $data['bidang_urusan'] = $this->db->query("SELECT * FROM jenis_spm WHERE id_jenis_spm != 1")->result();
        $data['jenis_pelayanan_dasar'] = $this->db->query("SELECT * FROM jenis_pelayanan_dasar")->result();
        $data['tahuns'] = $this->db->query("SELECT DISTINCT(tahun) FROM targetcapaian WHERE id_user = '$id'")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/dokumen_tapem/edit',$data);
    }

    public function aksi_edit(){ 
        $id = $this->session->userdata('id_pengguna');
        $id_dokumen 	    = $this->input->post('id_dokumen');
        $tahun 	    = $this->input->post('tahun');
        $latar 	    = $this->input->post('latar');
        $dasar 	    = $this->input->post('dasar');
        $umum 	    = $this->input->post('umum');
        $arah 	    = $this->input->post('arah');
        $spm 	    = $this->input->post('spm');
        $jenis 	    = $this->input->post('jenis');
        $target 	    = $this->input->post('target');
        $realisasi 	    = $this->input->post('realisasi');
        $alokasi 	    = $this->input->post('alokasi');
        $dukungan 	    = $this->input->post('dukungan');
        $permasalahan 	    = $this->input->post('permasalahan');
        $prog 	    = $this->input->post('prog');
        $penutup 	    = $this->input->post('penutup');
       
           $data=array(
                "id_user"            => $id,
                "tahun"              => $tahun,
				"latar_belakang"     => $latar,
                "dasar_hukum"        => $dasar,
                "kebijakan_umum"     => $umum,
                "arah_kebijakan"     => $arah,
                "bidang_urusan"      => $spm,
                "jenis_pelayanan"    => $jenis,
                "target_pencapaian"  => $target,
                "realisasi"          => $realisasi,
                "alokasi_anggaran"   => $alokasi,
                "dukungan_personil"  => $dukungan,
                "permasalahan_solusi"=> $permasalahan,
                "program_kegiatan"   => $prog,
                "penutup"            => $penutup
			); 
			
        $add = $this->M_dokumen_tapem->update($id_dokumen, $data);
        // echo json_encode($add);
		if ($add){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Selamat, Berhasil edit Dokumen');
			redirect(site_url('dokumen_tapem'));
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Berhasil edit Dokumen');
			redirect(site_url('dokumen_tapem'));
		}
    }
    
}
?>