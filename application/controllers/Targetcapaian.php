<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Targetcapaian extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
		$this->load->model('M_targetcapaian');
		$this->load->model('M_indikator');
		is_login();
	}
	
	public function index()
	{
		$id = $this->session->userdata('id_pengguna');
		$tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
		$data['alldata'] = $this->M_targetcapaian->get_all_by($id, $year);
		$data['tahun'] = $year;
	
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/targetcapaian/index', $data);
		// $this->load->view('backend/template/footer');
        
	}

	public function admin()
	{
		$id = $this->session->userdata('id_pengguna');
        $akses = get_user($id)->id_akses;
		if($akses != 99){
			redirect('targetcapaian');
        }else{
			$kota = $this->input->post('kota');
			if($kota == ''){
				$kab = 'all';
			}else{
				$kab = $kota;
			}

			$spm = $this->input->post('spm');
			if($spm == ''){
				$s = 'all';
			}else{
				$s = $spm;
			}

			$tahun = $this->input->post('tahun');
			if($tahun == ''){
				$year = date('Y')-1;
			}else{
				$year = $tahun ;
			}
			$data['alldata'] = $this->M_targetcapaian->get_by_all_for_admin($kab, $s, $year);
			$data['tahun'] = $year;
			$data['kota'] = $kab;
			$data['spm'] = $s;
			$data['selkota']  = $this->Masterdata->get_select_kota();
			$data['selspm']  = $this->Masterdata->get_select_pengampu_w_a();
		}
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/targetcapaian/admin', $data);
		// $this->load->view('backend/template/footer');
        
	}

	public function tambah()
	{
		$id = $this->session->userdata('id_pengguna');
       
        $pengampu = get_user($id)->id_pengampu_spm;
        $akses = get_user($id)->id_akses;
		
		$tahun = date('Y')-1;

        if($akses == 99){
            redirect('targetcapaian');
        }else{
            $data['alldata'] = $this->M_indikator->get_indikator_targetcapaian($pengampu, $id, $tahun);
		}
		
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/targetcapaian/tambah', $data);
	}

	public function aksi_tambah(){ 
        $idi 		= $this->input->post('id_indikator');
        $user 		= $this->input->post('id_pengguna');
        $jumlah 	= $this->input->post('target');
        $tahun		= $this->input->post('tahun');
		$anggaran	= $this->input->post('anggaran');
		$anggaran	= $this->input->post('anggaran');
		$jumlah_pembilang	= $this->input->post('jumlah_pembilang');
		$jumlah_penyebut = $this->input->post('jumlah_penyebut');
		$realisasi  = $this->input->post('realisasi');
		$kendala 	= $this->input->post('kendala');
		$now 		= $this->input->post('now');
		
        $newdata 	= array();
        for ($i = 0; $i < count($idi); $i++)
        {
           $newdata[$i]=array(
                "id_indikator"  			=> $idi[$i],
                "id_user"                   => $user[$i],
                "target"                    => $jumlah[$i],
                'tahun'                     => $tahun[$i],
				'anggaran_t'                => $anggaran[$i],
				'jumlah_pembilang'			=> $jumlah_pembilang[$i],
				'jumlah_penyebut'			=> $jumlah_penyebut[$i],
				'realisasi'					=> $realisasi[$i],
				'kendala'					=> $kendala[$i],
				'created_at'				=> $now[$i],

            ); 
        }
        
        $id = $this->session->userdata('id_pengguna');
        $cek = hidden_tambah_targetcapaian($id);
        if($cek == 0){
            $input = $this->M_targetcapaian->insert($newdata);
            if ($input){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Selamat, Berhasil menambah targetcapaian');
			redirect(site_url('targetcapaian'));
    		}else{
    			$this->session->set_flashdata('info', 'danger');
    			$this->session->set_flashdata('message', 'Gagal Berhasil menambah targetcapaian');
    			redirect(site_url('targetcapaian'));
    		}
        }else{
                $this->session->set_flashdata('info', 'danger');
    			$this->session->set_flashdata('message', 'Gagal Berhasil menambah targetcapaian');
    			redirect(site_url('targetcapaian'));
        }
		
	
	}
	
	public function edit($id)
	{
		$user = $this->session->userdata('id_pengguna');
        $akses = get_user($user)->id_akses;
		if($akses != 99){
			$data['alldata'] = $this->M_targetcapaian->get_by_id_n_user($id, $user);
        }else{
            $data['alldata'] = $this->M_targetcapaian->get_by_id($id);
		}
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/targetcapaian/edit', $data);
		
        
	}

	public function aksi_edit(){ 
		$id 				= $this->input->post('id');
		$target 			= $this->input->post('target');
		$jumlah_pembilang 	= $this->input->post('jumlah_pembilang');
		$jumlah_penyebut 	= $this->input->post('jumlah_penyebut');
		$anggaran_t 		= $this->input->post('anggaran_t');
		$realisasi 			= $this->input->post('realisasi');
		$kendala 			= $this->input->post('kendala');
       
		$data = array(
			"target"                    		=> $target,
			'anggaran_t'						=> $anggaran_t,
			'realisasi'							=> $realisasi,
			'kendala'							=> $kendala,
			'jumlah_pembilang'                  => $jumlah_pembilang,
			'jumlah_penyebut'					=> $jumlah_penyebut,	
		); 

		// echo json_encode($data);
			
		$update = $this->M_targetcapaian->update($id, $data);

		// echo json_encode($update);
		
		if ($update){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Selamat, Berhasil edit Targetcapaian');
			redirect(site_url('targetcapaian/edit/'.$id));
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Berhasil edit Targetcapaian');
			redirect(site_url('targetcapaian/edit/'.$id));
		}
	}
	
}
