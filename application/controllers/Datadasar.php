<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datadasar extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
		$this->load->model('M_datadasar');
		$this->load->model('M_pelayanan');
		is_login();
	}
	
	public function index()
	{
		$id = $this->session->userdata('id_pengguna');
        $akses = get_user($id)->id_akses;
        
        if($akses != 99){
            $data['alldata'] = $this->M_datadasar->get_all_by($id);
        }else{
            $data['alldata'] = $this->M_datadasar->get_all();
		}

		$tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
		$data['tahun'] = $year;
		
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/datadasar/index', $data);
		// $this->load->view('backend/template/footer');
        
	}

	public function admin()
	{
		$id = $this->session->userdata('id_pengguna');
        $akses = get_user($id)->id_akses;
		if($akses != 99){
			redirect('datadasar');
        }else{
            $data['alldata'] = $this->M_datadasar->get_all();
		}

		$tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y');
		}else{
			$year = $tahun ;
		}
		$data['tahun'] = $year;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/datadasar/index', $data);
		// $this->load->view('backend/template/footer');
        
	}

	public function tambah()
	{
		$id = $this->session->userdata('id_pengguna');
       
        $pengampu = get_user($id)->id_pengampu_spm;
        $akses = get_user($id)->id_akses;
        
        if($akses != 99){
            $data['alldata'] = $this->M_pelayanan->get_all_by($pengampu, $akses);
        }else{
            $data['alldata'] = $this->M_pelayanan->get_all($pengampu);
		}
		
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/datadasar/tambah', $data);
	}

	public function aksi_tambah(){ 
        $idp 		= $this->input->post('id_pelayanan');
        $user 		= $this->input->post('id_pengguna');
        $jumlah 	= $this->input->post('jumlah');
        $tahun		= $this->input->post('tahun');
		$anggaran	= $this->input->post('anggaran');
		$th_pelaporan	= $this->input->post('th_pelaporan');
		$now 		= $this->input->post('now');
        $newdata 	= array();
        for ($i = 0; $i < count($idp); $i++)
        {
           $newdata[$i]=array(
                "id_jenis_pelayanan_dasar"  => $idp[$i],
                "id_user"                   => $user[$i],
                "jumlah"                    => $jumlah[$i],
                'tahun'                     => $tahun[$i],
				'anggaran'                  => $anggaran[$i],
				'th_pelaporan'              => $th_pelaporan[$i],
				'created_at'				=> $now[$i],
            ); 
        }
		$input = $this->M_datadasar->insert($newdata);
		if ($input){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Selamat, Berhasil menambah Datadasar');
			redirect(site_url('datadasar'));
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Berhasil menambah Datadasar');
			redirect(site_url('datadasar'));
		}
	}
	
	public function edit($id)
	{
		$user = $this->session->userdata('id_pengguna');
        $akses = get_user($user)->id_akses;
		if($akses != 99){
			$data['alldata'] = $this->M_datadasar->get_by_id_n_user($id, $user);
        }else{
            $data['alldata'] = $this->M_datadasar->get_by_id($id);
		}
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/datadasar/edit', $data);
		// $this->load->view('backend/template/footer');
        
	}

	public function aksi_edit(){ 
        $id 		= $this->input->post('id');
        $jumlah 	= $this->input->post('jumlah');
		$anggaran	= $this->input->post('anggaran');
		$th_pelaporan	= $this->input->post('th_pelaporan');
       
           $data=array(
                "jumlah"                    => $jumlah,
				"anggaran"                  => $anggaran,
				"th_pelaporan"              => $th_pelaporan
				
			); 
			
		$update = $this->M_datadasar->update($id, $data);
		if ($update){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Selamat, Berhasil edit Datadasar');
			redirect(site_url('datadasar/edit/'.$id));
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Berhasil edit Datadasar');
			redirect(site_url('datadasar/edit/'.$id));
		}
	}
	
}
