<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
	
        $this->load->model('M_dashboard');


	}

	public function index()
	{

        $tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
        $data['tahun'] = $year;

		$kota = $this->input->post('kota');
		if($kota == ''){
			$kab = '1';
		}else{
			$kab = $kota ;
		}
        $data['kota'] = $kab;

        
            $data['realisasi'] = $this->M_dashboard->target_realisasi_capaian($year);
			$data['realisasis'] = $this->M_dashboard->target_realisasi_capaian($year);
            $data['kendala'] = $this->M_dashboard->kendala($year);
			$data['chartkota'] = $this->M_dashboard->spm($year, $kab);
			$data['chartprov'] = $this->M_dashboard->spm($year, 77);

			$data['piekota'] = $this->M_dashboard->kendala_pie($year, $kab);
			$data['pieprov'] = $this->M_dashboard->kendala_pie($year, 77);

			$data['real_ang'] = $this->M_dashboard->realisasi_ang($year, $kab);
			$data['real_ang_pr'] = $this->M_dashboard->realisasi_ang($year, 77);

			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('2', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('3', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('4', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('5', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('6', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('7', '88', '77', $tahun);

			$data['kotas'] = $this->db->query("SELECT * FROM kota WHERE id_kota < 77")->result();
			
			// $data['chartprov'] = $this->M_dashboard->spm_prov($year, '77');
        
        
			$this->load->view('backend/template/head');
			$this->load->view('backend/template/header_umum');
			// $this->load->view('backend/template/sidebar_umum');
			
			$this->load->view('backend/dashboard_umum', $data);
        
		// $this->load->view('backend/template/footer');
        
	}

	public function capaian_bidang($akses = 77, $kota = 1, $tahun = 2020)
	{
    
			$x = $this->M_dashboard->capai_an_spm_per_($akses, $kota, $tahun);
			echo Json_encode($x);
			// $data=array();
			// foreach($x as $d){
				
			// }

			// $results = array();

            //     foreach($x as $g){

			// 		$results[] = array(
			// 			$g->nama_jenis_spm,
			// 			$g->realisasi_capaian
			// 		);

			// 		// echo "data:['.$results.']";  
			// 		$results[] = $g;
                    
            //     }

			// 	echo $results;
        
	}

	

	public function capaian_bidang_provinsi()
	{

        $tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
        $data['tahun'] = $year;

		$kota = $this->input->post('kota');
		if($kota == ''){
			$kab = '1';
		}else{
			$kab = $kota ;
		}
        $data['kota'] = $kab;

			$data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('2', '88', '77', $year);
			$data['capaian_per_bidang_prov3'] = $this->M_dashboard->capai_an_spm_per('3', '88', '77', $year);
			$data['capaian_per_bidang_prov4'] = $this->M_dashboard->capai_an_spm_per('4', '88', '77', $year);
			$data['capaian_per_bidang_prov5'] = $this->M_dashboard->capai_an_spm_per('5', '88', '77', $year);
			$data['capaian_per_bidang_prov6'] = $this->M_dashboard->capai_an_spm_per('6', '88', '77', $year);
			$data['capaian_per_bidang_prov7'] = $this->M_dashboard->capai_an_spm_per('7', '88', '77', $year);
			$data['kotas'] = $this->db->query("SELECT * FROM kota WHERE id_kota < 77")->result();

			// echo Json_encode($data);

			$this->load->view('backend/template/head');
			$this->load->view('backend/template/header_umum');
		
			
			$this->load->view('backend/dashboard_umum_per_bidang', $data);
        
		// $this->load->view('backend/template/footer');
        
	}

	public function capaian_bidang_kabkota()
	{

        $tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
        $data['tahun'] = $year;

		$kota = $this->input->post('kota');
		if($kota == ''){
			$kab = '1';
		}else{
			$kab = $kota ;
		}
        $data['kota'] = $kab;

			$data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('2', '77', $kab, $year);
			$data['capaian_per_bidang_prov3'] = $this->M_dashboard->capai_an_spm_per('3', '77', $kab, $year);
			$data['capaian_per_bidang_prov4'] = $this->M_dashboard->capai_an_spm_per('4', '77', $kab, $year);
			$data['capaian_per_bidang_prov5'] = $this->M_dashboard->capai_an_spm_per('5', '77', $kab, $year);
			$data['capaian_per_bidang_prov6'] = $this->M_dashboard->capai_an_spm_per('6', '77', $kab, $year);
			$data['capaian_per_bidang_prov7'] = $this->M_dashboard->capai_an_spm_per('7', '77', $kab, $year);
			$data['kotas'] = $this->db->query("SELECT * FROM kota WHERE id_kota < 77")->result();

			// echo Json_encode($data);

			$this->load->view('backend/template/head');
			$this->load->view('backend/template/header_umum');
		
			
			$this->load->view('backend/dashboard_umum_per_bidang_kabkota', $data);
        
		// $this->load->view('backend/template/footer');
        
	}

	

	


	public function login()
	{
		$this->load->helper('url');
		$this->load->view('backend/login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url());
	}

	public function cek_login(){
		$user = $this->input->post('username');
		$pass = md5($this->input->post('password'));
		$this->load->model('M_welcome');
		$login = $this->M_welcome->login($user, $pass);
		if($login){
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('notice', 'BERHASIL');
			$this->session->set_flashdata('message', 'Selamat Datang, Anda Berhasil Melakukan Login ke Aplikasi SPM-E Jawa Tengah');
			redirect(site_url('dashboard'));
		}else{
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('notice', 'GAGAL');
			$this->session->set_flashdata('message', 'Maaf, login gagal');
			redirect(site_url('welcome/login'));
		}
	}

	public function listKecamatan(){
        // Ambil data ID Kota yang dikirim via ajax post
        $id_kota = $this->input->post('id_kota');

        $kecamatan = $this->Auto_model->get_kecamatan_by_kota($id_kota);
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option value=''>Silakan Pilih Kecamatan</option>";

        foreach($kecamatan as $kec){
          $lists .= "<option value='".$kec->id."'>".$kec->name."</option>"; // Tambahkan tag option ke variabel $lists
        }

        $callback = array('list_kecamatan'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	public function listDesa(){
        // Ambil data ID Kecamata yang dikirim via ajax post
        $id_kecamatan = $this->input->post('id_kecamatan');

        $desa = $this->Auto_model->get_desa_by_kecamatan($id_kecamatan);
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option value=''>Silakan Pilih Desa</option>";

        foreach($desa as $ds){
          $lists .= "<option value='".$ds->id."'>".$ds->name."</option>"; // Tambahkan tag option ke variabel $lists
        }

        $callback = array('list_desa'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }
}
