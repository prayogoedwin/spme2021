<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
		$this->load->model('M_informasi');
        $this->load->model('M_kelolauser');
        $this->load->model('M_dashboard');
        is_login();

	}
	
	public function index()
	{
        $id = $this->session->userdata('id_pengguna');
        $akses = get_user($id)->id_akses;
        $data['informasi'] = $this->M_informasi->get_info($id);

        $tahun = $this->input->post('tahun');
		if($tahun == ''){
			$year = date('Y')-1;
		}else{
			$year = $tahun ;
		}
        $data['tahun'] = $year;

		$kota = $this->input->post('kota');
		if($kota == ''){
			$kab = '1';
		}else{
			$kab = $kota ;
		}
        $data['kota'] = $kab;

        // if($akses == '99' || $akses == '88' ){
        //     $data['realisasi'] = $this->M_dashboard->target_realisasi_capaian($year);
        //     $data['kendala'] = $this->M_dashboard->kendala($year);
        // }

            $data['realisasi'] = $this->M_dashboard->target_realisasi_capaian($year);
			$data['realisasis'] = $this->M_dashboard->target_realisasi_capaian($year);
            $data['kendala'] = $this->M_dashboard->kendala($year);
			$data['chartkota'] = $this->M_dashboard->spm($year, $kab);
			$data['chartprov'] = $this->M_dashboard->spm($year, 77);

			$data['piekota'] = $this->M_dashboard->kendala_pie($year, $kab);
			$data['pieprov'] = $this->M_dashboard->kendala_pie($year, 77);

			$data['real_ang'] = $this->M_dashboard->realisasi_ang($year, $kab);
			$data['real_ang_pr'] = $this->M_dashboard->realisasi_ang($year, 77);

			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('2', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('3', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('4', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('5', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('6', '88', '77', $tahun);
			// $data['capaian_per_bidang_prov2'] = $this->M_dashboard->capai_an_spm_per('7', '88', '77', $tahun);

			$data['kotas'] = $this->db->query("SELECT * FROM kota WHERE id_kota < 77")->result();
        
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/dashboard_admin', $data);
        // if($akses == '99' || $akses == '88' ){
        //     $this->load->view('backend/dashboard_admin', $data);
        // }else{
        //     $this->load->view('backend/dashboard', $data);
        // }
		// $this->load->view('backend/template/footer');
        
	}

	public function edit_password()
    {
		$id = $this->session->userdata('id_pengguna');
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/user/form_edit_password',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'waduh, gagal ');
            redirect(site_url('dashboard'));
        }
	}
	
	public function aksi_edit_password()
    {
            $id = $this->input->post('id_user',TRUE);
            $edit = array(
                'password'              => md5($this->input->post('password')),
            );
            $data = $this->M_kelolauser->update($id, $edit);
            //echo json_encode($data);
            if($data){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('dashboard'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'waduh, gagal ');
                redirect(site_url('dashboard/edit_password'));
            }
          
    }
	
}
