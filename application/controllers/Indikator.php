<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_indikator');
	}
	
	public function index()
	{
		$id = $this->session->userdata('id_pengguna');
		$akses = get_user($id)->id_akses;
		$pengampu = get_user($id)->id_pengampu_spm;
		if($akses == '99'){
			$data['alldata'] = $this->M_indikator->get_all();
		}else{
			$data['alldata'] = $this->M_indikator->get_all_by($akses, $pengampu);
		}
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
		$this->load->view('backend/indikator/index', $data);
		// $this->load->view('backend/template/footer');
        
	}
	
}
