 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Monitoring 
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Monitoring </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

      <div class="box">
      <div class="box-header">
     
        <?php echo form_open_multipart('monitoring/index');  ?>
        <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
                <select class="form-control select2" style="width: 100%;" name="tahun">
                  
                <?php
                $thn_skr = date('Y');
                $sel = NULL;
                for ($x = $thn_skr; $x >= 2019; $x--) {  
                ?>
                    <option <?php echo $year == $x ? 'selected' : ''; ?> value="<?php echo $x ?>"><?php echo $x?></option>
                <?php
                }
                ?>
                  
                </select>
              </div>
              <!-- /.form-group -->
        </div>
        <div class="col-md-2">
        <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
        </div>
        </div>
        <?php echo form_close(); ?>

        </div>
        <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>User</th>
                  <th>Datadasar</th>
                  <th>Targetcapaian</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td><?=$all->nama?></td>
                  <td>
                  <?php
                  $hit = datadasar($all->id_user, $year);
                  if($hit > 0){
                    echo '<button type="button" class="btn btn-sm btn-success">SUDAH <i class="fa fa-check"></i></button>';
                  }else{
                    echo '<button type="button" class="btn btn-sm btn-danger">BELUM <i class="fa fa-times"></i></button>';
                  }
                  ?></td>

<td>
                  <?php
                  $hit = targetcapaian($all->id_user, $year);
                  if($hit > 0){
                    echo '<button type="button" class="btn btn-sm btn-success">SUDAH <i class="fa fa-check"></i></button>';
                  }else{
                    echo '<button type="button" class="btn btn-sm btn-danger">BELUM <i class="fa fa-times"></i></button>';
                  }
                  ?></td>
                  
                  
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
             'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>