<body class="hold-transition skin-green layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url('dashboard')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SPM</b>E</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SPME</b>Jateng</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a> -->

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
          
          
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url('assets/Login_v1/images/logo-jateng.png')?>" class="user-image" alt="User Image">
              <span class="hidden-xs">SPM-e Jawa Tengah</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url('assets/Login_v1/images/logo-jateng.png')?>" class="img-circle" alt="User Image">

                <p>
                Silahkan login untuk mengelola data SPM-e Jawa Tengah
                  <!-- <small>Online</small> -->
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
          
                
                <div style="text-align:center">
                  <a href="<?=base_url('welcome/login')?>" class="btn btn-default btn-flat">Login Sekarang</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->