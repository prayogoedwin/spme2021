<?php
$userx = $this->session->userdata('id_pengguna');
$aksesx = get_user($userx)->id_akses;
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        <img src="<?=base_url('assets/user.png')?>" class="img-circle" alt="User Image"> 
        </div>
        <div class="pull-left info">
        <p><?= get_user($this->session->userdata('id_pengguna'))->nip; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li><a href="<?=base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
       
        <?php if($aksesx == 99){ ?>
        <li><a href="<?=base_url()?>kelolauser"><i class="fa fa-users"></i> <span>Kelola Pengguna</span></a></li>
        <!-- <li><a href="<?=base_url()?>monitoring"><i class="fa fa-desktop"></i> <span>Monitoring</span></a></li> -->

        <li class="treeview">
          <a href="#">
            <i class="fa fa-sitemap"></i> <span>Masterdata</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()?>spm"><i class="fa fa-book"></i> <span>Jenis SPM</span></a></li>
            <li><a href="<?=base_url()?>pelayanan"><i class="fa fa-book"></i> <span>Jenis Pelayanan Dasar</span></a></li>
            <li><a href="<?=base_url()?>informasi"><i class="fa fa-book"></i> <span>Informasi</span></a></li>
          </ul>
        </li>
        <?php } ?>

        <?php if($aksesx == 99 || $aksesx == 88 || $aksesx == 77){ ?>
        <li><a href="<?=base_url()?>datadasar"><i class="fa fa-database"></i> <span>Datadasar</span></a></li>
      
        <li><a href="<?=base_url()?>indikator"><i class="fa fa-heart-o"></i> <span>Indikator</span></a></li>
        <?php } ?>

        <?php if($aksesx == 99){ 
          echo '<li><a href="'.base_url().'targetcapaian/admin"><i class="fa fa-line-chart"></i> <span>Target Capaian</span></a></li>'; 
        }elseif($aksesx == 88 || $aksesx == 77){
          echo '<li><a href="'.base_url().'targetcapaian"><i class="fa fa-line-chart"></i> <span>Target Capaian</span></a></li>';
            
        }
        ?>

        <?php if($aksesx == 99 || $aksesx == 88 || $aksesx == 77){ ?>
        <li><a href="<?=base_url()?>dokumen"><i class="fa fa-file-o"></i> <span>Dokumen</span></a></li>
        <?php }else{ ?>
          <li><a href="<?=base_url()?>dokumen_tapem"><i class="fa fa-file-o"></i> <span>Dokumen</span></a></li>
        <?php } ?>
       
       
        
        
         
        
      </ul>
    </section>
  </aside>