<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versi</b> 3.0
    </div>
    <strong>Copyright &copy; 2018-2020 <a href="<?=base_url()?>">Biro Pemerintahan, Otonomi Daerah, & Kerjasama Provinsi Jawa Tengah
</a></strong>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/adminlte/dist/js/adminlte.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> -->
<script type="text/javascript" src="<?= base_url() ?>assets/pdfmake2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script> -->
<script src="<?php echo base_url() ?>assets/buttons.html5.js"></script>
<script src="<?php echo base_url() ?>assets/htmltotext.js"></script>




<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>

<?php
$info= $this->session->flashdata('info');
$pesan= $this->session->flashdata('message');

if($info == 'success'){ ?>
  <script>
      swal({
          title: "Berhasil",
          text: "<?php echo $pesan; ?>",
          timer: 1500,
          showConfirmButton: false,
          type: 'success'
      });
  </script>
  <?php    }elseif($info == 'danger'){ ?>
  <script>
      swal({
          title: "Gagal",
          text: "<?php echo $pesan; ?>",
          timer: 1500,
          showConfirmButton: false,
          type: 'danger'
      });
  </script>
  <?php  }?>


  <script>
    var url = window.location;
// for sidebar menu but not for treeview submenu
$('ul.sidebar-menu a').filter(function() {
    return this.href == url;
}).parent().siblings().removeClass('active').end().addClass('active');
// for treeview which is like a submenu
$('ul.treeview-menu a').filter(function() {
    return this.href == url;
}).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active menu-open').end().addClass('active menu-open');
</script>
