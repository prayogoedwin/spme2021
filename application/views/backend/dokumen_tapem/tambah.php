  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah Dokumen </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah Dokumen </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('dokumen_tapem/aksi_tambah');  ?>
              <div class="box-body">

              <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Tahun</label>
                    <select class="form-control" name="tahun">
                    <?php foreach($tahuns as $th){ ?>
                      <option value="<?=$th->tahun?>"><?=$th->tahun?></option>
                    <?php } ?>
                    </select>
                    <hr/><br/>                 
                  </div>
                </div>

            
            <b>BAB I PENDAHULUAN</b> :

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Latar Belakang</label>
                    <textarea id="latar" name="latar"></textarea>
                    
                    <hr/><br/>
                  </div>
                </div>
                

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Dasar Hukum</label>
                    <textarea id="dasar" name="dasar"></textarea>
                    <hr/><br/>
                  </div>
                </div>
                

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Kebijakan Umum</label>
                    <textarea id="umum" name="umum"></textarea>
                    <hr/><br/>
                  </div>
                </div>
                

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Arah Kebijakan</label>
                    <textarea id="arah" name="arah"></textarea>
                    <hr/><br/>
                  </div>
                </div>



             <b>BAB II PENERAPAN & PENCAPAIAN SPM</b> :   
                 <div class="form-group">
                  
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Bidang Urusan</label>
                    <input type="text"  readonly name="spm" value="<?php foreach($bidang_urusan as $bu){ echo $bu->nama_jenis_spm.',';}?>" class="form-control" id="inputEmail3"  required="required">
                    <hr/><br/>                 
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Jenis Pelayanan Dasar</label>
                    <textarea id="jenis" name="jenis">
                    <?php foreach($jenis_pelayanan_dasar as $jpd){ echo $jpd->nama_pelayanan.',';}?>
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>


                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Target Pelayanan SPM Oleh Daerah</label>
                    <textarea id="target" name="target"> 
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Realisasi</label>
                    <textarea id="realisasi" name="realisasi">
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Alokasi Anggaran</label>
                    <textarea id="alokasi" name="alokasi"> 
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>


                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Dukungan Personil</label>
                    <textarea id="dukungan" name="dukungan">
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>


                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Permasalahan & Solusi</label>
                    <textarea id="permasalahan" name="permasalahan"> 
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>


                <b>BAB III PROGRAM & KEGIATAN</b> :
                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Program & Kegiatan</label>
                    <textarea id="prog" name="prog"> 
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>


                <b>BAB IV PENUTUP</b> :
                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="control-label">Penutup</label>
                    <textarea id="penutup" name="penutup"> 
                    
                    </textarea>
                    <hr/><br/>
                  </div>
                </div>

                
                
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <?php if($this->session->userdata('id_akses') == 99){ 
                echo ' <a href="'.base_url().'dokumen/admin"><button type="button" class="btn pull-left">Batal</button></a>'; 
                }else{
                echo ' <a href="'.base_url().'dokumen"><button type="button" class="btn pull-left">Batal</button></a>';
                    
                }
                ?>

               
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>
  <script src="https://cdn.tiny.cloud/1/hsvjeom7ur4d0tx48ea4laj2ak88kq7qi1y4cv4p1h3z07d2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
  tinymce.init({
  selector: 'textarea#latar',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#dasar',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#umum',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#arah',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#jenis',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#target',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#realisasi',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>


<script>
  tinymce.init({
  selector: 'textarea#alokasi',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#dukungan',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#permasalahan',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#prog',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>

<script>
  tinymce.init({
  selector: 'textarea#penutup',
  plugins: 'lists paste',
  toolbar: 'numlist bullist | paste pastetext | undo redo | styleselect | forecolor | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
  height: 600,
  });
  </script>