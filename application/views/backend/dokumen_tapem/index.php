 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Datadasar
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Datadasar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

      <div class="box">
      <div class="box-header">
     
     <?php echo form_open_multipart('datadasar/index','style="display:none"');?>
     <div class="col-md-6">
     <!-- /.form-group -->
     <div class="form-group" >
             <select class="form-control select2" style="width: 100%;" name="tahun">
               
             <?php
             $thn_skr = date('Y');
             $sel = NULL;
             for ($x = $thn_skr; $x >= 2019; $x--) {  
             ?>
                 <option <?php echo $tahun == $x ? 'selected' : ''; ?> value="<?php echo $x ?>"><?php echo $x?></option>
             <?php
             }
             ?>
               
             </select>
           </div>
           <!-- /.form-group -->
     </div>
     <div class="col-md-2">
     <div class="form-group">
     <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
     </div>
     </div>
     <?php echo form_close(); ?>

     </div>
     
      <div class="box-header">
      
      <a href="<?=base_url()?>dokumen_tapem/tambah/"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen</button></a>  
        
          <!-- <a href="<?=base_url()?>dokumen_tapem/tambah/2"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen Pendidikan</button></a>  
          <a href="<?=base_url()?>dokumen_tapem/tambah/3"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen Kesehatan</button></a>
          <a href="<?=base_url()?>dokumen_tapem/tambah/4"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen PU</button></a>
          <a href="<?=base_url()?>dokumen_tapem/tambah/5"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen PR</button></a>
          <a href="<?=base_url()?>dokumen_tapem/tambah/6"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen Trantibumlinmas</button></a>
          <a href="<?=base_url()?>dokumen_tapem/tambah/7"><button type="button" class="btn btn-sm btn-primary">Tambah Dokumen Sosial</button></a> -->
        
  

        </div>
        <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Dibuat Oleh</th>
                  <!-- <th>SPM</th> -->
                  <th>Tahun</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td>Tapem <?=$all->user?></td>
                  <!-- <td><?=$all->bidang_urusan?></td> -->
                  <td><?=$all->tahun?></td>
                  <td>
                  <a target="blank" href="<?=base_url()?>dokumen_tapem/cetak/<?=$all->id_dokumen?>"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-print" style="font-size:12px"></i></button></a>
                  <a href="<?=base_url()?>dokumen_tapem/edit/<?=$all->id_dokumen?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>
                  </td>
                  
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
             'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>