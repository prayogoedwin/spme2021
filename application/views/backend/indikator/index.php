 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Indikator
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Indikator</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

        <div class="box">

        <div class="box-header">
         <!-- <a href="<?=base_url()?>kelolauser/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  -->
        </div>

        <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Pelayanan Dasar</th>
                  <th>SPM</th>
                  <th>Indikator</th>
                  <th>Sasaran</th>
                  <th>Cara Hitung</th>
                  <th>Pembilang</th>
                  <th>Penyebut</th>
                  <!-- <th width="50px">Aksi</th> -->
                 
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td><?=$all->pelayanan?></td>
                  <td><?=$all->spm?></td>
                  <td><?=$all->jenis_indikator?></td>
                  <td><?=$all->sasaran?></td>
                  <td><?=$all->cara_hitung?></td>
                  <td><?=$all->pembilang?></td>
                  <td><?=$all->penyebut?></td>
                  <!-- <td> -->
                
                  <!-- <a href="<?=base_url()?>indikator/edit/<?=$all->id_indikator?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>
             
                  <a href="<?=base_url()?>indikator/hapus/<?=$all->id_indikator?>"><button onclick="return confirm('Anda ingin menghapus ?')" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash" style="font-size:12px"></i></button></a></td> -->

                  <!-- </td> -->
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
             'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>