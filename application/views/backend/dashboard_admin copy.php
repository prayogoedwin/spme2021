<style>
	.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#container {
    height: 500px;
}

#container4 {
    height: 450px;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
</style>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($this->session->flashdata('info') != ''){ ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('notice')?></h4>
                <?=$this->session->flashdata('message')?>
              </div>
      <?php } ?>

        <div class="box">

        <?php echo form_open_multipart('dashboard/index');  ?>
        <div class="row">
        <div class="col-md-2">
        <!-- /.form-group -->
              <div class="form-group">
                <select class="form-control select2" style="width: 100%;" name="tahun">
                  
                <?php
                $thn_skr = date('Y');
                $sel = NULL;
                for ($x = $thn_skr; $x >= 2019; $x--) {  
                ?>
                    <option <?php echo $tahun == $x ? 'selected' : ''; ?> value="<?php echo $x ?>"><?php echo $x?></option>
                <?php
                }
                ?>
                  
                </select>
              </div>
              <!-- /.form-group -->
        </div>
        <div class="col-md-2">
        <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
        </div>
        </div>
        </div>
        <?php echo form_close(); ?>

         <div id="realisasi_capaian"></div>
                     
         <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
         <br/>

         <div id="anggaran_dilaporkan"></div>
                     
         <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
         <br/>

         <div id="kendala"></div>
                     
         <div class="divider divider-center"><i class="icon-circle-blank"></i></div>


         <div class="box-body">

        


              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Informasi</th>
                 
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($informasi as $info ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td><?=$info->info?></td>
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/template/footer.php"); ?>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            // 'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>

<script>
// Create the chart
Highcharts.chart('realisasi_capaian', {
    colors: ['#dd2857', '#f38e19', '#768daa', '#33dbcf', '#6bb08d', '#c2a695', '#6f83f6',
		'#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
	],
	 exporting: { enabled: false },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Realisasi Pencapaian SPM Tahun <?=$tahun?>'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Persentase Realisasi Pencapaian SPM'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation: -90,
                format: '{point.y:precentage.1f}%',
                y: 20, // 10 pixels down from the top
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:precentage.1f}%</b><br/>'
    },

    series: [
        {
            name: "Realisasi Pencapaian SPM",
            colorByPoint: true,
            data: [
                <?php 
					// data yang diambil dari database
					if(count($realisasi)>0)
					{
					   foreach ($realisasi as $rl) {
					   //echo "['<span style=\"font-color:#fff\">" .$est->sumber_data . "</span>'," . (int) $est->jumlah ."],\n";
					   echo "['" .$rl->kota . "</span>'," . (float) $rl->realisasi_capaian ."],\n";
					   }
					}
					?>
            ]
        }
    ],
    
       
});
</script>

<script>
// Create the chart
Highcharts.chart('anggaran_dilaporkan', {
    colors: ['#dd2857', '#f38e19', '#768daa', '#33dbcf', '#6bb08d', '#c2a695', '#6f83f6',
		'#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
	],
	 exporting: { enabled: false },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Anggaran Dilaporkan Tahun <?=$tahun?>'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Anggaran Dilaporkan Rp.'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation: -90,
                format: '{point.y}',
                y: 20, // 10 pixels down from the top
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: "Anggaran Dilaporkan",
            colorByPoint: true,
            data: [
                <?php 
					// data yang diambil dari database
					if(count($realisasi)>0)
					{
					   foreach ($realisasi as $rl) {
					   //echo "['<span style=\"font-color:#fff\">" .$est->sumber_data . "</span>'," . (int) $est->jumlah ."],\n";
					   echo "['" .$rl->kota . "</span>'," . (int) $rl->realisasi_anggaran_tahun_pelaporan ."],\n";
					   }
					}
					?>
            ]
        }
    ],
    
       
});
</script>

<script>
// Create the chart
Highcharts.chart('kendala', {
    colors: ['#dd2857', '#f38e19', '#768daa', '#33dbcf', '#6bb08d', '#c2a695', '#6f83f6',
		'#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
	],
	 exporting: { enabled: false },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Kendala Pada Tahun <?=$tahun?>'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Jumlah Kendala'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation: -90,
                format: '{point.y}',
                y: 20, // 10 pixels down from the top
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: "Kendala",
            colorByPoint: true,
            data: [
                <?php 
					// data yang diambil dari database
					if(count($kendala)>0)
					{
					   foreach ($kendala as $kn) {
					   //echo "['<span style=\"font-color:#fff\">" .$est->sumber_data . "</span>'," . (int) $est->jumlah ."],\n";
					   echo "['" .$kn->masalah . "</span>'," . (int) $kn->jml_kendala ."],\n";
					   }
					}
					?>
            ]
        }
    ],
    
       
});
</script>