<?php 


$rekp = "";
foreach ($chartkota as $rkp) {
    $rekp .= "{name: '" . $rkp->spm . "', y: " . $rkp->realisasi_capaian . "},";
}

$rekpr = "";
foreach ($chartprov as $rkpr) {
    $rekpr .= "{name: '" . $rkpr->spm . "', y: " . $rkpr->realisasi_capaian . "},";
}


$rpk = "";
foreach ($piekota as $pk) {
    $rpk .= "{name: '" . $pk->masalah . "', y: " . $pk->jml_kendala . "},";
}

$rppr = "";
foreach ($pieprov as $ppr) {
    $rppr .= "{name: '" . $ppr->masalah . "', y: " . $ppr->jml_kendala . "},";
}

$real_ang_prs = "";
foreach ($real_ang_pr as $rap) {
    $real_ang_prs .= "{name: '" . $rap->spm . "', y: " . $rap->realisasi_anggaran . "},";
}

$real_angs = "";
foreach ($real_ang as $ra) {
    $real_angs .= "{name: '" . $ra->spm . "', y: " . $ra->realisasi_anggaran . "},";
}


					
				

?>

<style>
	.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#container {
    height: 500px;
}

#container4 {
    height: 450px;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
</style>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        SELAMAT DATANG DI SPM-e JAWA TENGAH
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><?=$this->formatter->getDateTimeMonthFormatUser(date('Y-m-d H:i:s'))?></a></li>
        <!-- <li class="active">Dashboard</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($this->session->flashdata('info') != ''){ ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> <?=$this->session->flashdata('notice')?></h4>
                <?=$this->session->flashdata('message')?>
              </div>
      <?php } ?>

        <div class="box">

        <?php echo form_open_multipart('welcome/index');  ?>
        <div class="row">
        <div class="col-md-2">
        <!-- /.form-group -->
              <div class="form-group">
                <select class="form-control select2" style="width: 100%;" name="tahun">
                  
                <?php
                $thn_skr = date('Y');
                $sel = NULL;
                for ($x = $thn_skr; $x >= 2019; $x--) {  
                ?>
                    <option <?php echo $tahun == $x ? 'selected' : ''; ?> value="<?php echo $x ?>"><?php echo $x?></option>
                <?php
                }
                ?>
                  
                </select>
              </div>
              <!-- /.form-group -->
        </div>
        <div class="col-md-2">
        <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
        </div>
        </div>
        </div>

        <div id="realisasi_capaian_a"></div>
      

         <div id="realisasi_capaian"></div>

         <div class="row">
         <div class="col-md-6 text-center">
         <a target="BLANK" href="<?=base_url('capaian_bidang_provinsi')?>" class="btn btn-success">Lihat Persentase Capaian Per Bidang Provinsi</a>
         </div>

         <div class="col-md-6 text-center">
         <a target="BLANK" href="<?=base_url('capaian_bidang_kabkota')?>" class="btn btn-warning">Lihat Persentase Capaian Per Bidang Kabupaten/Kota</a>
         </div>

         </div>
                     
         <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
         <br/>

         <div id="anggaran_dilaporkan"></div>
                     
         <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
         <br/>

         <div id="kendala"></div>
                     
         <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
         <br/>

        <div class="row">
        <div class="col-md-6">
       
         <div id="piek" style="min-width: 300px; height: 400px; margin: 0 auto"></div>

         <div class="col-md-2">
         </div>
        <div class="row" style="text-align:center">
        <div class="col-md-5">
        <!-- /.form-group -->
              <div class="form-group">
                <select class="form-control select2" style="width: 100%;" name="kota">
                  
                <?php foreach($kotas as $kt): ?>

                        <option <?php echo $kt->id_kota == $kota ? 'selected' : 'null'; ?> value="<?=$kt->id_kota?>"> <?=$kt->nama_kota?></option>

                <?php endforeach; ?>
                  
                </select>
              </div>
              <!-- /.form-group -->
        </div>
        <div class="col-md-2">
        <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
        </div>
        </div>
        <div class="col-md-4">
         </div>
        </div>



        </div>
         <div class="col-md-6">
         <div id="piepr" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
         </div>
         </div>


        <div class="row">
        <div class="col-md-6">
       
         <div id="real_ang" style="min-width: 300px; height: 400px; margin: 0 auto"></div>

         <div class="col-md-2">
         </div>
        <div class="row" style="text-align:center">
        <div class="col-md-5">
        <!-- /.form-group -->
              <div class="form-group">
                <select class="form-control select2" style="width: 100%;" name="kota">
                  
                <?php foreach($kotas as $kt): ?>

                        <option <?php echo $kt->id_kota == $kota ? 'selected' : 'null'; ?> value="<?=$kt->id_kota?>"> <?=$kt->nama_kota?></option>

                <?php endforeach; ?>
                  
                </select>
              </div>
              <!-- /.form-group -->
        </div>
        <div class="col-md-2">
        <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
        </div>
        </div>
        <div class="col-md-4">
         </div>
        </div>



        </div>
         <div class="col-md-6">
         <div id="real_ang_pr" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
         <!--<div id="real_ang_pr" style="min-width: 300px; height: 100000px; margin: 0 auto"></div>-->
         </div>
         </div>

         


         <div class="box-body">

        


             
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php echo form_close(); ?>

  <?php include(__DIR__ . "/template/footer.php"); ?>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>


  <!-- <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            // 'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script> -->

<script>
// Create the chart
Highcharts.chart('realisasi_capaian', {
    colors: ['#dd2857', '#f38e19', '#768daa', '#33dbcf', '#6bb08d', '#c2a695', '#6f83f6',
		'#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
	],
	 exporting: { enabled: false },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Realisasi Pencapaian SPM Tahun <?=$tahun?>'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -40,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        categories: [
                  <?php foreach ($realisasi as $rl) { ?>
                                '<?=$rl->kota?>',
                  <?php }   ?>
                ]
    },
    // yAxis: {
    //     title: {
    //         text: 'Persentase Realisasi Pencapaian SPM'
    //     }

    // },
    legend: {
        enabled: true
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation: -90,
                format: '{point.y:precentage.1f}%',
                y: 20, // 10 pixels down from the top
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        // headerFormat: '<span style="font-size:11px"><?php foreach ($realisasi as $rl) { ?><br/>Pendidikan: <?=number_format($rl->pendidikan,2)?>%<br/>Kesehatan: <?=number_format($rl->kesehatan,2)?>%<br>PU: <?=number_format($rl->pu,2)?>%<br>PR: <?=number_format($rl->pr,2)?>%<br>Trantibumlinmas: <?=number_format($rl->trantibumlinmas,2)?>%<br>Sosial: <?=number_format($rl->sosial,2)?>%<br></span><br/>  <?php } ?>',
        pointFormat:  '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:precentage.1f}%</b><br/>'
    },
    // tooltip: {
    //   borderColor: '#2c3e50',
    //   shared: true,
    //   formatter: function (tooltip) {
    //     const header = `<span style="color: blue;">${this.x} - ${this.x.replace(/:00/, ':59')}</span><br/>`
        
    //     return header + (tooltip.bodyFormatter(this.points).join(''))
    //   }
    // },

    


    series: [
        {
          
            name: '',
            colorByPoint: true,
            data: [
                <?php 
                // data yang diambil dari database
                if(count($realisasi)>0)
                {
                  foreach ($realisasi as $rl) {
                  $x = "<br/>Pendidikan:".number_format($rl->pendidikan,2)."%<br/>Kesehatan:".number_format($rl->kesehatan,2)."%<br/>PU:".number_format($rl->pu,2)."%<br/>PR:".number_format($rl->pr,2)."%<br/>Trantibumlinmas:".number_format($rl->trantibumlinmas,2)."%<br/>Sosial:".number_format($rl->sosial,2)."<br/>Keseluruhan";
                  $y = ($rl->pendidikan+$rl->kesehatan+$rl->pu+$rl->trantibumlinmas+$rl->trantibumlinmas+$rl->sosial)/6;
                  //echo "['<span style=\"font-color:#fff\">" .$est->sumber_data . "</span>'," . (int) $est->jumlah ."],\n";
                  echo "['" .$rl->kota.$x."</span>'," . (float) number_format($y,2) ."],\n";

                  // echo "['Pendidikan:".$rl->pendidikan."</span>'," . (float) $rl->realisasi_capaian ."],\n";
                  //  echo "['" .$rl->kota"</span>'," . (float) $rl->realisasi_capaian ."],\n<br/>Pendidikan:'".$rl->pendidikan;
                  // echo "['Pendidikan:</span>'," . (float) $rl->pendidikan ."],\n";
                  }
                }
                ?>
            ]

        }
    ],
    
       
});
</script>



<script>
// Create the chart
Highcharts.chart('anggaran_dilaporkan', {
    colors: ['#dd2857', '#f38e19', '#768daa', '#33dbcf', '#6bb08d', '#c2a695', '#6f83f6',
		'#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
	],
	 exporting: { enabled: false },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Anggaran Dilaporkan Tahun <?=$tahun?>'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',
        
    },
    yAxis: {
        title: {
            text: 'Anggaran Dilaporkan Rp.'
        },

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation: -90,
                format: '{point.y}',
                y: 20, // 10 pixels down from the top
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: "Anggaran Dilaporkan",
            colorByPoint: true,
            data: [
                <?php 
					// data yang diambil dari database
					if(count($realisasi)>0)
					{
					   foreach ($realisasi as $rl) {
					   //echo "['<span style=\"font-color:#fff\">" .$est->sumber_data . "</span>'," . (int) $est->jumlah ."],\n";
					   echo "['" .$rl->kota . "</span>'," . (int) $rl->realisasi_anggaran_tahun_pelaporan ."],\n";
					   }
					}
					?>
            ]
        }
    ],
    
       
});
</script>

<script>
// Create the chart
Highcharts.chart('kendala', {
    colors: ['#dd2857', '#f38e19', '#768daa', '#33dbcf', '#6bb08d', '#c2a695', '#6f83f6',
		'#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
	],
	 exporting: { enabled: false },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Kendala Pada Tahun <?=$tahun?>'
    },
    // subtitle: {
    //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    // },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Jumlah Kendala'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation: -90,
                format: '{point.y}',
                y: 20, // 10 pixels down from the top
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: "Kendala",
            colorByPoint: true,
            data: [
                <?php 
					// data yang diambil dari database
					if(count($kendala)>0)
					{
					   foreach ($kendala as $kn) {
					   //echo "['<span style=\"font-color:#fff\">" .$est->sumber_data . "</span>'," . (int) $est->jumlah ."],\n";
					   echo "['" .$kn->masalah . "</span>'," . (int) $kn->jml_kendala ."],\n";
					   }
					}
					?>
            ]
        }
    ],
    
       
});
</script>



<script>
        // Rekap SPM kabkota
        Highcharts.chart('rekap', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            colors: [
            
            '#7fffd4',
            '#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
           
            
            ],
            title: {
                text: 'PROSENTASE PENERAPAN SPM <?=strtoupper(nama_kota($kota))?> <?=$tahun?>'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: ' {series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b> {point.name}</b>: {point.percentage:.1f}%',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Jumlah',
                data: [ <?= $rekp ?> ]
            }]
        });
    </script>

<script>
        // Rekap SPM Provinsi
        Highcharts.chart('rekapr', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            colors: [
            
            '#7fffd4',
            '#253090', '#aaeeee', '#c5f333', '#1f1011', '#c552df'
           
            
            ],
            title: {
                text: 'PROSENTASE PENERAPAN SPM PROVINSI JAWA TENGAH <?=$tahun?>'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: ' {series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b> {point.name}</b>: {point.percentage:.1f}%',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Jumlah',
                data: [ <?= $rekpr ?> ]
            }]
        });
    </script>

<script>
        // Rekap SPM kabkota
        Highcharts.chart('piek', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            // colors: [
            
            //     '#0000FF','#8A2BE2', '#A52A2A', '#FF7F50', '#DC143C', '#c552df'
           
            
            // ],
            title: {
                text: 'PERMASALAHAN PENERAPAN SPM <?=strtoupper(nama_kota($kota))?> TAHUN <?=$tahun?>'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: ' {series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b> {point.name}</b>: {point.percentage:.1f}%',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Jumlah',
                data: [ <?= $rpk ?> ]
            }]
        });
    </script>

<script>
        // Rekap SPM Provinsi
        Highcharts.chart('piepr', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            // colors: [
            
            // '#0000FF','#8A2BE2', '#A52A2A', '#FF7F50', '#DC143C', '#c552df'
           
            
            // ],
            title: {
                text: 'PERMASALAHAN PENERAPAN SPM PROVINSI JAWA TENGAH TAHUN <?=$tahun?>'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: ' {series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b> {point.name}</b>: {point.percentage:.1f}%',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Jumlah',
                data: [ <?= $rppr ?> ]
            }]
        });
    </script>



<script>
        // Rekap SPM Provinsi
        Highcharts.chart('real_ang', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            // colors: [
            
            // '#0000FF','#8A2BE2', '#A52A2A', '#FF7F50', '#DC143C', '#c552df'
           
            
            // ],
            title: {
                text: 'REALISASI ANGGARAN <?=strtoupper(nama_kota($kota))?> TAHUN <?=$tahun?>'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: ' {series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b> {point.name}</b>: {point.y}',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Jumlah',
                data: [ <?= $real_angs ?> ]
            }]
        });
    </script>

<script>
        // Rekap SPM Provinsi
        Highcharts.chart('real_ang_pr', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            // colors: [
            
            // '#0000FF','#8A2BE2', '#A52A2A', '#FF7F50', '#DC143C', '#c552df'
           
            
            // ],
            title: {
                text: 'REALISASI ANGGARAN PROVINSI JAWA TENGAH TAHUN <?=$tahun?>'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: ' {series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b> {point.name}</b>: {point.y}',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Jumlah',
                data: [ <?= $real_ang_prs ?> ]
            }]
        });
    </script>