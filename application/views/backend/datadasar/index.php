 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Datadasar
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Datadasar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

      <div class="box">
      <div class="box-header">
     
     <?php echo form_open_multipart('datadasar/index');?>
     <div class="col-md-6">
     <!-- /.form-group -->
     <div class="form-group">
             <select class="form-control select2" style="width: 100%;" name="tahun">
               
             <?php
             $thn_skr = date('Y');
             $sel = NULL;
             for ($x = $thn_skr; $x >= 2019; $x--) {  
             ?>
                 <option <?php echo $tahun == $x ? 'selected' : ''; ?> value="<?php echo $x ?>"><?php echo $x?></option>
             <?php
             }
             ?>
               
             </select>
           </div>
           <!-- /.form-group -->
     </div>
     <div class="col-md-2">
     <div class="form-group">
     <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
     </div>
     </div>
     <?php echo form_close(); ?>

     </div>
     
      <div class="box-header">
      
      <?php 
      $id = $this->session->userdata('id_pengguna');
      if(hidden_tambah_datadasar($id) == 0){ ?>
        
          <a href="<?=base_url()?>datadasar/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  
        
      <?php }else{ ?>
        <div class="alert alert-warning alert-dismissible">
       Datadasar, 1 tahun hanya di tambahkan 1 kali (sesuai RPJMD / 5 Tahun). Silahkan cari dan edit data tahun ini jika ada kesalahan data saat penambahan sebelumnya
        </div>
        <?php } ?>

        </div>
        <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Pelayanan Dasar</th>
                  <th>Penerima</th>
                  <th>Jumlah Sasaran RPJMD 5 Tahun<br/>(Hanya input Angka)</th>
                  <th>Tahun Data<br/>(RPJMD 5 Tahun)</th>
                  <th>Jumlah Anggaran RPJMD 5 Tahun<br/>(Hanya input Angka)</th>
                   <th>APBD Tahun Pelaporan</th>
                  <th>User</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td><?=$all->pelayanan?></td>
                  <td><?=$all->penerima?></td>
                  <td><?=$all->jumlah?></td>
                  <td><?=$all->tahun?></td>
                  <td><?=$all->anggaran?></td>
                   <td><?=$all->th_pelaporan?></td>
                  <td><?=$all->user?></td>
                  <td>

                  <a href="<?=base_url()?>datadasar/edit/<?=$all->id_datadasar?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>

                  </td>
                  
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
             'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>