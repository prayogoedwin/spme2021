 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Datadasar
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Datadasar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

      <div class="box">
      <div class="box-header">
      <?php 
      $id = $this->session->userdata('id_pengguna');
      if(hidden_tambah_datadasar($id) == 0){ ?>
        
          <!--<a href="<?=base_url()?>datadasar/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  -->
        
      <?php }else{ ?>
        <div class="alert alert-warning alert-dismissible">
        Datadasar, 1 tahun hanya di tambahkan 1 kali (sesuai RPJMD / 5 Tahun). Silahkan cari dan edit data tahun ini jika ada kesalahan data saat penambahan sebelumnya
        </div>
        <?php } ?>

        </div>
        <div class="box-body">
        <?php echo form_open_multipart('datadasar/aksi_tambah');  ?>
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Pelayanan Dasar</th>
                  <th>Penerima</th>
                  <th>Tahun Data<br/>(RPJMD 5 Tahun)</th>
                  <th>Jumlah Sasaran RPJMD 5 Tahun<br/>(Hanya input Angka)</th>
                  <th>Jumlah Anggaran RPJMD 5 Tahun<br/>(Hanya input Angka)</th>
                  <th>APBD Tahun Pelaporan<br/>(Hanya input Angka)</th>
               
                 
                </tr>
                </thead>
                <tbody>
                <?php 
                $th = DATE('Y') - 1;
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td>
                  <input type="hidden" name="id_pelayanan[]" value="<?=$all->id_jenis_pelayanan_dasar?>" >   
                  <input type="hidden" name="id_pengguna[]" value="<?=$this->session->userdata('id_pengguna')?>" >   
                  <input type="hidden" name="now[]" value="<?=date('Y-m-d H:i:s')?>" >   
                  <?=$all->nama_pelayanan?>
                  </td>
                  <td><?=$all->penerima?></td>
                  <td>
                  <input type="hidden" name="tahun[]" value="<?=$th?>" >   
                 <?=$th?>
                </td>
                  <td><input type="number" name="jumlah[]"  required="required"></td>
                  <td><input type="number" name="anggaran[]"  required="required"></td>
                   <td><input type="number" name="th_pelaporan[]"  required="required"></td>
        <!--           <td>-->
        <!--               <select class="form-control select2" style="width: 100%;" name="th_pelaporan[]">-->
        <!--            
        <--          </select>    -->
        <!--           </td>-->
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <a href="<?=base_url()?>"><button type="button" class="btn pull-left">Batal</button></a>
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
          <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
             'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>