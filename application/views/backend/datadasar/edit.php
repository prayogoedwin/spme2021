  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Datadasar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit Datadasar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('datadasar/aksi_edit');  ?>
              <div class="box-body">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">Jenis Pelayanan Dasar</label>
                  <div class="col-sm-6">
                    <input type="hidden" name="id" class="form-control" id="inputEmail3" value="<?=$alldata->id_datadasar?>" required="required">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->pelayanan?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">Penerima</label>
                  <div class="col-sm-6">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->penerima?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">Tahun</label>
                  <div class="col-sm-6">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->tahun?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">User</label>
                  <div class="col-sm-6">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->user?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">Jumlah Sasaran RPJMD 5 Tahun (Hanya input Angka)</label>
                  <div class="col-sm-6">
                    <input type="number" name="jumlah" class="form-control" id="inputEmail3" value="<?=$alldata->jumlah?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">Jumlah Anggaran RPJMD 5 Tahun (Hanya input Angka)</label>
                  <div class="col-sm-6">
                    <input type="number" name="anggaran" class="form-control" id="inputEmail3" value="<?=$alldata->anggaran?>" required="required">
                  </div>
                </div>
                
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-6 control-label">APBD Tahun Pelaporan (Hanya input Angka)</label>
                  <div class="col-sm-6">
                    <input type="number" name="th_pelaporan" class="form-control" id="inputEmail3" value="<?=$alldata->th_pelaporan?>" required="required">
                  </div>
                </div>
                
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <?php if($this->session->userdata('id_akses') == 99){ 
                echo ' <a href="'.base_url().'datadasar/admin"><button type="button" class="btn pull-left">Batal</button></a>'; 
                }else{
                echo ' <a href="'.base_url().'datadasar"><button type="button" class="btn pull-left">Batal</button></a>';
                    
                }
                ?>

               
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>