 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pelayanan Dasar
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Pelayanan Dasar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

        <div class="box">
        <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>SPM</th>
                  <th>Nama Pelayanan</th>
                  <th>Penerima</th>
                  <th>Pengampu</th>
                 
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td><?=$all->spm?></td>
                  <td><?=$all->nama_pelayanan?></td>
                  <td><?=$all->penerima?></td>
                  <td><?=$all->pengampu?></td>
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>