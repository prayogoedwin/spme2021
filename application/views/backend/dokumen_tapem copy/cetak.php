<html>
<head><title>Cetak Dokumen SPME Jateng</title></head>
<body onload="window.print();">
<H3>BAB I PENDAHULUAN</H3>
<ol type="A">
<li>LATAR BELAKANG</li>
<?=$dokumen->latar_belakang?>

<li>DASAR HUKUM</li>
<?=$dokumen->dasar_hukum?>

<li>KEBIJAKAN UMUM</li>
<?=$dokumen->dasar_hukum?>

<li>ARAH KEBIJAKAN</li>
<?=$dokumen->dasar_hukum?>

</ol>

<H3>BAB II PENERAPAN DAN PENCAPAIAN SPM</H3>
<ol type="A">
<li>BIDANG URUSAN <?=strtoupper($dokumen->bidang_urusan)?></li>
    <ol>
    <li>Jenis Pelayanan Dasar</li>
    <?=$dokumen->jenis_pelayanan?>
    <br/>

    <li>Target Pencapaian SPM</li>
    <?=$dokumen->target_pencapaian?>
    <br/>
    <table border="1" id="example" class="table table-bordered table-hover"> 
    <tr>
    <th>No</th>
    <th>Jenis Pelayanan Dasar</th>
    <th>Indikator</th>
    <th>Tahun</th>
    <th>Target %</th>
    </tr>
    <?php 
    $no = 0;
    $x=100;
    foreach($targetcapaian as $tr ): 
    $no++;
    ?>
    <tr>
    <tr>
    <td><?=$no?></td>
    <td><?=$tr->pelayanan?></td>
    <td><?=$tr->indikator?></td>
    <td><?=$tr->thn_datadasar?></td>
    <td><?=$tr->target?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    <br/>

    <li>Realisasi</li>
    <?=$dokumen->realisasi?>
    <br/>
    <table border="1" id="example" class="table table-bordered table-hover"> 
    <tr>
    <th>No</th>
    <th>Jenis Pelayanan Dasar</th>
    <th>Indikator</th>
    <th>Tahun</th>
    <th>Pembilang</th>
    <th>Penyebut</th>
    <th>Capaian %</th>
    <th>Anggaran Tahun Ini</th>
    <th>Realisasi Anggaran Tahun Pelaporan</th>
    </tr>
    <?php 
    $no = 0;
    $x=100;
    foreach($targetcapaian as $rl ): 
    $no++;
    ?>
    <tr>
    <tr>
    <td><?=$no?></td>
    <td><?=$rl->pelayanan?></td>
    <td><?=$rl->indikator?></td>
    <td><?=$rl->thn_datadasar?></td>
    <td><?=$rl->jumlah_pembilang?></td>
    <td><?=$rl->jumlah_penyebut?></td>
    <td>
    <?php if( $rl->jumlah_pembilang == 0 OR $rl->jumlah_penyebut == 0 ){

    echo "0" ;

    }else{

    echo number_format((($rl->jumlah_pembilang/$rl->jumlah_penyebut)*$x))."%" ; 

    }?></td>
     <td><?=number_format($rl->anggaran_t)?></td>
                  <td><?=$rl->realisasi?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    <br/>

    <li>Alokasi Anggaran</li>
    <?=$dokumen->alokasi_anggaran?>
    <br/>

    <li>Dukungan Personil</li>
    <?=$dokumen->dukungan_personil?>
    <br/>

    <li>Permasalahan dan Solusi</li>
    <?=$dokumen->permasalahan_solusi?>
    <br/>
    <table border="1" id="example" class="table table-bordered table-hover"> 
    <tr>
    <th>No</th>
    <th>Jenis Pelayanan Dasar</th>
    <th>Indikator</th>
    <th>Tahun</th>
    <th>Kendala</th>
    <!-- <th>Solusi</th> -->
    </tr>
    <?php 
    $no = 0;
    $x=100;
    foreach($targetcapaian as $kn ): 
    $no++;
    ?>
    <tr>
    <tr>
    <td><?=$no?></td>
    <td><?=$kn->pelayanan?></td>
    <td><?=$kn->indikator?></td>
    <td><?=$kn->thn_datadasar?></td>
    <td><?=$kn->kendalanya?></td>
    <!-- <td><?=$kn->solusi?></td> -->
    
    </tr>
    <?php endforeach; ?>
    </table>
    <br/>
    </ol>


</ol>

<H3>BAB III PENERAPAN DAN PENCAPAIAN SPM</H3>
<?=$dokumen->program_kegiatan?>
<br/>


<H3>BAB IV PENUTUP</H3>
<?=$dokumen->penutup?>
</body>
</html>

