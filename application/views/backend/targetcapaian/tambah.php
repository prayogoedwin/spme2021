 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Datadasar
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Datadasar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

      <div class="box">
        <div class="box-header">
     

        </div>
        <div class="box-body">
        <?php echo form_open_multipart('targetcapaian/aksi_tambah');  ?>
              <table id="examples" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Pelayanan Dasar</th>
                  <th>Jumlah Datadasar</th>
                  <th>Tahun Datadasar</th>
                  <th>Indikator</th>
                  <th>Tahun</th>
                  <th>Target %<br/>(Hanya Angka)</th>
                  <th>Anggaran Tahun Ini<br/>(Hanya Angka)</th>
                  <th>Pembilang</th>
                  <th>Penyebut (Sasaran tahun ini)</th>
                  <th>Realisasi Anggaran Tahun Pelaporan</th>
                  <th>Kendala</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $th = DATE('Y') - 1;
                $no = 0;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td>
                  <input type="hidden" name="id_indikator[]" value="<?=$all->id_indikator?>" >   
                  <input type="hidden" name="id_pengguna[]" value="<?=$this->session->userdata('id_pengguna')?>" >   
                  <input type="hidden" name="now[]" value="<?=date('Y-m-d H:i:s')?>" >   
                  <?=$all->pelayanan?>
                  </td>
                  <td><?=$all->jml_datadasar?></td>
                  <td><?=$all->tahun_datadasar?></td>
                  <td><?=$all->jenis_indikator?></td>
                  <td>
                  <input type="hidden" name="tahun[]" class="form-control"  value="<?=$th?>" >   
                  <?=$th?>
                  </td>
                  <td><input type="number" name="target[]"  class="form-control"  required="required" value="100" readonly></td>
                  <td><input type="number" name="anggaran[]"  class="form-control"  required="required"></td>
                  <td><?=$all->pembilang?><br/>
                  <input type="number" name="jumlah_pembilang[]"  class="form-control"  placeholder="Jumlah Pembilang" required="required">
                  </td>
                  <td><?=$all->penyebut?>
                  <input type="number" name="jumlah_penyebut[]"  class="form-control" placeholder="Jumlah Penyebut" required="required">
                  </td>
                  <td><input type="number" name="realisasi[]" class="form-control"  required="required"></td>
                  <td>
                  <!--<textarea name="kendala[]" id="" cols="30" rows="5"></textarea>-->
                  <select name="kendala[]"class="form-control" >
                      <option value="0">Pilih Kendala (Kosongi jika tidak ada)</option>
                      <?php $x= $this->db->query("SELECT * FROM kendala")->result(); 
                      foreach($x as $a):
                      ?>
                      <option value=" <?=$a->id?>"><?=$a->nama?></option>
                      <?php endforeach; ?>
                  </select>
                  </td>
                  
                  
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <a href="<?=base_url('targetcapaian')?>"><button type="button" class="btn pull-left">Batal</button></a>
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
          <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#examples').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            // 'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>