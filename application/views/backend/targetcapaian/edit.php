  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Targetcapaian</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit Targetcapaian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-10">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('targetcapaian/aksi_edit');  ?>
              <div class="box-body">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Jenis Pelayanan Dasar</label>
                  <div class="col-sm-8">
                    <input type="hidden" name="id" class="form-control" id="inputEmail3" value="<?=$alldata->id_targetcapaian?>" required="required">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->pelayanan?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Indikator</label>
                  <div class="col-sm-8">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->indikator?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Datadasar</label>
                  <div class="col-sm-8">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->jml_datadasar?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Anggaran Dasar</label>
                  <div class="col-sm-8">
                    <input type="number" disabled  class="form-control" id="inputEmail3" value="<?=$alldata->anggaran?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Tahun Datadasar</label>
                  <div class="col-sm-8">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->thn_datadasar?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Target % (Hanya input Angka)</label>
                  <div class="col-sm-8">
                    <input type="text" name="target" class="form-control" id="inputEmail3" value="<?=$alldata->target?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Pembilang</label>
                  <div class="col-sm-8">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->pembilang?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Jumlah Pembilang</label>
                  <div class="col-sm-8">
                    <input type="number" name="jumlah_pembilang" class="form-control" id="inputEmail3" value="<?=$alldata->jumlah_pembilang?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Penyebut</label>
                  <div class="col-sm-8">
                    <input type="text" disabled class="form-control" id="inputEmail3" value="<?=$alldata->penyebut?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Jumlah Penyebut</label>
                  <div class="col-sm-8">
                    <input type="number" name="jumlah_penyebut" class="form-control" id="inputEmail3" value="<?=$alldata->jumlah_penyebut?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Anggaran Capaian</label>
                  <div class="col-sm-8">
                    <input type="number" name="anggaran_t"  class="form-control" id="inputEmail3" value="<?=$alldata->anggaran_t?>" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Realisasi Anggaran Tahun Pelaporan</label>
                  <div class="col-sm-8">
                    <input type="number" name="realisasi"  class="form-control" id="inputEmail3" value="<?=$alldata->realisasi?>" required="required">
                  </div>
                </div>

                <div class="form-group" hidden>
                  <label for="inputEmail3" class="col-sm-4 control-label">Kendala</label>
                  <div class="col-sm-8">
                    <input type="text" name="kendala"  class="form-control" id="inputEmail3" value="<?=$alldata->kendala?>" >
                  </div>
                </div>
              
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <?php 
              $cek = get_user($this->session->userdata('id_pengguna'))->id_akses;
              if($cek == 99){ 
                echo ' <a href="'.base_url().'targetcapaian/admin"><button type="button" class="btn pull-left">Batal</button></a>'; 
                }else{
                echo ' <a href="'.base_url().'targetcapaian"><button type="button" class="btn pull-left">Batal</button></a>';
                    
                }
                ?>

               
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>