 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Targetcapaian
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Targetcapaian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     

      <div class="box">
      <div class="box-header">
          
      <?php 
      $id = $this->session->userdata('id_pengguna');
      if(hidden_tambah_targetcapaian($id) == 0){ ?>
        
      <a href="<?=base_url()?>targetcapaian/tambah"><button type="button" class="btn btn-sm btn-primary pull-right">Tambah Data</button></a>  
        
      <?php }else{ ?>
        <div class="alert alert-warning alert-dismissible">
        Targetcapaian, 1 tahun hanya di tambahkan 1 kali. Silahkan cari dan edit data tahun ini jika ada kesalahan data saat proses penambahan
        </div>
        <?php } ?>
      </div>

      <div class="box-header">
     
        <?php echo form_open_multipart('targetcapaian/index');  ?>
        <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
                <select class="form-control select2" style="width: 100%;" name="tahun">
                  
                <?php
                $thn_skr = date('Y');
                $sel = NULL;
                for ($x = $thn_skr; $x >= 2019; $x--) {  
                ?>
                    <option <?php echo $tahun == $x ? 'selected' : ''; ?> value="<?php echo $x ?>"><?php echo $x?></option>
                <?php
                }
                ?>
                  
                </select>
              </div>
              <!-- /.form-group -->
        </div>
        <div class="col-md-2">
        <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">Cari Data</button>
        </div>
        </div>
        <?php echo form_close(); ?>

        </div>
        <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>User</th>
                  <th>Jenis Pelayanan Dasar</th>
                  <th>Indikator</th>
                  <th>Jumlah Sasaran RPJMD 5 Tahun</th>
                  <th>Jumlah Anggaran RPJMD 5 Tahun</th>
                  <th>Tahun</th>
                  <th>Target %</th>
                  <th>Pembilang</th>
                  <th>Jumlah</th>
                  <th>Penyebut (Sasaran tahun ini)</th>
                  <th>Jumlah Sasaran tahun ini</th>
                  <th>Capaian %</th>
                  <th>Anggaran Tahun Ini</th>
                  <th>Realisasi Anggaran Tahun Pelaporan</th>
                  <th>Kendala</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                $x=100;
                foreach($alldata as $all ): 
                $no++;
                ?>
                <tr>
                  <td><?=$no?></td>
                  <td><?=$all->user?></td>
                  <td><?=$all->pelayanan?></td>
                  <td><?=$all->indikator?></td>
                  <td><?=$all->jml_datadasar?></td>
                  <td><?=number_format($all->anggaran)?></td>
                  <td><?=$all->thn_datadasar?></td>
                  <td><?=$all->target?></td>
                  <td><?=$all->pembilang?></td>
                  <td><?=$all->jumlah_pembilang?></td>
                  <td><?=$all->penyebut?></td>
                  <td><?=$all->jumlah_penyebut?></td>
                  <td>
                          <?php if( $all->jumlah_pembilang == 0 OR $all->jumlah_penyebut == 0 ){

                            echo "<button  class=\"btn btn-warning\"> Data 0 </button>" ;

                          }else{

                          echo number_format((($all->jumlah_pembilang/$all->jumlah_penyebut)*$x))."%" ; 

                          }?></td>
                  <td><?=number_format($all->anggaran_t)?></td>
                  <td><?=$all->realisasi?></td>
                  <td><?=$all->kendalanya?></td>
                  
                  
                  <td>

                  <a href="<?=base_url()?>targetcapaian/edit/<?=$all->id_targetcapaian?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>

                  </td>
                  
                </tr>
                <?php endforeach; ?>
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
     
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
             'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
     
      "fixedHeader": true
    });

</script>